<?php


/*_________Pages--*/
Route::get('/privacy-policy',function(){
	return view('pages.privacy-policy');
});

Route::get('/terms-conditions',function(){
	return view('pages.terms-conditions');
});
Route::get('/about-us',function(){
	return view('pages.about-us');
});







/*******************************************/
/*              ADMIN ROUTES START         */
/******************************************/
Route::get('/admin', function(){
	return view('admin.login');
});
Route::post('admin/dashboard', 'LoginController@login');
Route::middleware(['CheckAdmin'])->group(function () {
	Route::get('/admin/logout', 'LoginController@logout');
	

	Route::get('admin/dashboard', 'UserController@dashboard');


	Route::get('admin/sign-up', function(){
		return view('admin.sign-up');
	});
	Route::get('/admin/edit-profile','UserController@editAdminProfileForm');
	Route::get('/admin/change-password', 'UserController@changeAdminPassword');


	Route::post('/admin/sign-up', 'LoginController@signUp');
	/*Admin Events--*/

	Route::get('/admin/events', 'EventController@upcomingEventsList');
	Route::get('/admin/upcoming-events', 'EventController@upcomingEventsList');
	Route::get('admin/events/add', 'EventController@addEventForm');
	Route::post('/admin/events', 'EventController@addEvent');
	Route::get('/admin/viewEvent/{id}', 'EventController@viewEvent');
	Route::get("admin/events/{eventId}/delete",'EventController@deleteEvent');
	Route::get('admin/events/{eventId}/edit','EventController@editEventForm');
	Route::put('/admin/events/{eventId}','EventController@updateEvent');

	Route::post('/admin/update-events','EventController@updateEvent');

	Route::get('/admin/users', 'UserController@userList');
	Route::get('/admin/genre', 'EventController@genreList');
	Route::get('/admin/artist', 'EventController@artistList');
	Route::get('/admin/addArtist', function(){
		return view('events.addArtist');
	});
	Route::get('admin/viewArtist/{id}', 'EventController@viewArtist');
	Route::post('/admin/saveArtist', 'EventController@saveArtist');
	Route::post('/admin/addArtist', 'EventController@addArtist');
	Route::get('/admin/editArtist/{id}', 'EventController@editArtist');
	Route::post('/admin/updateArtist', 'EventController@updateArtist');
	Route::get('/admin/deleteArtist/{id}', 'EventController@deleteArtist');

	Route::get('/admin/addGenre', function(){
		return view('events.addGenre');
	});
	Route::get('admin/viewGenre/{id}', 'EventController@viewGenre');
	Route::post('/admin/saveGenre', 'EventController@saveGenre');
	Route::post('/admin/addGenre', 'EventController@addGenre');
	Route::get('/admin/editGenre/{id}', 'EventController@editGenre');
	Route::post('/admin/updateGenre', 'EventController@updateGenre');
	Route::get('/admin/deleteGenre/{id}', 'EventController@deleteGenre');

	Route::get('/admin/viewUser/{id}', 'UserController@viewUser');
	Route::get('/admin/suspendUser/{id}', 'UserController@suspendUser');
	Route::get('/admin/editUser/{id}', 'UserController@editUser');
	Route::get('/admin/addUser', function () {
		return view('admin.addUser');
	});
	Route::get('/admin/suspendedUsers', 'UserController@suspendedUserList');
	Route::get('/admin/activeUser/{id}', 'UserController@activeUser');
	Route::post('/admin/saveUser', 'UserController@saveUser');
	Route::post('/admin/updateUser', 'UserController@updateUser');
	Route::get('/admin/deleteUser/{id}', 'UserController@deleteUser');

});
/*******************************************/
/*              ADMIN ROUTES ENDS         */
/******************************************/




/*******************************************/
/*          USERS ROUTES START        	  */
/******************************************/
Route::get('/', 'UserController@events')->name("home");
Route::get('/load_more_events','UserController@loadMoreEvents');



Route::get('/events/filter','EventController@filterEvents')->name("home_filter");

Route::get('/login/google', 'Auth\LoginController@googleLogin');
Route::get('/login/google/callback', 'Auth\LoginController@googleLoginCallback');

Route::get('/login/facebook', 'Auth\LoginController@facebookLogin');
Route::get('/login/facebook/callback', 'Auth\LoginController@facebookLoginCallback');

Route::get('/login/linkedin', 'Auth\LoginController@linkedinLogin');
Route::get('/login/linkedin/callback', 'Auth\LoginController@linkedinLoginCallback');

Route::get('/sign-up', function () {
	return view('users.sign-up');
});
Route::get('/login', function () {
	return view('users.login');
});

Route::post('/sign-up', 'UserController@signUp');
Route::post('/login', 'UserController@login');

Route::get('/logout', 'UserController@logout');
Route::post('/forgot-password','UserController@forgotPassword');




Route::get('/forgot-password', function () {
	return view('pages.forgot-password');
});
Route::post('/forgot-password','UserController@forgotPassword');
/* Check user loggedin--*/

	//Route::middleware(['CheckUser'])->group(function () {
		Route::get('/create-profile', 'UserController@createProfile');
		Route::post('/create-profile', 'UserController@create_profile');
		Route::get('/my-profile', 'UserController@myProfile');
		Route::get('/edit-profile', 'UserController@editProfile');
		Route::post('/edit-profile', 'UserController@edit_profile');
		Route::post('/change-password', 'UserController@changePassword');

		Route::get('/setting', 'UserController@setings');
		/*Route::get('/messages', 'MessageController@message');*/
		/*Route::post('/send_message','MessageController@sendMessage');*/



		Route::get('/other-user-profile/{id}', 'UserController@otherUserProfile');
		Route::post('/blockUser', 'MessageController@blockUser');
		Route::post('/unBlockUser', 'MessageController@unBlockUser');
		//------------------events------------------------
		
		Route::post('/favouriteEvent', 'EventController@favouriteEvent');
		Route::post('/unfavouriteEvent', 'EventController@unfavouriteEvent');
		Route::get('/favorites', 'EventController@favouriteEventList');
		Route::post('/eventComment', 'EventController@eventComment');
		Route::get('/news-feed', 'EventController@addEventNewsFeed');
		Route::get('/news-feed-details/{id}', 'EventController@newsFeedDetails');
		Route::post('/addNewsFeed', 'EventController@addNewsFeed');
		Route::post('/likeNews', 'UserController@likeNews');

		Route::post('/newsComment', 'UserController@newsComment');
		Route::post('/followSetting', 'UserController@followSetting');
		Route::post('/replyComment', 'UserController@replyComment');
		Route::post('/likePost', 'UserController@likePost');
		Route::post('/follow', 'UserController@follow');
		Route::post('/unfollow', 'UserController@unFollow');
		Route::post('/feedback', 'EventController@feedback');
		Route::post('/feedComments', 'EventController@feedComments');
		Route::get('/logout', 'UserController@logout');
		Route::get('/notifications', 'UserController@notifications');
	//});

Route::middleware(['checkAuth'])->group(function () {
	Route::get('/create-profile', 'UserController@createProfile');
	Route::post('/create-profile', 'UserController@create_profile');
	Route::get('/my-profile', 'UserController@myProfile');
	Route::get('/edit-profile', 'UserController@editProfile');
	Route::post('/edit-profile', 'UserController@edit_profile');
	Route::post('/change-password', 'UserController@changePassword');
	Route::get('/setting', function () {
		return view('users.setting');
	});
	
	
	//------------------events------------------------
	
	Route::post('/like', 'EventController@likeEvent');
	Route::post('/unlike', 'EventController@unlikeEvent');
	Route::post('/favouriteEvent', 'EventController@favouriteEvent');
	//Route::post('/unfavouriteEvent', 'EventController@unfavouriteEvent');
	Route::get('/favorites', 'EventController@favouriteEventList');



	/* News Feed--*/
	// Route::get('/news-feed', function () {
	// 	return view('events.news-feed');
	// });

  Route::get('/create-profile', 'UserController@createProfile');
Route::post('/create-profile', 'UserController@create_profile');
Route::get('/my-profile', 'UserController@myProfile');
Route::get('/edit-profile', 'UserController@editProfile');
Route::post('/edit-profile', 'UserController@edit_profile');
Route::post('/change-password', 'UserController@changePassword');

Route::get('/setting', 'UserController@setings');

/*Messages Start--*/
Route::get("/messages/{userId}", 'MessageController@message');
Route::get('/messages', 'MessageController@message');
Route::post('/send_message','MessageController@sendMessage');
Route::get("/getMessagesWithLimit","MessageController@getMessagesWithLimit");
Route::get('/message/get_messages_auto','MessageController@get_messages_auto');
/*Messages Ends--*/

Route::post('/blockUser', 'MessageController@blockUser');
Route::post('/unBlockUser', 'MessageController@unBlockUser');
//------------------events------------------------
Route::get('/event-details/{id}', 'EventController@eventDetails');
Route::post('/like', 'EventController@likeEvent');
Route::post('/unlike', 'EventController@unlikeEvent');
Route::post('/favouriteEvent', 'EventController@favouriteEvent');
Route::post('/unfavouriteEvent', 'EventController@unfavouriteEvent');
Route::get('/favorites', 'EventController@favouriteEventList');
Route::post('/eventComment', 'EventController@eventComment');
Route::get('/news-feed', 'EventController@addEventNewsFeed');
Route::get('/news-feed-details/{id}', 'EventController@newsFeedDetails');
Route::post('/addNewsFeed', 'EventController@addNewsFeed');
Route::post('/likeNews', 'UserController@likeNews');

Route::post('/newsComment', 'UserController@newsComment');
Route::post('/followSetting', 'UserController@followSetting');
Route::post('/replyComment', 'UserController@replyComment');
Route::post('/likePost', 'UserController@likePost');
Route::post('/follow', 'UserController@follow');
Route::post('/unfollow', 'UserController@unFollow');
Route::post('/feedback', 'EventController@feedback');
Route::post('/feedComments', 'EventController@feedComments');
Route::get('/logout', 'UserController@logout');
});

Route::get('/get_cities',"UserController@getCities");




//-----------------pages--------------------------
Route::get('/contact-us', function () {
	return view('pages.contact-us');
});
Route::post('/contactUs', 'MessageController@contactUs');


Route::get('/send_email', 'MessageController@mail');
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
Artisan::call('config:clear');
Artisan::call('config:clear');
// return what you want
});
/*******************************************/
/*          USERS ROUTES ENDS        	  */
/******************************************/
