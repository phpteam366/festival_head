<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDataType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE users MODIFY COLUMN gmail_id VARCHAR(100)');
        DB::statement('ALTER TABLE users MODIFY COLUMN facebook_id VARCHAR(100)');
        DB::statement('ALTER TABLE users MODIFY COLUMN linkdin_id VARCHAR(100)');
        DB::statement('ALTER TABLE users MODIFY COLUMN password VARCHAR(255) NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
