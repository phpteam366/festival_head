<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fname')->nullable();
            $table->string('lname')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('image')->nullable();
            $table->integer('facebook_id')->nullable();
            $table->integer('gmail_id')->nullable();
            $table->integer('linkdin_id')->nullable();
            $table->string('register_type')->nullable()->comment("F = facebook, G = gmail, L = linkedIn, E = email");
            $table->enum('user_type',['A', 'U'])->comment("A = admin, U = normal user");
            $table->boolean('status')->comment("0=>Inactive, 1=>Active");
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
