<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('nick_name');
            $table->integer('age');
            $table->enum('gender', ['Female', 'Male']);
            $table->integer('genre_id');
            $table->integer('artists_id');
            $table->integer('state_id');
            $table->integer('city_id')->nullable();
            $table->enum('marital_status',['Married', 'Unmarried', 'In relationship']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
