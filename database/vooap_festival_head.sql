-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 11, 2019 at 04:41 AM
-- Server version: 5.6.43
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vooap_festival_head`
--

-- --------------------------------------------------------

--
-- Table structure for table `artists`
--

CREATE TABLE `artists` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `artists`
--

INSERT INTO `artists` (`id`, `name`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Kylie Minogue', '155428487478805_v9_bc.jpg', 1, '2019-04-03 09:47:54', '2019-04-03 09:53:23'),
(2, 'Olivia Newton-John', '15542849911254_v9_bb.jpg', 1, '2019-03-28 05:18:51', '2019-04-04 05:20:16'),
(5, 'Rick Springfield', '15542850371674_v9_ba.jpg', 1, '2019-03-28 05:38:32', '2019-04-04 05:24:15'),
(6, 'Michae', 'dummy.png', 1, '2019-04-04 05:02:31', '2019-04-04 05:02:31'),
(7, 'Nick Cave', '1554465777index.jpeg', 1, '2019-04-04 05:03:05', '2019-04-05 12:02:57'),
(8, 'Johny', '1554355095f1.jpeg', 1, '2019-04-04 05:18:15', '2019-04-04 05:18:15'),
(12, 'chicago', '1554366854harry.jpeg', 1, '2019-04-04 08:33:30', '2019-04-04 08:34:14'),
(13, 'Mary Horay', '1554436766mary.jpeg', 1, '2019-04-05 03:59:26', '2019-04-05 03:59:26'),
(14, 'john Horay', '15544658051254_v9_bb.jpg', 1, '2019-04-05 03:59:53', '2019-04-05 12:03:25'),
(15, 'innayka', 'dummy.png', 1, '2019-04-05 09:38:47', '2019-04-05 10:09:56'),
(16, 'yashika', '1554464463inna.jpeg', 1, '2019-04-05 11:41:03', '2019-04-05 11:41:03'),
(17, 'metallica', 'dummy.png', 1, '2019-04-10 02:10:41', '2019-04-10 02:10:41');

-- --------------------------------------------------------

--
-- Table structure for table `blocked_users`
--

CREATE TABLE `blocked_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `blocked_user_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0=>Inactive, 1=>Active',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` int(10) UNSIGNED NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `seen_by` tinyint(1) NOT NULL COMMENT 'viewed user id',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `chats`
--

INSERT INTO `chats` (`id`, `sender_id`, `receiver_id`, `message`, `seen_by`, `created_at`, `updated_at`) VALUES
(1, 30, 34, 'are you available on sunday?', 30, '2019-04-05 12:06:04', '2019-04-05 12:38:23'),
(2, 30, 40, '', 0, '2019-04-05 12:09:19', '2019-04-05 12:09:19'),
(3, 49, 48, 'tell', 49, '2019-04-08 11:20:08', '2019-04-08 11:58:02');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `state_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL COMMENT '0=>Inactive. 1=>active',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL COMMENT 'event or news feed id',
  `receiver_type` enum('E','N') COLLATE utf8_unicode_ci NOT NULL COMMENT 'E = event, N = news feed',
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `receiver_id`, `receiver_type`, `comment`, `created_at`, `updated_at`) VALUES
(35, 30, 26, 'E', 'test', '2019-04-04 08:52:21', '2019-04-04 08:52:21'),
(36, 30, 25, 'E', 'test', '2019-04-04 09:19:25', '2019-04-04 09:19:25'),
(37, 30, 14, 'N', 'test', '2019-04-05 04:33:16', '2019-04-05 04:33:16'),
(38, 30, 14, 'N', 'test by yamini', '2019-04-05 04:34:56', '2019-04-05 04:34:56'),
(39, 30, 14, 'N', 'test', '2019-04-05 05:10:38', '2019-04-05 05:10:38'),
(40, 30, 19, 'N', 'hi', '2019-04-05 06:46:42', '2019-04-05 06:46:42'),
(41, 30, 23, 'N', 'hiiyiyiyi', '2019-04-05 07:22:59', '2019-04-05 07:22:59'),
(42, 33, 22, 'N', 'hiiiiii', '2019-04-05 07:24:50', '2019-04-05 07:24:50'),
(43, 39, 24, 'N', 'dfhfdghfghfg', '2019-04-05 07:36:22', '2019-04-05 07:36:22'),
(44, 30, 23, 'N', 'hi', '2019-04-05 08:58:09', '2019-04-05 08:58:09'),
(45, 30, 23, 'N', 'ello', '2019-04-05 08:58:11', '2019-04-05 08:58:11'),
(46, 30, 23, 'N', 'hey', '2019-04-05 08:58:13', '2019-04-05 08:58:13'),
(47, 30, 23, 'N', 'bye', '2019-04-05 08:58:15', '2019-04-05 08:58:15'),
(48, 30, 23, 'N', 'u', '2019-04-05 08:58:16', '2019-04-05 08:58:16'),
(49, 30, 23, 'N', 'bbye', '2019-04-05 08:58:18', '2019-04-05 08:58:18'),
(50, 40, 25, 'N', 'Hi', '2019-04-05 09:15:59', '2019-04-05 09:15:59'),
(51, 40, 25, 'N', 'you', '2019-04-05 09:16:02', '2019-04-05 09:16:02'),
(52, 40, 25, 'N', 'Nice video', '2019-04-05 09:16:18', '2019-04-05 09:16:18'),
(53, 40, 24, 'N', 'Hi', '2019-04-05 09:29:17', '2019-04-05 09:29:17'),
(54, 40, 24, 'N', 'you', '2019-04-05 09:29:19', '2019-04-05 09:29:19'),
(55, 40, 24, 'N', '??\n\\', '2019-04-05 09:29:20', '2019-04-05 09:29:20'),
(56, 40, 24, 'N', '\\', '2019-04-05 09:29:21', '2019-04-05 09:29:21'),
(57, 40, 24, 'N', '\\', '2019-04-05 09:29:22', '2019-04-05 09:29:22'),
(58, 40, 25, 'N', 'hi', '2019-04-05 09:34:47', '2019-04-05 09:34:47'),
(59, 40, 25, 'N', 't1', '2019-04-05 09:34:49', '2019-04-05 09:34:49'),
(60, 40, 25, 'N', 't1', '2019-04-05 09:34:49', '2019-04-05 09:34:49'),
(61, 40, 25, 'N', 'test 3', '2019-04-05 09:34:56', '2019-04-05 09:34:56'),
(62, 40, 25, 'N', '4', '2019-04-05 09:34:58', '2019-04-05 09:34:58'),
(63, 40, 25, 'N', 't 5', '2019-04-05 09:34:59', '2019-04-05 09:34:59'),
(64, 40, 25, 'N', 'hi', '2019-04-05 09:35:19', '2019-04-05 09:35:19'),
(65, 40, 25, 'N', '2', '2019-04-05 09:35:21', '2019-04-05 09:35:21'),
(66, 40, 25, 'N', '3', '2019-04-05 09:35:23', '2019-04-05 09:35:23'),
(67, 40, 25, 'N', '4', '2019-04-05 09:35:24', '2019-04-05 09:35:24'),
(68, 40, 25, 'N', '5', '2019-04-05 09:35:25', '2019-04-05 09:35:25'),
(69, 40, 25, 'N', '6', '2019-04-05 09:35:26', '2019-04-05 09:35:26'),
(70, 40, 25, 'N', 'i7', '2019-04-05 09:35:28', '2019-04-05 09:35:28'),
(71, 40, 25, 'N', 'i8', '2019-04-05 09:35:30', '2019-04-05 09:35:30'),
(72, 40, 25, 'N', 'i9', '2019-04-05 09:35:32', '2019-04-05 09:35:32'),
(73, 43, 31, 'N', 'hi', '2019-04-08 07:47:33', '2019-04-08 07:47:33'),
(74, 43, 31, 'N', 'ws you', '2019-04-08 07:47:35', '2019-04-08 07:47:35'),
(75, 43, 25, 'N', '8-4-2019', '2019-04-08 07:48:12', '2019-04-08 07:48:12'),
(76, 43, 34, 'N', 'Hi Bob, How are you doing?', '2019-04-08 08:39:27', '2019-04-08 08:39:27'),
(77, 43, 34, 'N', 'Are you free Today?', '2019-04-08 08:39:36', '2019-04-08 08:39:36'),
(78, 43, 34, 'N', 'Are you free bob\n?', '2019-04-08 08:39:48', '2019-04-08 08:39:48'),
(79, 44, 36, 'N', 'comment by bob on alice post', '2019-04-08 09:10:42', '2019-04-08 09:10:42'),
(80, 44, 35, 'N', 'comment by bob', '2019-04-08 09:12:27', '2019-04-08 09:12:27'),
(81, 44, 39, 'N', 'Hi Alice, Can you please help me.', '2019-04-08 09:22:38', '2019-04-08 09:22:38'),
(82, 44, 40, 'N', 'ABCD', '2019-04-08 09:25:46', '2019-04-08 09:25:46'),
(83, 46, 41, 'N', 'Paul still me waiting for you?', '2019-04-08 09:32:48', '2019-04-08 09:32:48'),
(84, 47, 42, 'N', 'Hi Heena, You can please go nOW?', '2019-04-08 09:36:03', '2019-04-08 09:36:03'),
(85, 46, 43, 'N', 'hi 123', '2019-04-08 09:39:29', '2019-04-08 09:39:29'),
(86, 47, 42, 'N', 'hi paul', '2019-04-08 09:40:24', '2019-04-08 09:40:24'),
(87, 47, 27, 'E', 'hi', '2019-04-08 10:03:02', '2019-04-08 10:03:02'),
(88, 49, 45, 'N', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humor, or randomized words which don\'t look even slightly believable.', '2019-04-08 10:30:15', '2019-04-08 10:30:15'),
(89, 48, 45, 'N', 'Where can I get some?', '2019-04-08 10:31:12', '2019-04-08 10:31:12'),
(90, 48, 44, 'N', 'Where can I get some?', '2019-04-08 10:32:05', '2019-04-08 10:32:05'),
(91, 48, 46, 'N', 'Where can I get some?', '2019-04-08 10:37:42', '2019-04-08 10:37:42'),
(92, 49, 46, 'N', 'don\'t look even slightly believable.', '2019-04-08 10:39:35', '2019-04-08 10:39:35'),
(93, 48, 46, 'N', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humor, or randomized words which don\'t look even slightly believable. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humor, or randomized words which don\'t look even slightly believable.are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humor, or randomized words which don\'t look even slightly believable.', '2019-04-08 10:39:39', '2019-04-08 10:39:39'),
(94, 42, 28, 'E', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.', '2019-04-08 11:36:23', '2019-04-08 11:36:23'),
(95, 49, 46, 'N', 'Hi', '2019-04-08 11:52:05', '2019-04-08 11:52:05'),
(96, 49, 46, 'N', 'you are Fine?', '2019-04-08 11:52:11', '2019-04-08 11:52:11'),
(97, 49, 46, 'N', 'at are you doing now a daya?', '2019-04-08 11:52:17', '2019-04-08 11:52:17'),
(98, 49, 46, 'N', 'can you please discuss about your hobbies and interests?', '2019-04-08 11:52:35', '2019-04-08 11:52:35'),
(99, 49, 46, 'N', 'Do you have any knowledge about Leatest Trends towards Technology?', '2019-04-08 11:52:54', '2019-04-08 11:52:54'),
(100, 49, 47, 'N', 'hi', '2019-04-08 12:57:44', '2019-04-08 12:57:44'),
(101, 49, 47, 'N', 'u', '2019-04-08 12:57:45', '2019-04-08 12:57:45'),
(102, 49, 47, 'N', 'at u doing', '2019-04-08 12:57:50', '2019-04-08 12:57:50'),
(103, 49, 47, 'N', 'here r u', '2019-04-08 12:57:53', '2019-04-08 12:57:53'),
(104, 49, 47, 'N', 'u fine?', '2019-04-08 12:57:57', '2019-04-08 12:57:57'),
(105, 49, 47, 'N', 'll me pleasse', '2019-04-08 12:58:01', '2019-04-08 12:58:01');

-- --------------------------------------------------------

--
-- Table structure for table `contactuses`
--

CREATE TABLE `contactuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 = replied, 0 = not replied',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contactuses`
--

INSERT INTO `contactuses` (`id`, `name`, `email`, `subject`, `message`, `status`, `created_at`, `updated_at`) VALUES
(14, 'Harry', 'harry@yopmail.com', 'Intro', 'Demo Prac', 0, '2019-04-04 07:07:30', '2019-04-04 07:07:30'),
(15, 'Harjinder Singh', 'Paul@yopmail.com', 'Intro', 'hi', 0, '2019-04-08 09:50:48', '2019-04-08 09:50:48'),
(16, 'Bib', 'bob@yopmail.com', 'How to join', 'How to join this', 0, '2019-04-08 10:17:26', '2019-04-08 10:17:26'),
(17, 'Ashley', 'ashley@yopmail.com', 'How to join', 'How to join', 0, '2019-04-08 10:36:20', '2019-04-08 10:36:20'),
(18, 'Ashley', 'ashley@yopmail.com', 'How to join', 'How to join', 0, '2019-04-08 10:38:46', '2019-04-08 10:38:46'),
(19, 'Ashley', 'ashley@yopmail.com', 'How to join', 'How to join', 0, '2019-04-08 10:38:56', '2019-04-08 10:38:56'),
(20, 'Ashley', 'ashley@yopmail.com', 'How to join', 'How to join', 0, '2019-04-08 10:39:18', '2019-04-08 10:39:18'),
(21, 'Ashley', 'ashley@yopmail.com', 'How to join', 'How to join', 0, '2019-04-08 10:40:12', '2019-04-08 10:40:12'),
(22, 'Ashley', 'ashley@yopmail.com', 'How to join', 'How to joinHow to joinHow to joinv', 0, '2019-04-08 10:43:34', '2019-04-08 10:43:34'),
(23, 'Aarav Test', 'bb@yopmail.com', 'HOw to join this', 'HOw to join this tell me in details', 0, '2019-04-08 11:44:53', '2019-04-08 11:44:53'),
(24, 'Aarav Test', 'bb@yopmail.com', 'HOw to join this', 'HOw to join this tell me in details', 0, '2019-04-08 11:48:34', '2019-04-08 11:48:34'),
(25, 'Aarav Test', 'bb@yopmail.com', 'HOw to join this', 'HOw to join this tell me in details', 0, '2019-04-08 11:48:38', '2019-04-08 11:48:38'),
(26, 'Aarav Test', 'bb@yopmail.com', 'HOw to join this', 'HOw to join this tell me in details', 0, '2019-04-08 11:49:06', '2019-04-08 11:49:06'),
(27, 'Aarav Test', 'bb@yopmail.com', 'HOw to join this', 'HOw to join this tell me in details', 0, '2019-04-08 11:50:14', '2019-04-08 11:50:14'),
(28, 'Neww', 'bb@yopmail.com', 'Hiwwww', 'How to login', 0, '2019-04-08 11:53:15', '2019-04-08 11:53:15'),
(29, 'Neww', 'bb@yopmail.com', 'Hiwwww', 'How to login', 0, '2019-04-08 11:53:42', '2019-04-08 11:53:42'),
(30, 'Neww', 'bb@yopmail.com', 'Hiwwww', 'How to login', 0, '2019-04-08 11:54:13', '2019-04-08 11:54:13'),
(31, 'Neww', 'bb@yopmail.com', 'Hiwwww', 'How to login', 0, '2019-04-08 11:54:24', '2019-04-08 11:54:24'),
(32, 'Neww', 'bb@yopmail.com', 'Hiwwww', 'How to login', 0, '2019-04-08 11:56:22', '2019-04-08 11:56:22'),
(33, 'ash', 'ashley@yopmail.com', 'hi', 'waiting for you', 0, '2019-04-08 12:02:08', '2019-04-08 12:02:08'),
(34, 'ash', 'ashley@yopmail.com', 'hey', '2nd email', 0, '2019-04-08 12:07:27', '2019-04-08 12:07:27'),
(35, 'Ashley Mark', 'ashley@yopmail.com', 'I can able to book events', 'I can able to book events, When i click on book button', 0, '2019-04-08 12:11:47', '2019-04-08 12:11:47');

-- --------------------------------------------------------

--
-- Table structure for table `contact_uses`
--

CREATE TABLE `contact_uses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 = replied, 0 = not replied',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `genre_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `artists_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'multiple artist',
  `date_time` datetime NOT NULL,
  `ticket_url` text COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL COMMENT '0 = Pending, 1 = Outdated',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `description`, `location`, `latitude`, `longitude`, `genre_id`, `image`, `artists_id`, `date_time`, `ticket_url`, `status`, `created_at`, `updated_at`) VALUES
(9, 'Festival Event2', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Mohali, Punjab, India', '30.7046486', '76.71787259999996', 4, '1554355943.jpeg', '7', '2019-04-05 11:15:00', 'https://www.google.com/', '0', '2019-04-04 05:32:23', '2019-04-05 12:16:04'),
(10, 'CLT2', 'It  is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English.', 'Channapatna, Karnataka, India', '12.6492422', '77.20027449999998', 2, '1554356100.jpg', '6', '2019-04-06 12:00:00', 'https://www.google.com/', '0', '2019-04-04 05:35:00', '2019-04-05 09:41:47'),
(12, 'Event Calender', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 'Abdul Gafoor Khan Estate, Lalbahadur Shastri Road, Ambedkar Nagar, Kurla West, Kurla, Mumbai, Maharashtra, India', '19.07389449999999', '72.87664689999997', 1, '1554356238.jpg', '5', '2019-04-20 12:00:00', 'https://www.google.com/', '0', '2019-04-04 05:37:18', '2019-04-04 05:37:18'),
(13, 'Gamification', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Federal Territory of Kuala Lumpur, Malaysia', '3.1569486', '101.71230300000002', 4, '1554356479.jpeg', '5', '2019-04-26 14:45:00', 'https://www.google.com/', '0', '2019-04-04 05:41:19', '2019-04-04 05:41:19'),
(14, 'Paramount Event', '\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\"\r\n\r\n1914 translation by H. Rackham\r\n\"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?\"', 'Riyadh Saudi Arabia', '24.7135517', '46.67529569999999', 2, '1554356562.jpg', '2', '2019-04-27 03:30:00', 'https://www.google.com/', '0', '2019-04-04 05:42:42', '2019-04-04 05:42:42'),
(15, 'Splan event 6', 'vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\"', 'Edinburgh, UK', '55.953252', '-3.188266999999996', 2, '1554356757.jpeg', '1', '2019-05-03 08:30:00', 'https://www.google.com/', '0', '2019-04-04 05:45:57', '2019-04-05 10:51:53'),
(16, 'Vendor Event', '\"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?\"', 'Las Vegas, NV, USA', '36.1699412', '-115.13982959999998', 2, '1554356853.jpg', '1', '2019-04-30 15:45:00', 'https://www.google.com/', '0', '2019-04-04 05:47:33', '2019-04-04 05:47:33'),
(17, 'Chingay Event', '\"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.\"\r\n\r\n1914 translation by H. Rackham\r\n\"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.\"', 'Paris, France', '48.85661400000001', '2.3522219000000177', 2, '1554356929.jpeg', '5', '2019-05-11 21:30:00', 'https://www.google.com/', '0', '2019-04-04 05:48:49', '2019-04-04 05:48:49'),
(18, 'Stress Buster Event', '2019 translation by H. Rackham 2019 translation by H. Rackham', 'QLD, Australia', '-20.9175738', '142.70279559999994', 2, '1554362358.jpeg', '1', '2019-05-04 11:30:00', 'https://www.google.com/', '0', '2019-04-04 05:50:22', '2019-04-05 11:46:05'),
(19, 'WWE Event', '2019  of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC', 'Fort Worth, TX, USA', '32.7554883', '-97.3307658', 4, '1554357110.jpeg', '5', '2019-05-05 11:30:00', 'https://www.google.com/', '0', '2019-04-04 05:51:50', '2019-04-04 05:55:16'),
(20, 'Edudata', 'The standard Lorem Ipsum passage, used since the 1500s', 'West Bengal, India', '22.9867569', '87.85497550000002', 1, '1554357209.jpeg', '5', '2019-04-04 13:30:00', 'https://www.google.com/', '0', '2019-04-04 05:53:29', '2019-04-04 05:53:29'),
(21, 'Special Event', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', 'Rajasthan India Tours, Ana Sagar Circular Rd, Rawat Nagar, Ajmer, Rajasthan, India', '26.453285', '74.59331499999996', 1, '1554357296.jpg', '1', '2019-04-04 11:30:00', 'https://www.google.com/', '0', '2019-04-04 05:54:56', '2019-04-04 05:54:56'),
(22, 'Cooldest Neighbourhood', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"', 'UK', '55.378051', '-3.43597299999999', 4, '1554357448.jpeg', '6', '2019-04-05 11:45:00', 'https://www.google.com/', '0', '2019-04-04 05:57:28', '2019-04-04 05:57:28'),
(23, 'Christmas Event', 'Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate', 'Edinburgh, UK', '55.953252', '-3.188266999999996', 4, '1554357553.jpeg', '6', '2019-12-25 11:45:00', 'https://www.google.com/', '0', '2019-04-04 05:59:13', '2019-04-04 05:59:13'),
(24, 'Classical Event', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'AB, Canada', '53.9332706', '-116.5765035', 1, '1554357651.jpeg', '6', '2019-04-04 01:45:00', 'https://www.google.com/', '0', '2019-04-04 06:00:51', '2019-04-04 06:00:51'),
(25, 'Classical event 2', 'sequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum', 'QLD, Australia', '-20.9175738', '142.70279559999994', 1, '1554357720.jpg', '6', '2019-04-04 11:45:00', 'https://www.google.com/', '0', '2019-04-04 06:02:00', '2019-04-04 06:02:00'),
(26, 'Classical event 3', 'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"', 'India', '20.593684', '78.96288000000004', 1, '1554357801.jpeg', '1', '2019-04-04 11:45:00', 'https://www.google.com/', '0', '2019-04-04 06:03:21', '2019-04-04 06:03:21'),
(27, 'world final', 'et, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\"', 'OR, USA', '43.8041334', '-120.55420119999997', 2, '1554358116.jpeg', '1', '2019-04-05 11:45:00', 'https://www.google.com/', '0', '2019-04-04 06:08:36', '2019-04-04 07:18:46'),
(28, 'Adventure Related Event', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.', 'Mumbai Central', '18.9690247', '72.82052920000001', 5, '1554459395.jpeg', '14,7', '2019-04-24 16:00:00', 'http://google.com', '0', '2019-04-05 10:16:35', '2019-04-05 11:45:27'),
(29, 'Download', 'rock festival', 'Melbourne VIC, Australia', '-37.8136276', '144.96305759999996', 7, '1554862358.jpg', '17', '2019-04-24 12:30:00', 'https://www.ticketmaster.com.au/', '0', '2019-04-10 02:12:39', '2019-04-10 02:12:39');

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE `favorites` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 = favorite, 0 = unfavorite',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `favorites`
--

INSERT INTO `favorites` (`id`, `user_id`, `event_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 30, 26, 0, '2019-04-05 11:25:36', '2019-04-05 12:17:26'),
(2, 30, 28, 1, '2019-04-05 11:25:42', '2019-04-05 12:18:26'),
(3, 30, 27, 1, '2019-04-05 11:25:48', '2019-04-05 12:17:58'),
(4, 30, 24, 0, '2019-04-05 11:58:19', '2019-04-05 12:17:42'),
(5, 30, 25, 0, '2019-04-05 11:58:22', '2019-04-05 12:17:40'),
(6, 30, 23, 1, '2019-04-05 12:18:12', '2019-04-05 12:18:12'),
(7, 43, 28, 1, '2019-04-08 07:26:25', '2019-04-08 07:26:25'),
(8, 43, 27, 1, '2019-04-08 07:26:26', '2019-04-08 07:26:26'),
(9, 44, 20, 1, '2019-04-08 08:32:10', '2019-04-08 08:32:10'),
(10, 44, 22, 1, '2019-04-08 08:32:14', '2019-04-08 08:32:14'),
(11, 44, 21, 1, '2019-04-08 08:32:15', '2019-04-08 08:32:15'),
(12, 46, 28, 1, '2019-04-08 09:30:57', '2019-04-08 09:30:57'),
(13, 46, 27, 1, '2019-04-08 09:30:57', '2019-04-08 09:30:57'),
(14, 46, 26, 1, '2019-04-08 09:30:58', '2019-04-08 09:30:58'),
(15, 47, 27, 1, '2019-04-08 09:34:42', '2019-04-08 10:01:42'),
(16, 47, 26, 1, '2019-04-08 10:01:41', '2019-04-08 10:01:41'),
(17, 49, 26, 1, '2019-04-08 10:28:07', '2019-04-08 10:28:07'),
(18, 49, 27, 1, '2019-04-08 10:28:07', '2019-04-08 10:28:07');

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE `feedbacks` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `ratings` tinyint(4) NOT NULL,
  `reviews` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `feedbacks`
--

INSERT INTO `feedbacks` (`id`, `user_id`, `event_id`, `ratings`, `reviews`, `created_at`, `updated_at`) VALUES
(2, 47, 27, 3, 'good', '2019-04-08 10:02:53', '2019-04-08 10:02:53'),
(3, 42, 28, 3, 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia volupta', '2019-04-08 11:36:36', '2019-04-08 11:36:36');

-- --------------------------------------------------------

--
-- Table structure for table `feeds`
--

CREATE TABLE `feeds` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `feeds`
--

INSERT INTO `feeds` (`id`, `user_id`, `title`, `description`, `image`, `video`, `status`, `created_at`, `updated_at`) VALUES
(13, 31, 'CLT', 'test it', '', '', 1, '2019-04-04 06:54:14', '2019-04-04 06:54:14'),
(14, 33, 'Hello', 'sdfgdsgsdfgdf', '15543680190_OJNi5e6O1gHvsN98.jpeg', '', 1, '2019-04-04 08:53:39', '2019-04-04 08:53:39'),
(15, 33, 'Yessss', 'Yessss YessssYessssYessssYessssYessssYessss YessssYessssYessss YessssYessss', '', '1554368109SampleVideo_1280x720_1mb (1).mp4', 1, '2019-04-04 08:55:09', '2019-04-04 08:55:09'),
(16, 34, 'assa', 'sxsx', '', '', 1, '2019-04-04 10:01:28', '2019-04-04 10:01:28'),
(17, 37, 'Olivia first post', 'Hi, I am here for data exchange.', '155438293812.jpeg', '', 1, '2019-04-04 13:02:18', '2019-04-04 13:02:18'),
(18, 30, 'title', 'descrption', '1554441159police.png', '', 1, '2019-04-05 05:12:39', '2019-04-05 05:12:39'),
(19, 30, 'test1', 'kjkjk', '', '1554441264Insta-video.mp4', 1, '2019-04-05 05:14:25', '2019-04-05 05:14:25'),
(20, 2, 'Hi, i am interested to post my all posts.', 'Hello Guys', '1554442147m2.jpeg', '', 1, '2019-04-05 05:29:07', '2019-04-05 05:29:07'),
(21, 33, 'fdghbfdh', 'dfghdfgh', '', '1554447687SampleVideo_1280x720_1mb (1).mp4', 1, '2019-04-05 07:01:27', '2019-04-05 07:01:27'),
(22, 33, 'Yessss', 'Yessss Yessss Yessss Yessss Yessss Yessss YessssYessss Yessss Yessss Yessss Yessss Yessss Yessss', '15544477771_hxxM_pKi8A5uw3Pl4rlcBQ.gif', '', 1, '2019-04-05 07:02:57', '2019-04-05 07:02:57'),
(23, 30, 'dfgrdgd', 'rtehegrtgtgtg', '', '1554448201yum.mp4', 1, '2019-04-05 07:10:01', '2019-04-05 07:10:01'),
(24, 30, 'khjujhhjh', 'uhuhiuiunm', '1554448360f1.jpeg', '', 1, '2019-04-05 07:12:40', '2019-04-05 07:12:40'),
(25, 40, 'Hi INNA', 'This is my first post.', '', '1554455717yum.mp4', 1, '2019-04-05 09:15:17', '2019-04-05 09:15:17'),
(26, 42, 'Classical Event 3', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', '1554460287CJC Marketing.png', '', 1, '2019-04-05 10:31:27', '2019-04-05 10:31:27'),
(27, 43, 'Hi', 'I am new user. i would like to share about my friend image.', '', '', 1, '2019-04-08 07:29:45', '2019-04-08 07:29:45'),
(28, 43, 'Hi', 'I am ready to share my 2nd friend image.', '1554708766inna.jpeg', '', 1, '2019-04-08 07:32:46', '2019-04-08 07:32:46'),
(29, 43, 'hI', '3RD IMAGE', '1554708826m1.jpeg', '', 1, '2019-04-08 07:33:46', '2019-04-08 07:33:46'),
(30, 43, 'Dummy post test user', 'Dummy data for new post Dummy data for new post Dummy data for new post Dummy data for new post Dummy data for new post Dummy data for new post Dummy data for new post Dummy data for new post Dummy data for new post Dummy data for new post .', '1554709027m1.jpeg', '', 1, '2019-04-08 07:37:07', '2019-04-08 07:37:07'),
(31, 43, 'Dummy post text new user', 'test comment Add New Post Add New Post Add New Post Add New Post Add New Post Add New Post', '1554709177inayka.jpeg', '', 1, '2019-04-08 07:39:37', '2019-04-08 07:39:37'),
(32, 44, 'Hi', 'This is my first job', '1554712379f1.jpeg', '', 1, '2019-04-08 08:32:59', '2019-04-08 08:32:59'),
(33, 44, 'hi', 'Bob First IMage', '1554712612inayka.jpeg', '', 1, '2019-04-08 08:36:52', '2019-04-08 08:36:52'),
(34, 44, 'Hi', 'Bob 2nd Image', '1554712638inna.jpeg', '', 1, '2019-04-08 08:37:18', '2019-04-08 08:37:18'),
(35, 43, 'hi', 'Alice First IMage', '1554712706passport img.jpeg', '', 1, '2019-04-08 08:38:26', '2019-04-08 08:38:26'),
(36, 43, 'Hey', 'Alice 2nd Image', '1554712732m1.jpeg', '', 1, '2019-04-08 08:38:52', '2019-04-08 08:38:52'),
(37, 44, 'Hi Alice', 'Hope You are doing great Alice?', '1554713158f1.jpeg', '', 1, '2019-04-08 08:45:58', '2019-04-08 08:45:58'),
(38, 44, 'Subject - Alice, need for the favor', 'Hi Alice,\r\nI want to discuss with you related to future trends. can you please help me alice?', '1554713227inna.jpeg', '', 1, '2019-04-08 08:47:07', '2019-04-08 08:47:07'),
(39, 44, 'Regarding BA Profile', 'Hi Alice, Do you have any knowledge about BA Profile?', '1554715286provider.jpeg', '', 1, '2019-04-08 09:21:26', '2019-04-08 09:21:26'),
(40, 44, 'Hi alice', 'Are you QA Alice?', '1554715463inayka.jpeg', '', 1, '2019-04-08 09:24:23', '2019-04-08 09:24:23'),
(41, 46, 'For Paul', 'Hi Paul,\r\nHope you are feeling great.', '1554715935f1.jpeg', '', 1, '2019-04-08 09:32:15', '2019-04-08 09:32:15'),
(42, 47, 'Hi Heena,', 'This is for you', '1554716126passport img.jpeg', '', 1, '2019-04-08 09:35:26', '2019-04-08 09:35:26'),
(43, 46, '123', '123', '1554716355m1.jpeg', '', 1, '2019-04-08 09:39:15', '2019-04-08 09:39:15'),
(44, 44, 'What is Lorem Ipsum?', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularized in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1554717425restaurant1.jpg', '', 1, '2019-04-08 09:57:05', '2019-04-08 09:57:05'),
(45, 48, 'Where can I get some?', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humor, or randomized words which don\'t look even slightly believable.', '1554719378p2.jpg', '', 1, '2019-04-08 10:29:38', '2019-04-08 10:29:38'),
(46, 49, 'Get Something?', 'by injected humor, or randomized words which don\'t look even slightly believable.', '1554719833inayka.jpeg', '', 1, '2019-04-08 10:37:13', '2019-04-08 10:37:13'),
(47, 49, 'hi', 'cbvcvx', '1554728227f1.jpeg', '', 1, '2019-04-08 12:57:07', '2019-04-08 12:57:07'),
(48, 43, 'sdf', 'sdfsdfsdf', '1554728648Screen Shot 2019-04-04 at 7.18.34 PM.png', '', 1, '2019-04-08 13:04:08', '2019-04-08 13:04:08');

-- --------------------------------------------------------

--
-- Table structure for table `follows`
--

CREATE TABLE `follows` (
  `id` int(10) UNSIGNED NOT NULL,
  `follower_id` int(11) NOT NULL,
  `following_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0=>Inactive, 1=>Active',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `follows`
--

INSERT INTO `follows` (`id`, `follower_id`, `following_id`, `status`, `created_at`, `updated_at`) VALUES
(12, 48, 49, 1, '2019-04-08 12:32:20', '2019-04-08 13:33:30'),
(13, 30, 39, 1, '2019-04-09 04:37:16', '2019-04-09 04:49:07'),
(14, 39, 30, 1, '2019-04-09 04:48:26', '2019-04-09 04:48:26');

-- --------------------------------------------------------

--
-- Table structure for table `genres`
--

CREATE TABLE `genres` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `genres`
--

INSERT INTO `genres` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Classical', 1, '2019-03-28 06:27:02', '2019-04-04 03:55:41'),
(2, 'Hip Hop', 1, '2019-03-28 06:27:02', '2019-04-03 09:54:18'),
(4, 'Dance', 1, '2019-03-28 06:27:02', '2019-04-03 09:54:58'),
(5, 'Folk  Dance1', 1, '2019-04-05 09:42:58', '2019-04-05 09:43:33'),
(6, 'Salsa', 1, '2019-04-05 11:42:40', '2019-04-05 11:42:40'),
(7, 'balboa', 1, '2019-04-05 11:43:26', '2019-04-05 11:43:26'),
(8, 'Argentine Tango', 1, '2019-04-05 11:43:49', '2019-04-05 11:43:49'),
(9, 'Ballroom', 1, '2019-04-05 11:44:08', '2019-04-05 11:44:08'),
(10, 'swing', 1, '2019-04-05 11:46:10', '2019-04-05 11:46:10');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL COMMENT 'event or news feed id',
  `receiver_type` enum('E','N') COLLATE utf8_unicode_ci NOT NULL COMMENT 'E = event, N = news feed',
  `status` tinyint(1) NOT NULL COMMENT '1 = liked, 0 = unliked',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `user_id`, `receiver_id`, `receiver_type`, `status`, `created_at`, `updated_at`) VALUES
(42, 30, 15, 'N', 1, '2019-04-04 12:44:59', '2019-04-04 12:45:18'),
(43, 37, 26, 'E', 0, '2019-04-04 13:11:07', '2019-04-04 13:11:09'),
(44, 30, 17, 'N', 1, '2019-04-05 03:59:10', '2019-04-05 03:59:10'),
(45, 30, 16, 'N', 1, '2019-04-05 04:30:17', '2019-04-05 04:30:17'),
(46, 2, 17, 'N', 1, '2019-04-05 04:43:40', '2019-04-05 04:43:55'),
(47, 2, 27, 'E', 0, '2019-04-05 04:50:03', '2019-04-05 04:50:06'),
(48, 30, 13, 'E', 0, '2019-04-05 05:04:54', '2019-04-05 05:05:06'),
(49, 30, 14, 'N', 1, '2019-04-05 05:11:27', '2019-04-05 05:11:27'),
(50, 30, 19, 'N', 1, '2019-04-05 05:16:54', '2019-04-05 06:46:37'),
(51, 30, 10, 'E', 0, '2019-04-05 06:29:48', '2019-04-05 06:29:53'),
(52, 30, 26, 'E', 1, '2019-04-05 06:36:19', '2019-04-05 06:38:25'),
(53, 33, 20, 'N', 1, '2019-04-05 06:46:20', '2019-04-05 06:46:20'),
(54, 39, 25, 'E', 1, '2019-04-05 07:38:02', '2019-04-05 07:38:02'),
(55, 33, 25, 'E', 1, '2019-04-05 07:40:48', '2019-04-05 07:40:48'),
(56, 33, 27, 'E', 1, '2019-04-05 07:42:34', '2019-04-05 07:42:34'),
(57, 43, 24, 'N', 1, '2019-04-08 07:28:11', '2019-04-08 07:28:11'),
(58, 43, 27, 'N', 1, '2019-04-08 07:30:02', '2019-04-08 07:30:02'),
(59, 43, 36, 'N', 1, '2019-04-08 09:28:01', '2019-04-08 09:28:02'),
(60, 47, 27, 'E', 0, '2019-04-08 10:01:53', '2019-04-08 10:01:55'),
(61, 47, 44, 'N', 1, '2019-04-08 10:12:47', '2019-04-08 10:12:47'),
(62, 47, 41, 'N', 1, '2019-04-08 10:13:25', '2019-04-08 10:13:25'),
(63, 49, 46, 'N', 1, '2019-04-08 10:42:56', '2019-04-08 10:42:56'),
(64, 49, 45, 'N', 1, '2019-04-08 10:43:13', '2019-04-08 10:43:13'),
(65, 48, 46, 'N', 1, '2019-04-08 10:43:57', '2019-04-08 10:43:57'),
(66, 42, 26, 'N', 1, '2019-04-08 11:41:34', '2019-04-08 11:41:34'),
(67, 49, 47, 'N', 1, '2019-04-08 12:57:13', '2019-04-08 12:57:13');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `chat_id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `sender_id`, `receiver_id`, `chat_id`, `message`, `created_at`, `updated_at`) VALUES
(1, 30, 34, 1, 'Hello', '2019-04-05 12:06:16', '2019-04-05 12:06:16'),
(2, 30, 30, 1, 'hi', '2019-04-05 12:38:02', '2019-04-05 12:38:02'),
(3, 30, 30, 1, 'are you available on sunday?', '2019-04-05 12:38:23', '2019-04-05 12:38:23'),
(4, 49, 48, 3, 'Hiiii', '2019-04-08 11:20:13', '2019-04-08 11:20:13'),
(5, 48, 49, 3, 'yes', '2019-04-08 11:20:29', '2019-04-08 11:20:29'),
(6, 49, 48, 3, 'how are you?', '2019-04-08 11:51:20', '2019-04-08 11:51:20'),
(7, 49, 48, 3, 'tell', '2019-04-08 11:58:02', '2019-04-08 11:58:02');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(106, '2014_10_12_000000_create_users_table', 1),
(107, '2014_10_12_100000_create_password_resets_table', 1),
(108, '2019_03_04_064346_create_user_details_table', 1),
(109, '2019_03_04_064407_create_events_table', 1),
(110, '2019_03_04_064437_create_likes_table', 1),
(111, '2019_03_04_064457_create_comments_table', 1),
(112, '2019_03_04_064507_create_shares_table', 1),
(113, '2019_03_04_064527_create_feeds_table', 1),
(114, '2019_03_04_064547_create_favorites_table', 1),
(115, '2019_03_04_064601_create_chats_table', 1),
(116, '2019_03_04_064610_create_messages_table', 1),
(119, '2019_03_04_090919_create_genres_table', 1),
(120, '2019_03_04_091237_create_notification_settings_table', 1),
(121, '2019_03_04_091306_create_states_table', 1),
(122, '2019_03_05_043342_create_artists_table', 1),
(125, '2019_03_05_050550_change_data_type', 1),
(126, '2019_03_04_090853_create_blocked_users_table', 2),
(127, '2019_03_12_095557_create_cities_table', 3),
(128, '2019_03_22_042859_create_follows_table', 3),
(129, '2019_03_22_090234_create_feedbacks_table', 4),
(136, '2019_03_04_064633_create_contactuses_table', 5),
(137, '2019_03_26_103714_create_notifications_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `feed_id` int(11) NOT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notification_for` enum('L','C','F') COLLATE utf8_unicode_ci NOT NULL COMMENT 'L = like, C = comment, F = follow',
  `is_seen` tinyint(1) DEFAULT NULL COMMENT '1 = viewed, 0 = not viewed',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `sender_id`, `receiver_id`, `feed_id`, `comment`, `notification_for`, `is_seen`, `created_at`, `updated_at`) VALUES
(19, 30, 33, 15, NULL, 'L', NULL, '2019-04-04 12:44:59', '2019-04-04 12:44:59'),
(20, 30, 37, 17, NULL, 'L', NULL, '2019-04-05 03:59:11', '2019-04-05 03:59:11'),
(21, 30, 34, 16, NULL, 'L', NULL, '2019-04-05 04:30:17', '2019-04-05 04:30:17'),
(22, 30, 33, 14, 'test', 'C', NULL, '2019-04-05 04:33:16', '2019-04-05 04:33:16'),
(23, 30, 33, 14, 'test by yamini', 'C', NULL, '2019-04-05 04:34:56', '2019-04-05 04:34:56'),
(24, 2, 37, 17, NULL, 'L', NULL, '2019-04-05 04:43:40', '2019-04-05 04:43:40'),
(25, 30, 33, 14, 'test', 'C', NULL, '2019-04-05 05:10:38', '2019-04-05 05:10:38'),
(26, 30, 33, 14, NULL, 'L', NULL, '2019-04-05 05:11:27', '2019-04-05 05:11:27'),
(27, 30, 30, 19, NULL, 'L', NULL, '2019-04-05 05:16:54', '2019-04-05 05:16:54'),
(28, 33, 2, 20, NULL, 'L', NULL, '2019-04-05 06:46:20', '2019-04-05 06:46:20'),
(29, 30, 30, 19, 'hi', 'C', NULL, '2019-04-05 06:46:42', '2019-04-05 06:46:42'),
(30, 30, 30, 23, 'hiiyiyiyi', 'C', NULL, '2019-04-05 07:22:59', '2019-04-05 07:22:59'),
(31, 33, 33, 22, 'hiiiiii', 'C', NULL, '2019-04-05 07:24:50', '2019-04-05 07:24:50'),
(32, 39, 30, 24, 'dfhfdghfghfg', 'C', NULL, '2019-04-05 07:36:22', '2019-04-05 07:36:22'),
(33, 33, 23, 0, NULL, 'F', NULL, '2019-04-05 07:48:14', '2019-04-05 07:48:14'),
(34, 30, 30, 23, 'hi', 'C', NULL, '2019-04-05 08:58:09', '2019-04-05 08:58:09'),
(35, 30, 30, 23, 'ello', 'C', NULL, '2019-04-05 08:58:11', '2019-04-05 08:58:11'),
(36, 30, 30, 23, 'hey', 'C', NULL, '2019-04-05 08:58:13', '2019-04-05 08:58:13'),
(37, 30, 30, 23, 'bye', 'C', NULL, '2019-04-05 08:58:15', '2019-04-05 08:58:15'),
(38, 30, 30, 23, 'u', 'C', NULL, '2019-04-05 08:58:16', '2019-04-05 08:58:16'),
(39, 30, 30, 23, 'bbye', 'C', NULL, '2019-04-05 08:58:19', '2019-04-05 08:58:19'),
(40, 40, 40, 25, 'Hi', 'C', NULL, '2019-04-05 09:16:00', '2019-04-05 09:16:00'),
(41, 40, 40, 25, 'you', 'C', NULL, '2019-04-05 09:16:02', '2019-04-05 09:16:02'),
(42, 40, 40, 25, 'Nice video', 'C', NULL, '2019-04-05 09:16:18', '2019-04-05 09:16:18'),
(43, 40, 30, 24, 'Hi', 'C', NULL, '2019-04-05 09:29:17', '2019-04-05 09:29:17'),
(44, 40, 30, 24, 'you', 'C', NULL, '2019-04-05 09:29:19', '2019-04-05 09:29:19'),
(45, 40, 30, 24, '??\n\\', 'C', NULL, '2019-04-05 09:29:20', '2019-04-05 09:29:20'),
(46, 40, 30, 24, '\\', 'C', NULL, '2019-04-05 09:29:21', '2019-04-05 09:29:21'),
(47, 40, 30, 24, '\\', 'C', NULL, '2019-04-05 09:29:22', '2019-04-05 09:29:22'),
(48, 40, 40, 25, 'hi', 'C', NULL, '2019-04-05 09:34:47', '2019-04-05 09:34:47'),
(49, 40, 40, 25, 't1', 'C', NULL, '2019-04-05 09:34:49', '2019-04-05 09:34:49'),
(50, 40, 40, 25, 't1', 'C', NULL, '2019-04-05 09:34:49', '2019-04-05 09:34:49'),
(51, 40, 40, 25, 'test 3', 'C', NULL, '2019-04-05 09:34:57', '2019-04-05 09:34:57'),
(52, 40, 40, 25, '4', 'C', NULL, '2019-04-05 09:34:58', '2019-04-05 09:34:58'),
(53, 40, 40, 25, 't 5', 'C', NULL, '2019-04-05 09:34:59', '2019-04-05 09:34:59'),
(54, 40, 40, 25, 'hi', 'C', NULL, '2019-04-05 09:35:19', '2019-04-05 09:35:19'),
(55, 40, 40, 25, '2', 'C', NULL, '2019-04-05 09:35:21', '2019-04-05 09:35:21'),
(56, 40, 40, 25, '3', 'C', NULL, '2019-04-05 09:35:23', '2019-04-05 09:35:23'),
(57, 40, 40, 25, '4', 'C', NULL, '2019-04-05 09:35:24', '2019-04-05 09:35:24'),
(58, 40, 40, 25, '5', 'C', NULL, '2019-04-05 09:35:25', '2019-04-05 09:35:25'),
(59, 40, 40, 25, '6', 'C', NULL, '2019-04-05 09:35:26', '2019-04-05 09:35:26'),
(60, 40, 40, 25, 'i7', 'C', NULL, '2019-04-05 09:35:28', '2019-04-05 09:35:28'),
(61, 40, 40, 25, 'i8', 'C', NULL, '2019-04-05 09:35:30', '2019-04-05 09:35:30'),
(62, 40, 40, 25, 'i9', 'C', NULL, '2019-04-05 09:35:32', '2019-04-05 09:35:32'),
(63, 43, 30, 24, NULL, 'L', NULL, '2019-04-08 07:28:11', '2019-04-08 07:28:11'),
(64, 43, 43, 27, NULL, 'L', NULL, '2019-04-08 07:30:02', '2019-04-08 07:30:02'),
(65, 43, 43, 31, 'hi', 'C', NULL, '2019-04-08 07:47:33', '2019-04-08 07:47:33'),
(66, 43, 43, 31, 'ws you', 'C', NULL, '2019-04-08 07:47:35', '2019-04-08 07:47:35'),
(67, 43, 40, 25, '8-4-2019', 'C', NULL, '2019-04-08 07:48:13', '2019-04-08 07:48:13'),
(68, 43, 44, 34, 'Hi Bob, How are you doing?', 'C', NULL, '2019-04-08 08:39:27', '2019-04-08 08:39:27'),
(69, 43, 44, 34, 'Are you free Today?', 'C', NULL, '2019-04-08 08:39:36', '2019-04-08 08:39:36'),
(70, 43, 44, 34, 'Are you free bob\n?', 'C', NULL, '2019-04-08 08:39:48', '2019-04-08 08:39:48'),
(71, 44, 43, 36, 'comment by bob on alice post', 'C', NULL, '2019-04-08 09:10:42', '2019-04-08 09:10:42'),
(72, 44, 44, 39, 'Hi Alice, Can you please help me.', 'C', NULL, '2019-04-08 09:22:38', '2019-04-08 09:22:38'),
(73, 44, 44, 40, 'ABCD', 'C', NULL, '2019-04-08 09:25:46', '2019-04-08 09:25:46'),
(74, 43, 43, 36, NULL, 'L', NULL, '2019-04-08 09:28:01', '2019-04-08 09:28:01'),
(75, 46, 46, 41, 'Paul still me waiting for you?', 'C', NULL, '2019-04-08 09:32:48', '2019-04-08 09:32:48'),
(76, 47, 47, 42, 'Hi Heena, You can please go nOW?', 'C', NULL, '2019-04-08 09:36:03', '2019-04-08 09:36:03'),
(77, 46, 46, 43, 'hi 123', 'C', NULL, '2019-04-08 09:39:29', '2019-04-08 09:39:29'),
(78, 47, 47, 42, 'hi paul', 'C', NULL, '2019-04-08 09:40:24', '2019-04-08 09:40:24'),
(79, 47, 44, 44, NULL, 'L', NULL, '2019-04-08 10:12:47', '2019-04-08 10:12:47'),
(80, 47, 46, 41, NULL, 'L', NULL, '2019-04-08 10:13:25', '2019-04-08 10:13:25'),
(81, 49, 48, 45, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humor, or randomized words which don\'t look even slightly believable.', 'C', NULL, '2019-04-08 10:30:15', '2019-04-08 10:30:15'),
(82, 48, 48, 45, 'Where can I get some?', 'C', NULL, '2019-04-08 10:31:12', '2019-04-08 10:31:12'),
(83, 48, 44, 44, 'Where can I get some?', 'C', NULL, '2019-04-08 10:32:05', '2019-04-08 10:32:05'),
(84, 48, 49, 46, 'Where can I get some?', 'C', NULL, '2019-04-08 10:37:42', '2019-04-08 10:37:42'),
(85, 48, 42, 0, NULL, 'F', NULL, '2019-04-08 10:40:56', '2019-04-08 10:40:56'),
(86, 49, 43, 0, NULL, 'F', NULL, '2019-04-08 10:41:09', '2019-04-08 10:41:09'),
(87, 49, 49, 46, NULL, 'L', NULL, '2019-04-08 10:42:56', '2019-04-08 10:42:56'),
(88, 49, 48, 45, NULL, 'L', NULL, '2019-04-08 10:43:13', '2019-04-08 10:43:13'),
(89, 48, 49, 46, NULL, 'L', NULL, '2019-04-08 10:43:57', '2019-04-08 10:43:57'),
(90, 42, 42, 26, NULL, 'L', NULL, '2019-04-08 11:41:34', '2019-04-08 11:41:34'),
(91, 49, 49, 46, 'Hi', 'C', NULL, '2019-04-08 11:52:05', '2019-04-08 11:52:05'),
(92, 49, 49, 46, 'you are Fine?', 'C', NULL, '2019-04-08 11:52:11', '2019-04-08 11:52:11'),
(93, 49, 49, 46, 'at are you doing now a daya?', 'C', NULL, '2019-04-08 11:52:17', '2019-04-08 11:52:17'),
(94, 49, 49, 46, 'can you please discuss about your hobbies and interests?', 'C', NULL, '2019-04-08 11:52:35', '2019-04-08 11:52:35'),
(95, 49, 49, 46, 'Do you have any knowledge about Leatest Trends towards Technology?', 'C', NULL, '2019-04-08 11:52:54', '2019-04-08 11:52:54'),
(96, 48, 42, 0, NULL, 'F', NULL, '2019-04-08 12:30:24', '2019-04-08 12:30:24'),
(97, 48, 49, 0, NULL, 'F', NULL, '2019-04-08 12:32:20', '2019-04-08 12:32:20'),
(98, 49, 49, 47, NULL, 'L', NULL, '2019-04-08 12:57:13', '2019-04-08 12:57:13'),
(99, 49, 49, 47, 'hi', 'C', NULL, '2019-04-08 12:57:44', '2019-04-08 12:57:44'),
(100, 49, 49, 47, 'u', 'C', NULL, '2019-04-08 12:57:45', '2019-04-08 12:57:45'),
(101, 49, 49, 47, 'at u doing', 'C', NULL, '2019-04-08 12:57:50', '2019-04-08 12:57:50'),
(102, 49, 49, 47, 'here r u', 'C', NULL, '2019-04-08 12:57:53', '2019-04-08 12:57:53'),
(103, 49, 49, 47, 'u fine?', 'C', NULL, '2019-04-08 12:57:57', '2019-04-08 12:57:57'),
(104, 49, 49, 47, 'll me pleasse', 'C', NULL, '2019-04-08 12:58:01', '2019-04-08 12:58:01'),
(105, 30, 39, 0, '', 'F', NULL, '2019-04-09 04:37:17', '2019-04-09 04:48:03'),
(106, 39, 30, 0, NULL, 'F', NULL, '2019-04-09 04:48:26', '2019-04-09 04:48:26'),
(107, 30, 39, 0, NULL, 'F', NULL, '2019-04-09 04:49:07', '2019-04-09 04:49:07');

-- --------------------------------------------------------

--
-- Table structure for table `notification_settings`
--

CREATE TABLE `notification_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `notification_type` enum('F','C','L') COLLATE utf8_unicode_ci NOT NULL COMMENT 'F = follow, C = comment, L = like',
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notification_settings`
--

INSERT INTO `notification_settings` (`id`, `user_id`, `notification_type`, `status`, `created_at`, `updated_at`) VALUES
(25, 30, 'L', 1, '2019-04-03 13:36:54', '2019-04-03 13:36:54'),
(26, 30, 'C', 1, '2019-04-03 13:36:54', '2019-04-03 13:36:54'),
(27, 30, 'F', 1, '2019-04-03 13:36:54', '2019-04-08 07:23:05'),
(28, 31, 'L', 1, '2019-04-04 06:30:47', '2019-04-04 06:30:47'),
(29, 31, 'C', 1, '2019-04-04 06:30:47', '2019-04-04 06:30:47'),
(30, 31, 'F', 1, '2019-04-04 06:30:47', '2019-04-04 06:33:41'),
(31, 32, 'L', 1, '2019-04-04 06:51:32', '2019-04-04 06:51:32'),
(32, 32, 'C', 1, '2019-04-04 06:51:32', '2019-04-04 06:51:32'),
(33, 32, 'F', 1, '2019-04-04 06:51:32', '2019-04-04 06:51:32'),
(34, 32, 'L', 1, '2019-04-04 06:51:52', '2019-04-04 06:51:52'),
(35, 32, 'C', 1, '2019-04-04 06:51:52', '2019-04-04 06:51:52'),
(36, 32, 'F', 1, '2019-04-04 06:51:52', '2019-04-04 06:51:52'),
(37, 32, 'L', 1, '2019-04-04 06:53:30', '2019-04-04 06:53:30'),
(38, 32, 'C', 1, '2019-04-04 06:53:31', '2019-04-04 06:53:31'),
(39, 32, 'F', 1, '2019-04-04 06:53:31', '2019-04-04 06:53:31'),
(40, 33, 'L', 1, '2019-04-04 07:42:16', '2019-04-04 07:42:16'),
(41, 33, 'C', 1, '2019-04-04 07:42:16', '2019-04-04 07:42:16'),
(42, 33, 'F', 1, '2019-04-04 07:42:16', '2019-04-04 07:42:16'),
(43, 34, 'L', 1, '2019-04-04 08:34:33', '2019-04-04 08:34:33'),
(44, 34, 'C', 1, '2019-04-04 08:34:33', '2019-04-04 08:34:33'),
(45, 34, 'F', 1, '2019-04-04 08:34:33', '2019-04-04 08:34:33'),
(46, 35, 'L', 1, '2019-04-04 11:37:50', '2019-04-04 11:37:50'),
(47, 35, 'C', 1, '2019-04-04 11:37:50', '2019-04-04 11:37:50'),
(48, 35, 'F', 1, '2019-04-04 11:37:50', '2019-04-04 11:37:50'),
(49, 36, 'L', 1, '2019-04-04 12:54:44', '2019-04-04 12:54:44'),
(50, 36, 'C', 1, '2019-04-04 12:54:44', '2019-04-04 12:54:44'),
(51, 36, 'F', 1, '2019-04-04 12:54:44', '2019-04-04 12:54:44'),
(52, 37, 'L', 1, '2019-04-04 13:01:29', '2019-04-04 13:01:29'),
(53, 37, 'C', 1, '2019-04-04 13:01:29', '2019-04-04 13:01:29'),
(54, 37, 'F', 1, '2019-04-04 13:01:29', '2019-04-04 13:01:29'),
(55, 39, 'L', 1, '2019-04-05 07:29:03', '2019-04-05 07:29:03'),
(56, 39, 'C', 1, '2019-04-05 07:29:03', '2019-04-05 07:29:03'),
(57, 39, 'F', 1, '2019-04-05 07:29:03', '2019-04-05 07:29:03'),
(58, 40, 'L', 1, '2019-04-05 09:08:40', '2019-04-05 09:08:40'),
(59, 40, 'C', 1, '2019-04-05 09:08:40', '2019-04-05 09:08:40'),
(60, 40, 'F', 1, '2019-04-05 09:08:40', '2019-04-05 09:08:40'),
(61, 41, 'L', 1, '2019-04-05 09:08:59', '2019-04-05 09:08:59'),
(62, 41, 'C', 1, '2019-04-05 09:08:59', '2019-04-05 09:11:50'),
(63, 41, 'F', 1, '2019-04-05 09:08:59', '2019-04-05 09:08:59'),
(64, 42, 'L', 1, '2019-04-05 10:17:43', '2019-04-05 10:17:43'),
(65, 42, 'C', 1, '2019-04-05 10:17:43', '2019-04-05 10:17:43'),
(66, 42, 'F', 1, '2019-04-05 10:17:43', '2019-04-05 10:17:43'),
(67, 43, 'L', 1, '2019-04-08 07:25:54', '2019-04-08 07:25:54'),
(68, 43, 'C', 1, '2019-04-08 07:25:54', '2019-04-08 09:22:08'),
(69, 43, 'F', 1, '2019-04-08 07:25:54', '2019-04-08 07:25:54'),
(70, 44, 'L', 1, '2019-04-08 08:31:51', '2019-04-08 08:31:51'),
(71, 44, 'C', 1, '2019-04-08 08:31:51', '2019-04-08 08:31:51'),
(72, 44, 'F', 1, '2019-04-08 08:31:51', '2019-04-08 08:31:51'),
(73, 45, 'L', 1, '2019-04-08 09:09:17', '2019-04-08 09:09:17'),
(74, 45, 'C', 1, '2019-04-08 09:09:17', '2019-04-08 09:09:17'),
(75, 45, 'F', 1, '2019-04-08 09:09:17', '2019-04-08 09:09:17'),
(76, 46, 'L', 1, '2019-04-08 09:30:45', '2019-04-08 09:30:45'),
(77, 46, 'C', 1, '2019-04-08 09:30:45', '2019-04-08 09:30:45'),
(78, 46, 'F', 1, '2019-04-08 09:30:45', '2019-04-08 09:30:45'),
(79, 47, 'L', 1, '2019-04-08 09:34:06', '2019-04-08 09:34:06'),
(80, 47, 'C', 1, '2019-04-08 09:34:06', '2019-04-08 09:34:06'),
(81, 47, 'F', 1, '2019-04-08 09:34:06', '2019-04-08 09:34:06'),
(82, 49, 'L', 1, '2019-04-08 10:27:56', '2019-04-08 10:27:56'),
(83, 49, 'C', 1, '2019-04-08 10:27:56', '2019-04-08 10:40:14'),
(84, 49, 'F', 1, '2019-04-08 10:27:56', '2019-04-08 10:27:56'),
(85, 48, 'L', 1, '2019-04-08 10:28:53', '2019-04-08 10:28:53'),
(86, 48, 'C', 1, '2019-04-08 10:28:53', '2019-04-08 10:40:05'),
(87, 48, 'F', 1, '2019-04-08 10:28:53', '2019-04-08 10:28:53'),
(88, 50, 'L', 1, '2019-04-09 12:15:15', '2019-04-09 12:15:15'),
(89, 50, 'C', 1, '2019-04-09 12:15:15', '2019-04-09 12:15:15'),
(90, 50, 'F', 1, '2019-04-09 12:15:15', '2019-04-09 12:15:15'),
(91, 51, 'L', 1, '2019-04-09 22:52:31', '2019-04-09 22:52:31'),
(92, 51, 'C', 1, '2019-04-09 22:52:31', '2019-04-09 22:52:31'),
(93, 51, 'F', 1, '2019-04-09 22:52:31', '2019-04-09 22:52:31'),
(94, 52, 'L', 1, '2019-04-10 04:03:49', '2019-04-10 04:03:49'),
(95, 52, 'C', 1, '2019-04-10 04:03:49', '2019-04-10 04:03:49'),
(96, 52, 'F', 1, '2019-04-10 04:03:49', '2019-04-10 04:03:49'),
(97, 53, 'L', 1, '2019-04-10 07:42:17', '2019-04-10 07:42:17'),
(98, 53, 'C', 1, '2019-04-10 07:42:17', '2019-04-10 07:42:17'),
(99, 53, 'F', 1, '2019-04-10 07:42:17', '2019-04-10 07:42:17');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `shares`
--

CREATE TABLE `shares` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `receiver_id` int(11) NOT NULL COMMENT 'event or news feed id',
  `receiver_type` enum('E','N') COLLATE utf8_unicode_ci NOT NULL COMMENT 'E = event, N = news feed',
  `status` tinyint(1) NOT NULL COMMENT '1 = shared, 0 = notshared',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country_code` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `country_code`, `status`, `created_at`, `updated_at`) VALUES
(1, 'New South Wales', '12345', 1, '2019-04-02 06:02:52', '2019-04-03 09:41:40'),
(2, 'Queensland', '12343', 1, '2019-04-02 06:02:52', '2019-04-02 06:02:52'),
(3, 'South Australia', '123445', 1, '2019-04-03 09:42:13', '2019-04-03 09:42:13'),
(4, 'Tasmania', '23344455', 1, '2019-04-03 09:42:55', '2019-04-03 09:42:55'),
(5, 'Victoria', '2345433', 1, '2019-04-03 09:42:55', '2019-04-03 09:42:55'),
(6, 'Western Australia', '3434532', 1, '2019-04-03 09:43:23', '2019-04-03 09:43:23');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gmail_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkdin_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `register_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'F = facebook, G = gmail, L = linkedIn, E = email',
  `user_type` enum('A','U') COLLATE utf8_unicode_ci NOT NULL COMMENT 'A = admin, U = normal user',
  `status` tinyint(1) NOT NULL COMMENT '0=>Inactive, 1=>Active',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `email`, `password`, `image`, `facebook_id`, `gmail_id`, `linkdin_id`, `register_type`, `user_type`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Leena', 'Admin', 'admin@gmail.com', '$2y$10$YCXFwH/2EqFVH.KDIjNeQOA0z0zXmJiKfgcSfmpxYZ95/6rPdsRTe', '1554442181f1.jpeg', NULL, NULL, NULL, NULL, 'A', 1, 'M4XSWB8DTpaTS2Yqakj8ra7xDGgeeabMhstqy3pBWaTLpucRzW85Wy4jketK', NULL, '2019-04-09 22:49:51'),
(30, 'yamini', 'dudeja', 'yamini@gmail.com', '$2y$10$qT6m.Qce9nqKHGvepiYqbegSw2mdCKD81C3utbB9WT.qLYEER.4QG', '1554298614teacher.png', NULL, NULL, NULL, 'E', 'U', 1, '8rTGveyEecshX0NlPYE7GPgOxTHJSdp4iJkRMlr2KkmJvn14DaATSB4n82UA', '2019-04-03 13:36:15', '2019-04-08 09:07:15'),
(31, 'Harry', 'Singh', 'harry@yopmail.com', '$2y$10$.ppovELIHBRwxPEDuXFfV.Lx.Ql9fmNWu5sQdBPok9RvWMv7CYr4C', '1554359447harry.jpeg', NULL, NULL, NULL, 'E', 'U', 1, 'GZCbAkUxh10yvGOIHbJLTe2KyHxOgXO5updy65PA7QYNUISFj0lNckhbjoEq', '2019-04-04 06:26:51', '2019-04-04 07:12:43'),
(32, 'diksha', 'mangala', 'diksha1@gmail.com', '$2y$10$KkpTVgzIiURFi75eQAxbquzn71ahcNXctswu.3ppKd7eQTqY4h6xC', '1554360810Screenshot_20190318-222751~2.png', NULL, NULL, NULL, 'E', 'U', 1, NULL, '2019-04-04 06:50:47', '2019-04-04 06:53:30'),
(33, 'Shubham', 'Sharma', 'sharmashubh19915@gmail.com', '$2y$10$VOnhoEPjq.ZQn4r.UJheIeQqX/LGjH3Ki57SgvwcXm2jcPY0ktIUO', 'dummy.png', NULL, NULL, NULL, 'E', 'U', 1, 'rBNPlMpKKcUmtJqY8uwhhRtPwCXkKzA8dZO8qOsHLh9rkYRhG22DJC8sm46k', '2019-04-04 07:41:45', '2019-04-08 10:34:10'),
(34, 'yamini', 'dudeja', 'test@gmail.com', '$2y$10$ZVm5cqG1njND59DLadJY0On8ljRyfjoMqcoYQmiIAPNzDHtazC.dq', 'dummy.png', NULL, NULL, NULL, 'E', 'U', 1, 'apQmTaWbksnCKhGWd7wqvE0U8Zav7BXWH9wOuDbqlvxe8wuOKFya4TnLmFwJ', '2019-04-04 08:34:07', '2019-04-04 10:03:53'),
(35, 'diksha', 'farandise', 'admin123@gmail.com', '$2y$10$xjs/TYKkg9OC9F9Lj7/giO9N8iHYuaT/ffK7n.6CVTRk5vcc8AVZe', 'dummy.png', NULL, NULL, NULL, 'E', 'U', 1, NULL, '2019-04-04 11:37:32', '2019-04-04 11:37:50'),
(36, 'kanika', 'sura', 'kanika@gmail.com', '$2y$10$Nwm7JwS10odSrF9iShqlPee3ouKX6jrMYdrR96m4gnBRJzddM19Y6', '1554382484f1.jpeg', NULL, NULL, NULL, 'E', 'U', 1, 'wBsEMaYZ30hf2Chbu1lRx6y1A6cOT2LDg0QmAYL0HmkOaSSpx60urvQHp4Iv', '2019-04-04 12:53:25', '2019-04-04 12:58:34'),
(37, 'olivia', 'sinha', 'olivia@yopmail.com', '$2y$10$d.fxwaZd60SiMYXRVGN.ve4fgFmcnfTOH60JckkK75Xp9w0OatnQO', '1554382889olivia.jpeg', NULL, NULL, NULL, 'E', 'U', 1, 'jSn5gkOpdJFWbd3bByCSNp7yB3tLc4jN4QvVezNzTW1q6Az8kyt9dxDpb48e', '2019-04-04 13:00:14', '2019-04-04 13:07:44'),
(38, NULL, NULL, 'shubham@gmail.com', '$2y$10$4sRDxX3X4CVSn3yJzpOiI.dQPiw0MHyV2uld8iInhOmrEo5SgBLBm', NULL, NULL, NULL, NULL, 'E', 'U', 1, NULL, '2019-04-05 06:51:11', '2019-04-05 06:51:11'),
(39, 'molishka', 'meeu', 'molishka@yopmail.com', '$2y$10$RZSNNuZKPzmDSqn9KMz59u8zSFOd/71mtPmGy9cQZZ.38imSLWpfO', '1554449343f1.jpeg', NULL, NULL, NULL, 'E', 'U', 1, NULL, '2019-04-05 07:27:58', '2019-04-05 07:35:57'),
(40, 'inna', 'salaria', 'inna@yopmail.com', '$2y$10$E39UXE/Rao3ItJdMzIT6rObUQMWMEkkHVQsJOfk.D0Au.4IFGXTny', '1554455320inna.jpeg', NULL, NULL, NULL, 'E', 'U', 1, NULL, '2019-04-05 09:06:28', '2019-04-05 09:10:27'),
(41, 'yamini', 'dudeja', 'yaminidudeja@gmail.com', '$2y$10$T4Ewqi82DoIpKiTwTtmZ7OwOgjxeh0dCraRTpk2Pr5.12G300JW5i', '1554455339festival_head(1).sql', NULL, NULL, NULL, 'E', 'U', 1, 'DapZEPquTVkVtCbKKfTUHXjDYh3YawK2NM51Y13ppd0O5HyrCbNnG3xE4p8d', '2019-04-05 09:07:52', '2019-04-05 09:52:41'),
(42, 'john', 'farandise', 'kanu@gmail.com', '$2y$10$JsVX.nP5sje7LlBw1AGmXeUX1Q7RVueXcbkm/vKziMZv1P5n92Vji', '1554459463login_bg.png', NULL, NULL, NULL, 'E', 'U', 1, 'BtOadz2N5oLF7WSbhhTK3MdufQUEQH79sc1sMozLGLuw9DKZOV6mX4zmuh9A', '2019-04-05 10:16:57', '2019-04-05 11:24:30'),
(43, 'Alice', 'Luee', 'alice@yopmail.com', '$2y$10$vEWTm47HnDMYQsSfygd8VeBOzNSoPmcjFXfMGn1Kky0z8jOky19CC', '1554708353inna.jpeg', NULL, NULL, NULL, 'E', 'U', 1, 'HQMYAjHaGb024BFZQfju5TRJC7m0qXGhJdSMRqTf3WjkXbwdfvy60quy6Gy0', '2019-04-08 07:25:06', '2019-04-08 13:28:35'),
(44, 'bob', 'sinha', 'bob@yopmail.com', '$2y$10$0zqQGvTWQGiFD5NkufBhh.Qc6fxG8TLU8BTl4d6ACbMYrp72lyTfO', '1554712311m2.jpeg', NULL, NULL, NULL, 'E', 'U', 1, 'JMkeSgGs8yTDllJNdVPtVYNIO2xqjlLMZeYCKlTiYTBAE1waRnwyiDM7YAaw', '2019-04-08 08:31:12', '2019-04-08 10:26:51'),
(45, 'Shubham', 'Sharma', 'shubham@mailinator.com', '$2y$10$PaMXHkL5.sRtcbmCLhKc0ufNSOSMtiVKQLvz4ESP.egVkBmuP5czG', 'dummy.png', NULL, NULL, NULL, 'E', 'U', 1, 'CJle75XWmDaXkxfpMO8AC0yqBjRHqUJ0gFhYBookCB9uvAph9QRCwr64LPj4', '2019-04-08 09:08:51', '2019-04-08 09:09:29'),
(46, 'Heena', 'Sayal', 'heena@yopmail.com', '$2y$10$kBuKlDchSvaF3EURlLMP8OEzS9fKYAcRfabxnxC2kejRkj22/39ji', '1554715845inna.jpeg', NULL, NULL, NULL, 'E', 'U', 1, '2eSLjK9ntw9ewpFYVWVVnA2LsJGGXEPq6lxVMlcGe5jH0zGO3DQF5GFBBQWK', '2019-04-08 09:29:53', '2019-04-08 09:44:40'),
(47, 'Paul', 'Sandhu', 'Paul@yopmail.com', '$2y$10$.Xc0d85eaTsvlhAq3W3GQuPO4GkyMK8eIDsO1PBWwSCzq2DgyrBcS', 'dummy.png', NULL, NULL, NULL, 'E', 'U', 1, 'qN8RqC0KRfFibKeOmowiOIUh3bRIZudbH8oUrbuveGM1mAp2UHX61QNx99j4', '2019-04-08 09:33:22', '2019-04-08 12:06:29'),
(48, 'Robin', 'Rick', 'bb@yopmail.com', '$2y$10$PE5epCy.9fFlFJ26ZTl76ek.wTIuoSWvuLdgvsHciEoIPtCyx532i', '1554719333restaurant8.jpg', NULL, NULL, NULL, 'E', 'U', 1, '0AoU8M5e2L7cqM6UKofQD4XjsbAL3iDy8YZTq4FCcpR1ubUFtQctPocNXCu9', '2019-04-08 10:27:12', '2019-04-08 13:32:51'),
(49, 'Ashley', 'Sinha', 'ashley@yopmail.com', '$2y$10$K7xZHrIonFDnArJOcr3/9.eX97R/6rSZd06xT.4DyYEhEnAploCfS', '1554719276m1.jpeg', NULL, NULL, NULL, 'E', 'U', 1, 'tXdb95OU25YnEY8IC9TBz66cR5I9cvwijqyJdk8tqASIQ66hayJJ2Cyzquwp', '2019-04-08 10:27:18', '2019-04-08 12:26:41'),
(50, 'Rahul', 'Sharma', 'rahul@yopmail.com', '$2y$10$mDx3ZsSMvo9AODRWlFUixOPS9r8XWqDRr2xAWH0BSudpx..fsHhQ.', 'dummy.png', NULL, NULL, NULL, 'E', 'U', 1, NULL, '2019-04-09 12:14:51', '2019-04-09 12:15:15'),
(51, 'Jake', 'Jayasuria', 'jakejayasplastering@hotmail.com', '$2y$10$rukeZf5ALxiTVBlsmS/Vx.tHODblJQjshXe2ry2jAes/lJoyjle/e', 'dummy.png', NULL, NULL, NULL, 'E', 'U', 1, NULL, '2019-04-09 22:51:33', '2019-04-09 22:52:30'),
(52, 'jack', 'watson', 'jack@yopmail.com', '$2y$10$pFnhkB2YeJnMxV.LlFfgOuAgsTec3SmplPFNo.ZaWytLPZG.H6zuq', '1554869029image3.jpg', NULL, NULL, NULL, 'E', 'U', 1, 'BpeOhxzQgNWwFIyVJesrOH5VyulKeAW83H6vmdEZuxw89I6MIHnXKiB2xJRQ', '2019-04-10 04:03:15', '2019-04-10 04:03:56'),
(53, 'Tom', 'Jack', 'jakejayaplastering@hotmail.com', '$2y$10$WcdQyPMicFXvBL7r/0FHIOHcVgux1DR8/p50nAyCjEWmm5ckgalR.', 'dummy.png', NULL, NULL, NULL, 'E', 'U', 1, NULL, '2019-04-10 07:41:04', '2019-04-10 07:42:17');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `nick_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `gender` enum('Female','Male') COLLATE utf8_unicode_ci NOT NULL,
  `genre_id` int(11) NOT NULL,
  `artists_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `marital_status` enum('Married','Unmarried','In relationship') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `user_id`, `nick_name`, `age`, `gender`, `genre_id`, `artists_id`, `state_id`, `marital_status`, `created_at`, `updated_at`) VALUES
(22, 30, 'yamini', 56, 'Female', 2, 2, 5, 'Unmarried', '2019-04-03 13:36:54', '2019-04-05 07:22:46'),
(23, 2, 'Liu', 23, 'Female', 2, 2, 5, 'Unmarried', '2019-04-04 04:00:18', '2019-04-05 06:15:05'),
(24, 31, 'hiu', 24, 'Male', 2, 6, 6, 'Married', '2019-04-04 06:30:47', '2019-04-04 06:33:25'),
(25, 32, 'dikshu', 26, 'Female', 0, 2, 2, 'Married', '2019-04-04 06:51:32', '2019-04-04 06:51:32'),
(26, 32, 'dikshu', 26, 'Female', 0, 2, 2, 'Married', '2019-04-04 06:51:52', '2019-04-04 06:51:52'),
(27, 32, 'dikshu', 26, 'Female', 0, 2, 5, 'Married', '2019-04-04 06:53:30', '2019-04-04 06:53:30'),
(28, 33, 'Shubh', 25, 'Male', 1, 1, 3, 'Unmarried', '2019-04-04 07:42:16', '2019-04-04 07:42:16'),
(29, 34, 'yamini', 22, 'Female', 2, 2, 2, 'Unmarried', '2019-04-04 08:34:33', '2019-04-04 08:34:33'),
(30, 35, 'ds', 22, '', 1, 7, 1, 'Unmarried', '2019-04-04 11:37:50', '2019-04-04 11:37:50'),
(31, 36, 'kani', 34, 'Female', 1, 2, 1, 'Married', '2019-04-04 12:54:44', '2019-04-04 12:54:44'),
(32, 37, 'oli', 21, 'Female', 4, 7, 4, 'In relationship', '2019-04-04 13:01:29', '2019-04-04 13:01:29'),
(33, 39, 'molly', 23, 'Female', 2, 12, 4, '', '2019-04-05 07:29:03', '2019-04-05 07:35:57'),
(34, 40, 'inni', 21, 'Female', 4, 8, 5, 'In relationship', '2019-04-05 09:08:40', '2019-04-05 09:08:40'),
(35, 41, 'yamini', 22, 'Female', 1, 5, 2, 'Unmarried', '2019-04-05 09:08:59', '2019-04-05 09:08:59'),
(36, 42, 'dd', 22, '', 1, 1, 1, 'Unmarried', '2019-04-05 10:17:43', '2019-04-05 10:17:43'),
(37, 43, 'Ali', 19, 'Female', 6, 8, 5, 'Unmarried', '2019-04-08 07:25:54', '2019-04-08 07:25:54'),
(38, 44, 'bobby', 29, 'Male', 2, 2, 1, 'Married', '2019-04-08 08:31:51', '2019-04-08 08:31:51'),
(39, 45, 'Shubh', 22, 'Female', 1, 14, 5, 'Married', '2019-04-08 09:09:17', '2019-04-08 09:09:17'),
(40, 46, 'Hiu', 35, 'Female', 6, 7, 6, 'Married', '2019-04-08 09:30:45', '2019-04-08 09:30:45'),
(41, 47, 'Piyu', 60, 'Male', 2, 8, 3, 'Married', '2019-04-08 09:34:06', '2019-04-08 09:34:06'),
(42, 49, 'ash', 23, 'Male', 2, 2, 4, '', '2019-04-08 10:27:56', '2019-04-08 12:26:41'),
(43, 48, 'Rick', 29, 'Male', 2, 2, 4, 'Unmarried', '2019-04-08 10:28:53', '2019-04-08 10:28:53'),
(44, 50, 'Rahul', 22, 'Male', 9, 13, 3, 'Unmarried', '2019-04-09 12:15:15', '2019-04-09 12:15:15'),
(45, 51, 'JJ', 32, '', 0, 0, 0, '', '2019-04-09 22:52:30', '2019-04-09 22:52:30'),
(46, 52, 'jack', 28, 'Male', 2, 1, 1, 'Married', '2019-04-10 04:03:49', '2019-04-10 04:03:49'),
(47, 53, 'Tj', 27, 'Male', 2, 7, 2, 'Unmarried', '2019-04-10 07:42:17', '2019-04-10 07:42:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artists`
--
ALTER TABLE `artists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blocked_users`
--
ALTER TABLE `blocked_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactuses`
--
ALTER TABLE `contactuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_uses`
--
ALTER TABLE `contact_uses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feeds`
--
ALTER TABLE `feeds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `follows`
--
ALTER TABLE `follows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_settings`
--
ALTER TABLE `notification_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `shares`
--
ALTER TABLE `shares`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artists`
--
ALTER TABLE `artists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `blocked_users`
--
ALTER TABLE `blocked_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `contactuses`
--
ALTER TABLE `contactuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `contact_uses`
--
ALTER TABLE `contact_uses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `feedbacks`
--
ALTER TABLE `feedbacks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `feeds`
--
ALTER TABLE `feeds`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `follows`
--
ALTER TABLE `follows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `genres`
--
ALTER TABLE `genres`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `notification_settings`
--
ALTER TABLE `notification_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `shares`
--
ALTER TABLE `shares`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
