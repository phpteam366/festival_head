var placeSearch, autocomplete;




function autoComplete(){
    autocomplete = new google.maps.places.Autocomplete(
    /** @type {!HTMLInputElement} */
    (document.getElementById('event_location')), {
      types: ['geocode']
    });

  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
   autocomplete.addListener('place_changed',  function () {
      var place = autocomplete.getPlace();
      document.getElementById('city2').value = place.name;
      document.getElementById('cityLat').value = place.geometry.location.lat();
      document.getElementById('cityLng').value = place.geometry.location.lng();
      searchFilter(place);
    });
}

function searchFilter(place){
  $(".search_with_location").submit();
    /*document.getElementById('city2').value = place.name;
    document.getElementById('cityLat').value = place.geometry.location.lat();
    document.getElementById('cityLng').value = place.geometry.location.lng();

    var event_lat  =  place.geometry.location.lat();
    var event_long =   place.geometry.location.lng();
    $.ajax({
        type:'GET',
        url: baseUrl+'/events/filter?event_lat='+event_lat+'&event_long='+event_long,
        cache:false,
        success:function(response){
            alert(response);
        },
        error:function(error){
             alert(error);
        }
    });*/
}
