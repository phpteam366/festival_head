if($(".validate-me").length > 0){
    $(".validate-me").validate();
}

   if($(".validate_me_newsfeed").length > 0){
        $(".validate_me_newsfeed").validate({
            errorPlacement: function(error, element) {
                if (element.attr("name") == "files") {
                    error.insertAfter(".file_err");
                }
                else{
                    error.insertAfter(element);
                }
              }
        });
    }




if($(".state_seletced").length > 0){
    $('.state_seletced').on('change', function() {
           var stateId = jQuery(this).val();
           if(stateId != ""){
                $.ajax({
                    url: baseUrl+"/get_cities?state_id="+stateId,
                    method:"get",
                    success:function(response) {
                         if(response != ""){
                            $(".city_list").html(response);
                         }else{
                            $(".city_list").html('');
                         }
                    },
                      error:function(status,res){
                        console.log(status);
                      }
                });
           }else{
            $(".city_list").html('<option value="">Select City</option>');
           }
    });
}




if($(".inputfileChoose").length > 0){
  $(".inputfileChoose").change(function(){
      var mimeType=this.files[0]['type'];
      var type= mimeType.split('/')[0];
      if(type == 'image' || type == 'video'){
        var file = this.files[0];

        var _URL = window.URL;
         var img = new Image();
         img.onload = function () {
            var widthImg = this.width;
            var heightImg = this.height;
           if(widthImg < 960 && heightImg < 400){
             $(".file_err").html("<label class='error'>Please upload image with width more than 960px and height 400px</label>");
             $(".inputfileChoose").val('');
             $(".file_chosen").addClass("hide");
             return false;
           }
            //alert("Width:" + this.width + "   Height: " + this.height);//this will give you image width and height and you can easily validate here....
        };
        img.src = _URL.createObjectURL(file);
       


            $(".file_chosen .progress-bar").css("width","0%");
            $(".file_chosen").removeClass("hide");
            $(".file_chosen .progress-bar").html("Uploading");
            var start=1;
            var interval = setInterval(function(){
                start = start+0.2;
                if(start >= 100){
                    $(".file_chosen .progress-bar").css("width","100%");
                    //$(".progress_passport .progress-bar").html("Uploaded");
                    clearInterval(interval);
                    $(".file_chosen .progress-bar").html("File Uploaded");
                }else{
                    $(".file_chosen .progress-bar").css("width",start+"%");
                    $(".file_chosen .progress-bar").html(Math.floor(start)+'%');
                }
            },10);
        }else{
            $(".inputfileChoose").val('');
        }
       
   });
}


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
if($("#imageUpload").length > 0){
	$("#imageUpload").change(function() {
		readURL(this);
	});
}



function refresh_message_auto(){
   var chat_id = jQuery(".message_chat_id").val();
   var last_record  = jQuery(".msg_chat_messages").find('.left_chatbox').last().attr("data-message-id");

   $.ajax({
            url: baseUrl+"/message/get_messages_auto",
            method:"get",
            data : {chat_id:chat_id,last_record:last_record},
            success:function(response) {
             if(response != ""){
                jQuery(".msg_chat_messages").append(response);
                 $(".chat_box").animate({ scrollTop: 20000000 }, "slow");
             }
          },
          error:function(status,res){
            console.log(status);
          }
        });
}

if($(".msg_chat_messages").length > 0){
    /* Use firebase here--*/
window.setInterval(function(){
    refresh_message_auto();
}, 5000);
}



$(function() {
    $('.no-space').on('keypress', function(e) {
        if (e.which == 32)
            return false;
    });
    
});
/* allow Only digits--*/
if($(".input-digits").length > 0){
    $(".input-digits").keypress(function (e) {
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
    }
   });
}


/* Google Autocomplete address--*/
/*if($("#event_location").length > 0){
    initialize();
    function initialize() {
        var input = document.getElementById('event_location');
        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            document.getElementById('city2').value = place.name;
            document.getElementById('cityLat').value = place.geometry.location.lat();
            document.getElementById('cityLng').value = place.geometry.location.lng();
            //alert("This function is working!");
            //alert(place.name);
           // alert(place.address_components[0].long_name);

        });
    }
    google.maps.event.addDomListener(window, 'load', initialize); 
}*/

function setUpHeader(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
}

function likeEvent(){
    var userId = $("#userId").val();
    var eventId = $("#eventId").val();
    setUpHeader();
    $.ajax({
        type:'POST',
        url: baseUrl+'/like',
        data: 'user_id='+userId+'&event_id='+eventId+'&receiver_type=E',
        cache:false,
        success:function(response){
            $("#like").html(response.like);
            $(".like_event").addClass("hide");
            $(".unlike_event").removeClass("hide");
        },
        error:function(response){

        }
    });
}

function unlikeEvent(){
    var userId = $("#userId").val();
    var eventId = $("#eventId").val();
    setUpHeader();
    $.ajax({
        type:'POST',
        url: baseUrl+'/unlike',
        data: 'user_id='+userId+'&event_id='+eventId+'&receiver_type=E',
        cache:false,
        success:function(response){
            $("#like").html(response.like);

            $(".unlike_event").addClass("hide");
            $(".like_event").removeClass("hide");
            
        },
        error:function(response){

        }
    });
}

if($(".filter_events_home").length > 0){
    $(".filter_events_home").click(function(){
       var eventSelected = $(".select_event").val();
       var selectGenere  = $(".select_genere").val();
       if(eventSelected == "" && selectGenere == ""){
            return false;
       }else{
            var url = baseUrl+"/events/filter?event="+eventSelected+"&genere="+selectGenere;
            window.location.href=url;
       }
    });
}
function favouriteEvent(eventId){
    var userId = $("#userId").val();
    setUpHeader();
    $.ajax({
        type:'POST',
        url: baseUrl+'/favouriteEvent',
        data: 'user_id='+userId+'&event_id='+eventId,
        cache:false,
        success:function(response){

        },
        error:function(response){

        }
    });
}

function unfavouriteEvent(eventId){
    var userId = $("#userId").val();
    setUpHeader();
    $.ajax({
        type:'POST',
        url: baseUrl+'/unfavouriteEvent',
        data: 'user_id='+userId+'&event_id='+eventId,
        cache:false,
        success:function(response){
            
        },
        error:function(response){

        }
    });
}


function eventComment(eventId){
    var userId = $("#userId").val();
    var comment = $("#comment").val();
    
   
    if(comment == ''){
        //alert('Please Enter Comment');
        return false;
    }
    setUpHeader();
    $.ajax({
        type:'POST',
        url: baseUrl+'/eventComment',
        data: 'event_id='+eventId+'&user_id='+userId+'&comment='+comment,
        cache:false,
        success:function(response){
            $("#comment").val('');
            //$("li #comment_section").append('<h1>hello</h1>');
        },
        error:function(response){

        }
    });
}

function block(userId){
    var blockedUserId = $("#blkedUsrId").val();
    setUpHeader();
    $.ajax({
        type:'POST',
        url: baseUrl+'/blockUser',
        data:'user_id='+userId+'&blocked_user_id='+blockedUserId,
        cache:false,
        success:function(response){

        },
        error:function(response){

        }
    });
}

function unblock(userId){
    var blockedUserId = $("#blkedUsrId").val();
    setUpHeader();
    $.ajax({
        type:'POST',
        url: baseUrl+'/unBlockUser',
        data:'sblocked_user_id='+blockedUserId,
        cache:false,
        success:function(response){

        },
        error:function(response){

        }
    });
}
$(".like_news").click(function(e){
    e.preventDefault();
    var feedId = $(this).data('news-id');
    var userId = $(this).data('user-id');
    setUpHeader();
     $(this).find('.fa').addClass("news_liked");
    $.ajax({
        type:'POST',
        url:baseUrl+'/likeNews',
        data:'feed_id='+feedId+'&receiver_id='+userId,
        cache:false,
        success:function(response){
            //alert(response.totalLikes);
            $("#total_like"+feedId).html(response.totalLikes);
        },
        error:function(response){
            
        }
    });
});


$("#unblock_user").click(function(){
    var unblockedUserId = $(this).data('blocked-user-id');
    //alert(unblockedUserId);
    setUpHeader();
    $.ajax({
        type:'POST',
        url:baseUrl+'/unBlockUser',
        data:'blocked_user_id='+unblockedUserId,
        cache:false,
        success:function(response){

        },
        error:function(response){

        }
    });
});

$(".news_comment").keyup(function(event){
    if (event.keyCode === 13) {
        var feedId = $(this).data('feed-id');
        var comment = $(this).val();
        var userId = $(this).data('user-id');
        //alert(feedId);
        setUpHeader();
        $.ajax({
            type:'POST',
            url: baseUrl+'/newsComment',
            data: 'feed_id='+feedId+'&comment='+comment+'&receiver_id='+userId,
            cache:false,
            success:function(response){
                $(".news_comment").val('');
                var totalCount = parseInt($(".total_count"+feedId).text())+1;
                $(".total_count"+feedId).text(totalCount);
            },
            error:function(response){

            }
        });
    }
});
if($(".submit_comment_news").length > 0){
    $(".submit_comment_news").click(function(){
            var feedId = $(this).data('feed-id');
            var comment = $(".news_comment_"+feedId).val();
            var userId = $(this).data('user-id');
            setUpHeader();
            $.ajax({
                type:'POST',
                url: baseUrl+'/newsComment',
                data: 'feed_id='+feedId+'&comment='+comment+'&receiver_id='+userId,
                cache:false,
                success:function(response){
                    $(".news_comment").val('');
                    var totalCount = parseInt($(".total_count"+feedId).text())+1;
                    $(".total_count"+feedId).text(totalCount);
                },
                error:function(response){

                }
            });
    });
}

$(".followed_you").click(function(){
    var checked = $(this).val();
    //alert(checked);
    setUpHeader();
    $.ajax({
        type:'POST',
        url:baseUrl+'/followSetting',
        data:'followed='+checked,
        cache:false,
        success:function(response){

        },
        error:function(response){

        }
    });
});

$(".replied_comment").click(function(){
    var checked = $(this).val();
    //alert(checked);
    setUpHeader();
    $.ajax({
        type:'POST',
        url:baseUrl+'/replyComment',
        data:'comment='+checked,
        cache:false,
        success:function(response){

        },
        error:function(response){

        }
    });
});

$(".like_post").click(function(){
    var checked = $(this).val();
    //alert(checked);
    setUpHeader();
    $.ajax({
        type:'POST',
        url:baseUrl+'/likePost',
        data:'like='+checked,
        cache:false,
        success:function(response){

        },
        error:function(response){

        }
    });
});
 
$(".follow").click(function(){
    var followingId = $(this).data('following-id');
    setUpHeader();
    $.ajax({
        type:'POST',
        url:baseUrl+'/follow',
        data:'following_id='+followingId,
        cache:false,
        success:function(response){
            $('#follower').html(response.followers);
	    $(".unfollow").removeClass("hide");
	    $(".follow").addClass("hide");
        },error:function(response){

        }
    });
});

$(".unfollow").click(function(){
    var followingId = $(this).data('following-id');
    setUpHeader();
    $.ajax({
        type:'POST',
        url:baseUrl+'/unfollow',
        data:'following_id='+followingId,
        cache:false,
        success:function(response){
            $('#follower').html(response.followers);
            $(".unfollow").addClass("hide");
	    $(".follow").removeClass("hide");
        },error:function(response){

        }
    });
});

$(".rating_review").click(function(e){
    e.preventDefault();
    var eventId = $(this).data('event-id');
    var formData = $(".feedback_for").serialize();
    $comment = $("#reviews").val();
    setUpHeader();
    $.ajax({
        type:'POST',
        url:baseUrl+'/feedback',
        data:formData+'&comment='+$comment+'&event_id='+eventId,
        cache:false,
        success:function(response){
            //alert('hi');
        },
        error:function(response){
            //alert('bye');
        }
    });
});

$(".feed_comment").click(function(){
    var feedId = $(this).data("news-id");
    console.log(feedId);
    setUpHeader();
    $.ajax({
        type:'POST',
        url:baseUrl+'/feedComments',
        data:'feed_id='+feedId,
        cache:false,
        success:function(response){
            if(response.data.length != "0"){
                    var html = "";
                    $.each(response.data, function(k, comment) {
                        html+="<li><div class='revw_detl_thumb'>";
                        html+="<div class='revw_thumb_img'>";
                        html+="<a href="+baseUrl+"/other-user-profile/"+comment.user_id+"><img src="+baseUrl+"/public/images/profile_image/"+comment.user_image+"></a>";
                        html+="</div>";
                        html+="<div class='ryt_info_detl'>";
                        html+="<div class='head_evnt'>";
                        html+="<h3><a href="+baseUrl+"/other-user-profile/"+comment.user_id+">"+comment.fname+" "+comment.lname+"</a></h3>";
                        //html+="<span>"+TimeElapsed+"::"+getElapsedTime(comment.created_at)+"</span>";
                        html+="</div>";
                        html+="<p>"+comment.comment+"</p>"
                        html+="</div>";
                        html+="</div></li>";
                                            
                    });
                    $("ul.comment_section").empty();
                    $("ul.comment_section").append(html);
                    $(".view_more").removeClass("hide");
            }else{
                var html = "<p class='text-center'>No comment for this feed</p>";
                $("ul.comment_section").empty();
                $("ul.comment_section").append(html);
                $(".view_more").addClass("hide");
            }
        },
        error:function(response){

        }
    });
});

$(".feed_share").click(function(){
    var feedId = $(this).data("news-id");
    $(".facebook_share").attr("href", "https://www.facebook.com/sharer/sharer.php?u="+baseUrl+"/news-feed-details/"+feedId);
    $(".google_share").attr("href", "https://plus.google.com/share?url="+baseUrl+"/news-feed-details/"+feedId);
    $(".linkedin_share").attr("href", "https://www.linkedin.com/sharing/share-offsite/?url="+baseUrl+"/news-feed-details/"+feedId+"&source=LinkedIn");
    //https://www.linkedin.com/sharing/share-offsite/?url=http://142.4.10.93/~vooap/festival_head/event-details/28&amp;source=LinkedIn
});



$(document).ready(function(){
      if($(".event_favourite").length > 0){
        $(document).on('click','.event_favourite',function(){
          var eventId = $(this).data("event-id");
            if($(this).find(".plus").hasClass("minus")){
               var status=1;
            }else{

               var status=0;
            }
            setUpHeader();
             $.ajax({
                type:'POST',
                url: baseUrl+'/favouriteEvent',
                data: 'eventId='+eventId+"&status="+status,
                cache:false,
                success:function(response){

                },
                error:function(response){

                }
            });

        });
      }

     


      /*Messages--*/
      if($(".chat_users").length > 0){
        $(".chat_users").click(function(){
            $(".chat_users").removeClass("active");
            $(this).addClass("active");
            var chat_id = $(this).data('chat-id');
            var receiver_id = $(this).data('user-id');
            $(".receiver_id").val(receiver_id);
            $(".message_chat_id").val(chat_id);
             
            getChatMessages(0,chat_id,20000);
        });
      }




  function getChatMessages(offset,chat_id,scrollTop=200){
       //$(".msg_chat_messages").prepend("<p class='text-center ajax_loader_msg'><i class='fa fa-spin fa-refresh'></i></p>");
        $.ajax({
            url: base_url+'/get_message?offset='+offset+"&chat_id="+chat_id,
            cache:false,
            type:"get",
            success:function(success){
                jQuery(".ajax_loader_msg").remove();
               /* if(success !=  ""){
                    jQuery(".msg_chat_messages").prepend(success);
                    jQuery(".offset_messages").val(offset);
                    $('.msg_chat_messages').scrollTop(scrollTop);
                    
                 }else{
                    jQuery(".ajax_loader_msg").remove();
                    jQuery(".offset_messages").val('');
                 }*/
            },
            error:function(errr,msg){
                jQuery(".ajax_loader_msg").remove();
                //jQuery(".receiver_id").val('');
                //console.log(msg);
            }
        });
       
    }


        /* Send Msg from providr to seeker--*/
    if($(".message_send_button").length >0){
        $(".chat_box").animate({ scrollTop: 20000000 }, "slow");
        $(".message_send_button").click(function(){
            let message = $(".message_input").val();
            let receiverId = $(".receiver_id").val();
            var message_chat_type = $(".message_chat_type").val();
            if(message_chat_type != "G" && $.trim(message) == ""){
               if($.trim(message) == "" && receiverId == "0"){
                    $(".message_input").focus();
                    return false;
                }
            }
              
                let chat_id  = $(".message_chat_id").val();
                setUpHeader();
                $.ajax({
                    url: baseUrl+'/send_message',
                    data:{text_message:message,receiverId:receiverId,chat_id:chat_id},
                    cache:false,
                    type:"post",
                    success:function(success){
                     var response = jQuery.parseJSON(success);
                     if(response.status == "1"){
			             if($('.no_message').length > 0){
                            $('.no_message').remove();
                         }


                        jQuery(".msg_chat_messages").append(response.data);
                        $(".message_input").val('');
                        $(".chat_box").animate({ scrollTop: 20000000 }, "slow");
                     }else{
                        console.log(success);
                     }
                        
                    },
                    error:function(res,status){
                        console.log(status);
                    }

                });
            
        });
    }



});
