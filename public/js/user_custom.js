    $(document).on('click','.load_more_events',function(){
 	  var page_no = parseInt($(this).attr("data-page-no"));
		
		 $.ajax({
	        type:'get',
	        url: baseUrl+'/load_more_events?page_no='+page_no,
	        cache:false,
	        success:function(response){
	        	var nextPage = parseInt(page_no)+1;
	            $(".load_more_events").attr("data-page-no",nextPage);
	           
	           if(response != "0"){
	           	 $(".all-pending-events").append(response);
	           }else{
	           	 $(".load_more_events").addClass("hide");
	           }
	           
	        },
	        error:function(response){

	        }
	    });
	});

 /*Onclick enter--*/
    $(".message_input").keypress(function(event) {
    	if (event.which == 13) {
    		$(".message_send_button").click();
         }
    });
