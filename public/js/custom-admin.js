if($(".chosen_multi").length > 0){
    $.validator.setDefaults({ ignore: ":hidden:not(.chosen_multi)" })
}

if($(".validate-me").length > 0){
	$(".validate-me").validate({
        errorPlacement: function(error, element) {
            if(element.attr("name") == "artist[]"){
                 error.insertAfter(".choose_artists_error");
            }else if(element.attr("name") == "genere_id[]"){
                error.insertAfter(".genere_id_error");
            }
            else{
                error.insertAfter(element);
            }
        }
    });
}

/* Google Autocomplete address--*/
if($("#event_location").length > 0){
	initialize();
	function initialize() {
        var input = document.getElementById('event_location');
        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            document.getElementById('city2').value = place.name;
            document.getElementById('cityLat').value = place.geometry.location.lat();
            document.getElementById('cityLng').value = place.geometry.location.lng();
            //alert("This function is working!");
            //alert(place.name);
           // alert(place.address_components[0].long_name);

        });
    }
    google.maps.event.addDomListener(window, 'load', initialize); 
}




$( document ).ready(function() {
	if($('.datepicker').length > 0){
		$('.datepicker').datepicker({
  			autoclose: true,
  			format: 'yyyy-mm-dd',
        startDate: '-0m'
    	});
	}
	if($('.timepicker').length > 0){
		$('.timepicker').timepicker({
     	 showInputs: false,
      	 setTime: null,
      	  showMeridian: false
    	});
	}
	if($(".chosen_multi").length > 0){
		$(".chosen_multi").chosen();
	}

	if($(".data_table").length > 0){
		$('.data_table').DataTable({
            "order": [[ 0, "asc" ]]
        });
	}
	
	
    /*if($(".delete_event").length > 0){
    //alert("dd");
     $(".delete_event").click(function(){
        var event_id = $(this).data('event-id');
       swal({
              title: "",
              text: "Your you sure to delete!",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-danger",
              confirmButtonText: "Yes, delete it!",
              closeOnConfirm: false
            },
            function(){
                //alert(base_url);
              //swal("Deleted!", "Your imaginary file has been deleted.", "success");
              window.location.href=base_url+"/admin/events/"+event_id+"/delete";
            });
     });
}*/






});

   




 /*$('.timepicker').datetimepicker({

        format: 'HH:mm:ss'

    }); */

   