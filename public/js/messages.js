if($(".msg_chat_messages").length > 0){
    $('.chat_box').scroll(function(){
            console.log($('.chat_box').scrollTop());
            if ($('.chat_box').scrollTop() < 10){
                var offset = parseInt(jQuery(".offset_messages").val())+1;
                var message_chat_id = jQuery(".message_chat_id").val();
                if(jQuery(".offset_messages").val() != ""){
                    getChatMessages(offset,message_chat_id);
                    //$(".msg_chat_messages").animate({ scrollTop: 250 });
                   return false;
                }
            }
    });
}

    function getChatMessages(offset,chat_id,scrollTop=200){
       $(".msg_chat_messages").prepend("<p class='text-center ajax_loader_msg'><i class='fa fa-spin fa-refresh'></i></p>");
        $.ajax({
            url: baseUrl+'/getMessagesWithLimit?offset='+offset+"&chat_id="+chat_id,
            cache:false,
            type:"get",
            success:function(success){
                jQuery(".ajax_loader_msg").remove();
                if(success !=  ""){
                    jQuery(".msg_chat_messages").prepend(success);
                    jQuery(".offset_messages").val(offset);
                    $('.msg_chat_messages').scrollTop(scrollTop);
                 }else{
                    jQuery(".ajax_loader_msg").remove();
                    jQuery(".offset_messages").val('');
                 }
            },
            error:function(errr,msg){
                jQuery(".ajax_loader_msg").remove();
                console.log(msg);
            }
        });
       
    }
