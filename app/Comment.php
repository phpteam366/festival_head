<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function feedsComment(){
    	return $this->belongsTo('App\Feed');
    }
}
