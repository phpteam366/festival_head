<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Config;
class Event extends Model
{

    protected $fillable = array('*');

	public function genere(){
		return $this->belongsTo('App\Genre','genre_id','id');
	}

    public function getEventsList($status){
    	$paginationLimit = Config::get("constants.PAGINATION_LIMIT");
		 $items = DB::table("events")
    			//->join("genres","genres.id", "=","events.genre_id")
                ->leftjoin("artists",\DB::raw("FIND_IN_SET(artists.id,events.artists_id)"),">",\DB::raw("'0'"))
                ->select("events.*", \DB::raw("GROUP_CONCAT(artists.name) as artists_name"))
    			->where("events.status",$status)
    			->groupBy("events.id")
                //->groupBy("genres.id")
                ->orderBy("events.id","DESC")
    			->paginate(20);
            


         $items->setCollection(
            $items->getCollection()
            ->map(function($item, $key)
            {
                $genereIds = explode(",",$item->genre_id);
                $genereName = DB::table("genres")->select(DB::raw("GROUP_CONCAT(genres.name) as genres_name"))->whereIn("id",$genereIds)->first();
                if(!empty($genereName)){
                    $genere = $genereName->genres_name;
                }else{
                    $genere = "";
                }
                $item->genere_name = $genere;
                return $item;
            })
        );
         return $items;
    }

    public function getEventsLatLong(){
        return DB::table("events")
                ->where("status",1)
                ->select("latitude","longitude","date_time")
                ->get()
                ->toArray();
    }
}
