<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    public function feedsLike(){
    	return $this->belongsTo('App\Feed');
    }
}
