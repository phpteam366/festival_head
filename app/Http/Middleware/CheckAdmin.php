<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;
use DB;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::check()) {
            if(Auth::user()->user_type != 'A'){
                Auth::logout();
                return redirect('/login');
            }
             return $next($request);
        }else{
            return Redirect::to("/admin");
        }
    }
}
