<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;
use DB;

class CheckUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if(Auth::user()->user_type != 'U'){
                Auth::logout();
                return Redirect::to("/admin");
            }

            $userId = Auth::id();
            $checkDetails = DB::table("user_details")->where("user_id",$userId)->first();
            if(isset($checkDetails) && !empty($checkDetails)){
                return $next($request);
            }else{
               return Redirect::to("/login");
            }
        }else{
            return Redirect::to("/login");
        }
    }
}
