<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;
class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            // The user is logged in...
           $userType = Auth::user()->user_type;
            if($userType != "A"){
                    $userStatus = Auth::user()->status;
                    if($userStatus == "1"){
                        return $next($request);
                    }else{
                        Auth::logout();
                        Session::flush();
                        return Redirect::to("/login");
                    }
            }else{
                Auth::logout();
                Session::flush();
                return Redirect::to("/admin");
            }
        }else{
            return Redirect::to("/login");
        }
    }
}
