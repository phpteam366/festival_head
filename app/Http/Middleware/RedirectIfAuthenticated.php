<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Redirect;
class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::check()) {
            if(Auth::user()->user_type != 'U'){
                return Redirect::to("/admin/dashboard");
            }else if(Auth::user()->user_type != 'A'){
                return Redirect::to("/");
            }
             return $next($request);
        }
        else{
            return Redirect::to("/");
        }
    }
}

