<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\User;
use Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }

    public function googleLogin(){
       return Socialite::driver('google')->redirect();
    }

    public function googleLoginCallback(){
        try {
            $user = Socialite::driver('google')->user();
        } catch (\Exception $e) {
            return redirect('/login');
        }
        // check if they're an existing user
        $existingUser = User::where(['email' => $user->email, 'user_type' => 'U'])->first();
        if($existingUser){
            // log them in
            auth()->login($existingUser, true);
        } else {
            // create a new user
            $newUser                  = new User;
            $newUser->fname            = $user->name;
            $newUser->email           = $user->email;
            $newUser->gmail_id       = $user->id;
            $newUser->register_type  = 'G';
            $newUser->status         = 1;
            $newUser->user_type      = 'U';
            $newUser->save();

            auth()->login($newUser, true);
        }
        return Redirect::to('/');
    }

    public function facebookLogin(){
       // echo'hello';die;
       return Socialite::driver('facebook')->redirect();
    }

    public function facebookLoginCallback(){
        
        try {
            //echo'hi';die;
            $user = Socialite::driver('facebook')->user();
        } catch (\Exception $e) {
            //echo'bye';die;
            return redirect('/login');
        }
        // check if they're an existing user
        $existingUser = User::where(['email' => $user->email, 'user_type' => 'U'])->first();
        if($existingUser){
            // log them in
            auth()->login($existingUser, true);
        } else {
            // create a new user
            $newUser                  = new User;
            $newUser->fname            = $user->name;
            $newUser->email           = $user->email;
            $newUser->gmail_id       = $user->id;
            $newUser->register_type  = 'F';
            $newUser->status         = 1;
            $newUser->user_type      = 'U';
            $newUser->save();

            auth()->login($newUser, true);
        }
        return Redirect::to('/');
    }

    public function linkedinLogin(){
       return Socialite::driver('linkedin')->redirect();
    }

    public function linkedinLoginCallback(){
        try {
            $user = Socialite::driver('linkedin')->user();
        } catch (\Exception $e) {
            return redirect('/login');
        }
        // check if they're an existing user
        $existingUser = User::where(['email' => $user->email, 'user_type' => 'U'])->first();
        if($existingUser){
            // log them in
            auth()->login($existingUser, true);
        } else {
            // create a new user
            $newUser                  = new User;
            $newUser->fname           = $user->name;
            $newUser->email           = $user->email;
            $newUser->gmail_id        = $user->id;
            $newUser->register_type   = 'L';
            $newUser->status          = 1;
            $newUser->user_type       = 'U';
            $newUser->save();

            auth()->login($newUser, true);
        }
        return Redirect::to('/');
    }
}
