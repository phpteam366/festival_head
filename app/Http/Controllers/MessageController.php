<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BlockedUser;
use App\ContactUs;
use Auth;
use App\Messages as Message;
use DB;
use Validator;
use URL;
use Mail;
use App\Mail\SendMailable;
use Config;
use Redirect;
class MessageController extends Controller
{

    private $Message;
    public function __construct(){
        $this->Message = new Message();
    }
    public function message($receiverId = "", Request $request){
      $userId = Auth::id(); 
      if(isset($request) && isset($request['event_id']) && empty($receiverId)){
        $eventId = $request['event_id'];

        $checkEventChats =DB::table("chats")->where("event_id",$eventId)->first();
        if(empty($checkEventChats)){
          return Redirect::to("/messages");
        }

        $findIdInChatsTable = DB::table("chats")->where("event_id",$eventId)->whereRaw("FIND_IN_SET($userId,chats.receiver_id)")->first();
         if(empty($findIdInChatsTable)){
           $findAllUsersOfChat = DB::table("chats")->where("event_id",$eventId)->select("receiver_id")->first();
           $receiver_ids = $findAllUsersOfChat->receiver_id;
           if(!empty($receiver_ids)){
             $addIdInReceiver = $receiver_ids.",".$userId;
           }else{
              $addIdInReceiver = $userId;
           }
           DB::table("chats")->where("event_id",$eventId)->where("chat_type","G")->update(['receiver_id' => $addIdInReceiver]);
         }

         $getChatId = DB::table("chats")->where("event_id",$eventId)->where("chat_type","G")->select("id")->first();
         $chatId = $getChatId->id;
         
         $chatUsers = DB::table("chats")
              ->join("events","events.id","=","chats.event_id")
              ->where('chats.id',$chatId)
              ->select("chats.*","events.image as event_image")
              ->orderBy('chats.updated_at',"DESC")
              ->get();


             $messagesLimit = Config::get('constants.MESSAGES_LIMIT');
             $offset        = 0;
             $limit         = $messagesLimit;
             $messageData = $this->Message->getMessageData($chatId,$offset,$limit);
             $messages    = $messageData;

      }
      else if(isset($receiverId) && $receiverId != "") {
          /* check user is listed in chats table--*/

          $checkUser = DB::table("chats")
                ->where(function($q1) use($receiverId){
                   $q1->where('sender_id', $receiverId)
                      ->orWhere('receiver_id', $receiverId);
                   })
                ->where(function($q1) use($userId){
                   $q1->where('sender_id', $userId)
                      ->orWhere('receiver_id', $userId);
                   })
              ->select("chats.id")
               ->get();
              
               if($checkUser->isEmpty()){
                $chatId =  DB::table("chats")->insertGetId(['sender_id' => $userId, 'receiver_id' => $receiverId, 'message' => '','seen_by' => '0','created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")]);
               }else{
                $chatId = $checkUser[0]->id;
               }
             
          $chatUsers = DB::table("chats")
               ->join("users as sender","sender.id","=","chats.sender_id")
               ->join("users as receiver","receiver.id","=","chats.receiver_id")
               
               ->select("chats.*","sender.id as sender_id","sender.fname as sender_fname","sender.lname as sender_lname","sender.image as sender_image","receiver.id as receiver_id","receiver.fname as receiver_fname","receiver.lname as receiver_lname","receiver.image as receiver_image")
               ->where('chats.id',$chatId)
               ->orderBy('updated_at',"DESC")
               ->get();


               $messagesLimit = Config::get('constants.MESSAGES_LIMIT');
               $offset        = 0;
               $limit         = $messagesLimit;
               $messageData = $this->Message->getMessageData($chatId,$offset,$limit);
               $messages    = $messageData;
        
        }else{

             $chatUsers = DB::table("chats")
               ->leftjoin("users as sender","sender.id","=","chats.sender_id")
               ->leftjoin("users as receiver","receiver.id","=","chats.receiver_id")
               ->leftjoin("events","events.id","=","chats.event_id")
               ->where(function($q1) use($userId){
                   $q1->whereRaw("FIND_IN_SET($userId,chats.receiver_id)")
                      ->orWhere('sender_id', $userId);
                      //->orWhere('sender_id', $userId);
                   })
               ->select("chats.*","sender.id as sender_id","sender.fname as sender_fname","sender.lname as sender_lname","sender.image as sender_image","receiver.id as receiver_id","receiver.fname as receiver_fname","receiver.lname as receiver_lname","receiver.image as receiver_image","events.image as event_image")
               //->where("sender.status",'1')
               //->where("receiver.status",'1')
               ->orderBy('updated_at',"DESC")
               ->get();
         
              

                 $messagesLimit = Config::get('constants.MESSAGES_LIMIT');
                if(isset($chatUsers) && count($chatUsers) >0){
                   $chatId      = $chatUsers[0]->id;
                   $offset      =0;
                   $limit       = $messagesLimit;
                   $messageData = $this->Message->getMessageData($chatId,$offset,$limit);
                   $messages    = $messageData;
                 }else{
                   $offset      =0;
                   $limit       = $messagesLimit;
                   $messages = array();
                 }
        }

        return view('message.messages',['chatUsers' => $chatUsers,'messages'=> $messages]);
  }



  public function message_bkp($receiverId = ""){
        $userId = Auth::id(); 
      if(isset($receiverId) && $receiverId != "") {
          /* check user is listed in chats table--*/

          $checkUser = DB::table("chats")
                ->where(function($q1) use($receiverId){
                   $q1->where('sender_id', $receiverId)
                      ->orWhere('receiver_id', $receiverId);
                   })
                ->where(function($q1) use($userId){
                   $q1->where('sender_id', $userId)
                      ->orWhere('receiver_id', $userId);
                   })
              ->select("chats.id")
               ->get();
              
               if($checkUser->isEmpty()){
                $chatId =  DB::table("chats")->insertGetId(['sender_id' => $userId, 'receiver_id' => $receiverId, 'message' => '','seen_by' => '0','created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")]);
               }else{
                $chatId = $checkUser[0]->id;
               }
             
          $chatUsers = DB::table("chats")
               ->join("users as sender","sender.id","=","chats.sender_id")
               ->join("users as receiver","receiver.id","=","chats.receiver_id")
               
               ->select("chats.*","sender.id as sender_id","sender.fname as sender_fname","sender.lname as sender_lname","sender.image as sender_image","receiver.id as receiver_id","receiver.fname as receiver_fname","receiver.lname as receiver_lname","receiver.image as receiver_image")
               //->where("sender.status",'1')
             //  ->where("receiver.status",'1')
                ->where('chats.id',$chatId)
               ->orderBy('updated_at',"DESC")
               ->get();


               $messagesLimit = Config::get('constants.MESSAGES_LIMIT');
               $offset        = 0;
               $limit         = $messagesLimit;
               $messageData = $this->Message->getMessageData($chatId,$offset,$limit);
               $messages    = $messageData;
        
        }else{
             $chatUsers = DB::table("chats")
               ->join("users as sender","sender.id","=","chats.sender_id")
               ->join("users as receiver","receiver.id","=","chats.receiver_id")
               ->where(function($q1) use($userId){
                   $q1->where('receiver_id', $userId)
                      ->orWhere('sender_id', $userId);
                   })
               ->select("chats.*","sender.id as sender_id","sender.fname as sender_fname","sender.lname as sender_lname","sender.image as sender_image","receiver.id as receiver_id","receiver.fname as receiver_fname","receiver.lname as receiver_lname","receiver.image as receiver_image")
               ->where("sender.status",'1')
               ->where("receiver.status",'1')
               ->orderBy('updated_at',"DESC")
               ->get();
         
                 $messagesLimit = Config::get('constants.MESSAGES_LIMIT');
                if(isset($chatUsers) && count($chatUsers) >0){
                   $chatId      = $chatUsers[0]->id;
                   $offset      =0;
                   $limit       = $messagesLimit;
                   $messageData = $this->Message->getMessageData($chatId,$offset,$limit);
                   $messages    = $messageData;
                 }else{
                   $offset      =0;
                   $limit       = $messagesLimit;
                   $messages = array();
                 }
        }
        return view('message.messages',['chatUsers' => $chatUsers,'messages'=> $messages]);
  }






 public function get_messages_auto(Request $request){
       $chat_id = $request['chat_id'];
       $last_record = $request['last_record'];
       $getNewMessages = $this->Message->getNewMessages($chat_id,$last_record);
       if(!empty($getNewMessages)) {
             $messageData =  view("message.render.messages_render",['messages'=>$getNewMessages]);
            return $messageData; 
       }else{
        return "";
       }
    }

 public function sendMessage(Request $request){
        $userId = Auth::id();
        $validator = Validator::make($request->all(), [
            'text_message' => 'required',
            'chat_id' => 'required'
        ]);

        if ($validator->fails()){
            $errors = $validator->getMessageBag()->toArray();
            return $this->sendResponseInJson(0,"Validation Fails",$errors);
            die;
        }

        $name           = Auth::user()->name;
        $checkImage     = Auth::user()->profile_pic;
        if($checkImage != ""){
             $userImage = URL::to('/')."/public/images/profiles/".$checkImage;
        }else{
            $userImage = URL::to('/')."/public/images/dummy_user.png";
        }

        /*Get post data--*/
        $text_message   = $request->get("text_message");
        $receiverId     = $request->get("receiverId");
        $chat_id        = $request->get("chat_id");
        
        /*Update chat table--*/
        $updateChat     = DB::table('chats')->where("id",$chat_id)->update(['message' => $text_message, 'updated_at' => date("Y-m-d H:i:s"), 'seen_by' => $userId]);

        $messageArray   = array(
                "sender_id" => $userId,
                "receiver_id" =>  $receiverId,
                "message" => $text_message ,
                "chat_id" => $chat_id ,
                "created_at" => date("Y-m-d H:i:s")
            );

       $messageInsert =  Message::insertGetId($messageArray);
       if($messageInsert){
            $sentMessage = '<div class="text-center">Rahul</div><div class="left_chatbox right_chatbox" data-message-id="'.$messageInsert.'">
              
              <div class="chatbox_outer">
                <div class="message_bubble">
                  '.$text_message.'
                </div>
                <span class="message_time">Just Now</span>
              </div>
            </div>';
        }
        return $this->sendResponseInJson(1,"Message send",$sentMessage);
    }



    public function getMessagesWithLimit(Request $request){
        $userId      = Auth::id();
        $limit       = Config::get('constants.MESSAGES_LIMIT'); /* get from config--*/
        $offset      = $request['offset']*$limit;
        $chat_id     = $request['chat_id'];
       
        $messageData = $this->Message->getMessageData($chat_id,$offset,$limit);

        if(count($messageData) > 0){
            $messageData =  view("message.render.messages_render",['messages'=>$messageData]);
            return $messageData;
        }else{
             return "";
        }
     
    }

    public function blockUser(Request $request){
    	$userId = $request->get('user_id');
    	$blockedUserId = $request->get('blocked_user_id');
    	$checkBlock = BlockedUser::where(['user_id' => $userId, 'blocked_user_id' => $blockedUserId])->first();
    	if($checkBlock){
    		if($checkBlock->status == 0){
    			$blockUser = BlockedUser::where(['user_id' => $userId, 'blocked_user_id' => $blockedUserId])->update(['status' => 1]);
    		}
    	}else{
    		$blockUser = BlockedUser::insert(['user_id' => $userId, 'blocked_user_id' => $blockedUserId, 'status' => 1]);
    	}
    	//return response->json(['success' => true]);
    }

    public function unBlockUser(Request $request){
		$userId = Auth::id();
        //echo $userId;die;
    	$blockedUserId = $request->get('blocked_user_id');
    	$checkBlock = BlockedUser::where(['user_id' => $userId, 'blocked_user_id' => $blockedUserId, 'status' => 1])->first();
    	if($checkBlock){
    		$unblockUser = BlockedUser::where(['user_id' => $userId, 'blocked_user_id' => $blockedUserId])->update(['status' => 0]);
    	}
    }

    public function contactUs(Request $request){
      $name = $request->get('name');
      $email = $request->get('email');
      //$email = "yaminidudeja176@gmail.com";
      $subject = $request->get('subject');
      $message = $request->get('message');
      $data = ['name' => $name, 'email' => $email, 'subject' => $subject, 'message' => $message, 'status' => 0];
	
       $insertContact = ContactUs::insert($data);
       $adminEmail = Config::get('constants.ADMIN_EMAIL'); 
	$data['messageToSend'] = $message;
       Mail::send('emails.contactUs', $data, function ($message) use ($adminEmail, $subject) {
                    $message->to($adminEmail)->subject($subject);
                });
      return Redirect::back()->with(['message' => 'Thanks for contact us, We will revert you soon']); 
    }

	public function mail(){
                 $subject = "Testing";
		 $data = ['name' => "Hello", 'email' => "helo@gmail.com", 'subject' => "ssss fg", 'message' => "fsdf", 'status' => 0];
		Mail::send('emails.contactUs', $data, function ($message) use ($subject) {
		    $message->to("shubham12@mailinator.com")->subject($subject);
		});
	}


}

