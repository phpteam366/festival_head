<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserDetail;
use App\Genre;
use App\Artist;
use App\State;
use App\Event;
use App\Favorite;
use App\BlockedUser;
use App\Like;
use App\Comment;
use App\Notification;
use App\NotificationSetting;
use App\Follow;
use App\Feed;
use Config;
use Hash;
use Auth;
use Redirect;
use DB;
use Mail;
class UserController extends Controller
{
    private $Event;
    public function __construct(){
        $this->Event = new Event();
    }
    public function signUp(Request $request){
    	$validateData = $request->validate([
    										'email' => 'required',
    										'password' => 'required|confirmed',
    										]);
        //echo'<pre>';print_r($request->all());die;
        $userEmail = $request->get('email');
        $checkEmail = User::where('email', $userEmail)->select("id")->first();
        if(!empty($checkEmail)){
        	return Redirect::back()->with(['message' => 'Email Already Registered.']);
        	die;
        }
        $password = Hash::make($request->get('password'));
        $userInsert = User::insertGetId(['email' => $userEmail, 'password' => $password, 'register_type' => 'E', 'user_type' => 'U','status' =>1]);
        if($userInsert){
            Auth::loginUsingId($userInsert);
            return Redirect::to('/create-profile');
        	//return view('users.create-profile');
        }else{
        	return Redirect::back()->with(['message' => 'Something went wrong.']);
        }

    }

    public function createProfile(Request $request){
        $genres = Genre::where(['status' => '1'])->get();
        $artists = Artist::where(['status' => '1'])->get();
        $states = State::where(['status' => '1'])->get();
        return view('users.create-profile', ['genres' => $genres, 'artists' => $artists, 'states' => $states]);
    }

    public function login(Request $request){

        $validateData = $request->validate([
                    'email' => 'required',
                    'password' => 'required'
                    ]);
        $userEmail = $request->get('email');
        $userPassword = $request->get('password');
        if (Auth::attempt(['email' => $userEmail, 'password' => $userPassword, 'user_type' => 'U'])) {
            $userStatus = Auth::user()->status;
            if($userStatus == "0"){
                Auth::logout();
                return Redirect::back()->with('message','Your account is suspended by admin. Please contact admin to activate your account.');    
            }elseif($userStatus == "1"){
                return Redirect::to('/');    
            }else{
                Auth::logout();
                return Redirect::back()->with('message','Email/username or password incorrect. Please
try again.');    
            }
        }else{
            Auth::logout();
            return Redirect::back()->with('message','Email or password incorrect. Please try again.');
        }
    }

    public function forgotPassword(REQUEST $request){
        $email = $request->get('email');
        $checkEmailExists = User::where("email",$email)->select("id", "fname")->first();
        if(empty($checkEmailExists)){
            return Redirect::back()->with('message', 'Email does not exists');
        }else{
            $password = $this->randomString(8);
            $generatePassword = Hash::make($password);
            $userId = $checkEmailExists->id;
            $updatePassword = DB::table('users')->where('id', $userId)->update(['password' => $generatePassword]);
            if($updatePassword){
                $userName = $checkEmailExists->f_name;
                $data = array(
                    'name' => $userName,
                    'password' => $password
                );

                Mail::send('emails.forgot_password', $data, function ($message) use ($email) {
                    $message->to($email)->subject('Forgot password for festival head');
                });
                return Redirect::back()->with("success", "Password sent to registered email.");
            }else{
                return Redirect::back()->with("message", "Something went wrong. Please try again later.");
            }
        }
    }

    function randomString($length = 6) {
        $str = "";
        $characters = array_merge(range('A','Z'), range('A','Z'), range('1','9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

    public function create_profile(Request $request){
        $user = Auth::user();
        $userId = $user->id;
        $fName = $request->get('f_name');
        $lName = $request->get('l_name');
        $nickName = $request->get('nick_name');
        $maritalStatus = $request->get('marital_status');
        $age = $request->get('age');
        $gender = $request->get('gender');
        $genreId = $request->get('genre');
        $artistId = $request->get('artists');
        $stateId = $request->get('state');
        $cityId   = $request->get("city");
        //echo'<pre>';print_r($request->all());die;
        if($request->hasFile('image')){
            $image = $request->file('image');
            $orignalName = $image->getClientOriginalName();
            $imageName = time().$orignalName;
            $destinationPath = public_path('/images/profile_image/');
            $image->move($destinationPath, $imageName);
        }else{
            $imageName = "dummy.png";
        }

        $usersArray = array(
                            "fname" => $fName,
                            "lname" => $lName,
                            "image" => $imageName,
                            );
        $userDetailArray = array(
                                "user_id" => $userId,
                                "nick_name" => $nickName,
                                "age" => $age,
                                "gender" => $gender,
                                "genre_id" => $genreId,
                                "artists_id" => $artistId,
                                "state_id" => $stateId,
                                "marital_status" => $maritalStatus,
                                'city_id' => $cityId
                                );
        //echo'<pre>';print_r($userDetailArray);die;
        $users = User::where("id", $userId)->update($usersArray);
        $userDetails = UserDetail::insert($userDetailArray);
        if($userDetails){
            $notificationSettings = NotificationSetting::insert(['user_id' => $userId, 'notification_type' => 'L', 'status' => '1']);
            $notificationSettings = NotificationSetting::insert(['user_id' => $userId, 'notification_type' => 'C', 'status' => '1']);
            $notificationSettings = NotificationSetting::insert(['user_id' => $userId, 'notification_type' => 'F', 'status' => '1']);
            return Redirect::to("/my-profile")->with(['message' => "Profile Created successfully."]);
        }else{
            return Redirect::back()->with("message", "Something went wrong.");
        }
    }

    public function myProfile(){
        $user = Auth::user();
    
        $userId = $user->id;
        $userName = $user->fname.' '.$user->lname;
        $userImage = $user->image;
        $userDetails = UserDetail::where('user_id', $userId)->first();
        $age = $userDetails->age;
        $gender = $userDetails->gender;
        $genreId = $userDetails->genre_id;
        $artistId = $userDetails->artists_id;
        $stateId = $userDetails->state_id;
        $maritalStatus = $userDetails->marital_status;
        $artist = Artist::where(['id' => $artistId])->select('name')->first();
        $genre = Genre::where(['id' => $genreId])->select('name')->first();
        $state = State::where(['id' => $stateId])->select('name')->first();
        $newsFeed = DB::table('feeds')
                            ->join('users', 'feeds.user_id', '=', 'users.id')
                            ->join('user_details', 'feeds.user_id', '=', 'user_details.user_id')
                            //->leftJoin('likes', 'feeds.id', '=', 'likes.receiver_id')
                            ->select('feeds.image As feed_image', 'feeds.video As feed_video', 'feeds.id As feed_id', 'feeds.created_at As feeds_created', 'users.image As user_image', 'users.*', 'feeds.*', 'user_details.*')
                            ->where(['feeds.user_id' => $userId])
                            ->OrderBy('feeds.created_at', 'Desc')->get();
        //echo'<pre>';print_r($newsFeed);die;
        $likes = DB::table('feeds')
                            ->leftJoin('likes', 'feeds.id', '=', 'likes.receiver_id')
                            ->select('feeds.id As feed_id', 'likes.user_id As liked_by_id', 'likes.status As like_status', DB::raw("(select COUNT(id) As total_likes from likes where status='1' and receiver_type ='N' and receiver_id=feeds.id) as total_like"))
                            ->where(['likes.status' => '1'])
                            ->OrderBy('feeds.created_at', 'Desc')->get();
        //echo'<pre>';print_r($likes);die;
        $likesArr = array();
        foreach ($likes as $key => $like) {
            $likesArr[$like->feed_id] = $like->total_like;
        }
        //echo'<pre>';print_r($likesArr);die;
        $comments = DB::table('feeds')
                    ->leftJoin('comments', 'feeds.id', '=', 'comments.receiver_id')
                    ->select('feeds.id As feed_id', DB::raw("(select COUNT(id) As total_comment from comments where receiver_type ='N' and receiver_id=feeds.id) as total_comment"))
                    ->where(['feeds.user_id' => $userId])
                    ->OrderBy('comments.created_at', 'Desc')->get();

        $totalComment = $feedLikes = $feedComment = array();
        foreach ($comments as $key => $comment) {
            $totalComment[$comment->feed_id] = $comment->total_comment;
        }
        //echo'<pre>';print_r($totalComment);die;
        $followings = $followers = 0;
        $follower = DB::table('follows')->where(['following_id' => $userId, 'status' => '1'])->get();
        $following = DB::table('follows')->where(['follower_id' => $userId, 'status' => '1'])->get();
        if($follower){
            $followers = count($follower);
        }
        if($following){
            $followings = count($following);
        }

        $getCityId = DB::table("user_details")->where("user_id",$userId )->select("city_id")->first();
        $cityName = "";
        if(isset($getCityId) && !empty($getCityId)){
            $cityId = $getCityId->city_id;
            $getCityName = DB::table("cities")->where("id",$cityId)->select("name")->first();
            $cityName = $getCityName->name;
        }
        $data = array(
                    "user_name" => $userName,
                    "gender" => $gender,
                    "age" => $age,
                    "profile_image" => $userImage,
                    "genre" => $genre->name,
                    "artist" => $artist->name,
                    "marital_status" => $maritalStatus,
                    "state" => $state->name,
                    'city'  =>  $cityName ,
                    'followers' => $followers,
                    'followings' => $followings,
                    );
        return view('users.my-profile', ['data' => $data, 'newsFeed' => $newsFeed, 'totalComment' => $totalComment, 'likesArr' => $likesArr])->with($data);
    }

    public function editProfile(){
        $user = Auth::user();
        $userId = $user->id;
        $fName = $user->fname;
        $lName = $user->lname;
        $userImage = $user->image;
        $userDetails = UserDetail::where('user_id', $userId)->first();
        $age = $userDetails->age;
        $gender = $userDetails->gender;
        $nickName = $userDetails->nick_name;
        $genreId = $userDetails->genre_id;
        $artistId = $userDetails->artists_id;
        $stateId = $userDetails->state_id;
        $cityId = $userDetails->city_id;
        $maritalStatus = $userDetails->marital_status;
        $genres = Genre::where(['status' => '1'])->get();
        $artists = Artist::where(['status' => '1'])->get();
        $states = State::where(['status' => '1'])->get();
        $cities = DB::table("cities")->where("status","1")->where("state_id",$stateId)->get();
        $data = array(
                    "fname" => $fName,
                    "lname" => $lName,
                    "nickname" => $nickName,
                    "gender" => $gender,
                    "age" => $age,
                    "profile_image" => $userImage,
                    "genreId" => $genreId,
                    "artistId" => $artistId,
                    "marital_status" => $maritalStatus,
                    "stateId" => $stateId,
                    'cityId' => $cityId
                    );
        return view('users.edit-profile', ['genres' => $genres, 'artists' => $artists, 'states' => $states, 'cities' => $cities])->with($data);
    }

    public function edit_profile(Request $request){
        $user = Auth::user();
        //echo'<pre>';print_r($request->all());die;
        $userId = $user->id;
        $fName = $request->get('f_name');
        $lName = $request->get('l_name');
        $nickName = $request->get('nick_name');
        $maritalStatus = $request->get('marital_status');
        $age = $request->get('age');
        $gender = $request->get('gender');
        $genreId = $request->get('genre');
        $artistId = $request->get('artists');
        $stateId = $request->get('state');
        $cityId   = $request->get('city');
        $usersArray = array(
                            "fname" => $fName,
                            "lname" => $lName,
                            );
        if($request->hasFile('image')){
            $image = $request->file('image');
            $orignalName = $image->getClientOriginalName();
            $imageName = time().$orignalName;
            $destinationPath = public_path('/images/profile_image/');
            $image->move($destinationPath, $imageName);
            $usersArray['image'] = $imageName;
        }

        $userDetailArray = array(
                                "nick_name" => $nickName,
                                "age" => $age,
                                "gender" => $gender,
                                "genre_id" => $genreId,
                                "artists_id" => $artistId,
                                "state_id" => $stateId,
                                "marital_status" => $maritalStatus,
                                'city_id'   => $cityId
                                );
        
        $users = User::where("id", $userId)->update($usersArray);
        $userDetails = UserDetail::where("user_id", $userId)->update($userDetailArray);
        if($userDetails){
            if(Auth::user()->user_type == 'A'){
                return Redirect::to("/admin/edit-profile")->with("message", "Profile Successfully Updated.");
            }else{
                return Redirect::to("/my-profile")->with("message", "Profile Updated Successfully.");
            }
        }else{
            return Redirect::back()->with("message", "Something went wrong.");
        }
    }

    public function dashboard(){
        $totalUser = $activeUsers = $events = 0;
        $totalUser = User::where(['user_type' => 'U'])->count();
        $activeUsers = User::where(['user_type' => 'U','status' => '1'])->count();
        $events = Event::where(['status' => '0'])->count();
        $suspendedUsers = User::where(['user_type' => 'U','status' => '0'])->count();
        return view('admin.dashboard', ['totalUser' => $totalUser, 'activeUsers' => $activeUsers, 'events' => $events, 'suspendedUsers' => $suspendedUsers]);
    }

    public function logout(){
        Auth::logout();
        return Redirect::to('/login');
    }

    public function changeAdminPassword(){
        return view('admin.change-password');
    }

    public function changePassword(Request $request){
        $user = Auth::user();
        $oldPassword = $request->get('old_password');
        $orignalOldPass = $user->password;
        $newPassword = $request->get('password');
        $confirmedPassword = $request->get('password_confirmation');
        if(Hash::check($oldPassword, $orignalOldPass)){
            if($newPassword == $confirmedPassword){
                if($oldPassword == $newPassword){
                    return Redirect::back()->with(['message' => 'old password must not be match with new password']);        
                }else{
                    $newHashedPass = Hash::make($newPassword);
                    $changePassword = User::where(['id' => $user->id])->update(['password' => $newHashedPass]);
                    return Redirect::back()->with(['message' => 'Password Successfully Changed']);        
                }
                
            }else{
                return Redirect::back()->with(['message' => 'Password and Confirmed Password is not matched']);    
            }
        }else{
            return Redirect::back()->with(['message' => 'Old Password is incorrect']);
        }
    }

    public function userList(){
        $users = User::where(['user_type' => 'U', 'status' => '1'])->orderBy("id","DESC")->paginate(20);
        return view('admin.user-list', ['users' => $users]);
    }

    public function suspendedUserList(){
        $users = User::where(['user_type' => 'U', 'status' => '0'])->paginate(20);
        return view('admin.suspended-user-list', ['users' => $users]);
    }

    public function viewUser($id){
        $users = User::where('id', $id)->first();
        $userData = UserDetail::where('user_id', $id)->first();
         $userCityId = $userData->city_id;
         $cityName ="";
         if(!empty($userCityId)){
            $getCityName =DB::table("cities")->where("id",$userCityId)->select("name")->first();
            if(isset($getCityName) && !empty($getCityName)){
                $cityName = $getCityName->name;
            }
         }
         $userData->city_name = $cityName;
        return view('admin.viewUser', ['users' => $users, 'userData' => $userData]);
    }

    public function editUser($id){
        $user = User::where('id', $id)->first();
        $userData = UserDetail::where('user_id', $id)->first();
        //echo'<pre>';print_r($user);die;
        return view('admin.editUser', ['user' => $user, 'userData' => $userData]);
    }

    public function saveUser(Request $request){
        $validateData = $request->validate([
                                            'email' => 'required',
                                            'password' => 'required|confirmed',
                                            ]);
        $userEmail = $request->get('email');
        $checkEmail = User::where('email', $userEmail)->select("id")->first();
        if(!empty($checkEmail)){
            return Redirect::back()->with(['message' => 'Email Already Registered.']);
            die;
        }
        $password = Hash::make($request->get('password'));
        $fName = $request->get('f_name');
        $lName = $request->get('l_name');
        $status = $request->get('status');
        $nickName = $request->get('nick_name');
        $maritalStatus = $request->get('marital_status');
        $age = $request->get('age');
        $gender = $request->get('gender');
        $genreId = $request->get('genre');
        $artistId = $request->get('artists');
        $stateId = $request->get('state');
        if($request->hasFile('image')){
            $image = $request->file('image');
            $orignalName = $image->getClientOriginalName();
            $imageName = time().$orignalName;
            $destinationPath = public_path('/images/profile_image/');
            $image->move($destinationPath, $imageName);
        }else{
            $imageName = "dummy.png";
        }
        $userInsert = User::insertGetId(['email' => $userEmail, 'password' => $password, 'fname' => $fName, 'lname' => $lName, 'image' => $imageName, 'register_type' => 'E', 'user_type' => 'U','status' => $status]);
        
        $userDetailArray = array(
                                "user_id" => $userInsert,
                                "nick_name" => $nickName,
                                "age" => $age,
                                "gender" => $gender,
                                "genre_id" => $genreId,
                                "artists_id" => $artistId,
                                "state_id" => $stateId,
                                "marital_status" => $maritalStatus,
                                );
        if($userInsert){
            $userDetails = UserDetail::insert($userDetailArray);    
            if($userDetails){
                $notificationSettings = NotificationSetting::insert(['user_id' => $userInsert, 'notification_type' => 'L', 'status' => '1']);
                $notificationSettings = NotificationSetting::insert(['user_id' => $userInsert, 'notification_type' => 'C', 'status' => '1']);
                $notificationSettings = NotificationSetting::insert(['user_id' => $userInsert, 'notification_type' => 'F', 'status' => '1']);
                return Redirect::to("/admin/users")->with("message", 'User Added Successfully');
            }else{
                return Redirect::back()->with("message", "Something went wrong.");
            }
        }else{
            return Redirect::back()->with("message", "Something went wrong.");
        }
    }

    public function updateUser(Request $request){
        //echo'<pre>';print_r($request->all());die;
        $userId = $request->get('id');
        $fName = $request->get('f_name');
        $lName = $request->get('l_name');
        $nickName = $request->get('nick_name');
        $maritalStatus = $request->get('marital_status');
        $age = $request->get('age');
        $gender = $request->get('gender');
        $genreId = $request->get('genre');
        $artistId = $request->get('artists');
        $stateId = $request->get('state');
        $usersArray = array(
                            "fname" => $fName,
                            "lname" => $lName,
                            );
        if($request->hasFile('image')){
            $image = $request->file('image');
            $orignalName = $image->getClientOriginalName();
            $imageName = time().$orignalName;
            $destinationPath = public_path('/images/profile_image/');
            $image->move($destinationPath, $imageName);
            $usersArray['image'] = $imageName;
        }

        $userDetailArray = array(
                                "nick_name" => $nickName,
                                "age" => $age,
                                "gender" => $gender,
                                "genre_id" => $genreId,
                                "artists_id" => $artistId,
                                "state_id" => $stateId,
                                "marital_status" => $maritalStatus,
                                );
        //echo'<pre>';print_r($userDetailArray);die;
        $users = User::where("id", $userId)->update($usersArray);
        $userDetails = UserDetail::where("user_id", $userId)->update($userDetailArray);
        if($userDetails){
            return Redirect::to("/admin/users")->with("message", 'User Updated Successfully');
        }else{
            return Redirect::back()->with("message", "Something went wrong.");
        }

    }

    public function suspendUser($id){
        $user = User::where('id', $id)->update(['status' => '0']);
        if($user){
            return Redirect::to("admin/users")->with("message", "User Successfully suspended.");
        }else{
            return Redirect::to("admin/users")->with("message", "Something went wrong.");
        }
    }

    public function activeUser($id){
        $user = User::where('id', $id)->update(['status' => '1']);
        if($user){
            return Redirect::to("admin/suspendedUsers")->with("message", "User Successfully activated.");
        }else{
            return Redirect::to("admin/suspendedUsers")->with("message", "Something went wrong.");
        }
    }

    public function deleteUser($id){
        $user = User::where('id', $id)->delete();
        if($user){
            $userData = UserDetail::where('user_id', $id)->delete();
            if($userData){
                return Redirect::to("/admin/users")->with("message", "User Successfully deleted");
            }else{
                return Redirect::back()->with("message", "Something went wrong");
            }
        }
    }

    public function events(Request $request){



        $limit = Config::get('constants.PAGINATION_LIMIT');
        if(isset($request['page'])){
            $page = $request['page'];
        }else{
            $page = 0;
        }

       // $getAllEventsName = Event::
        $offset = $page*$limit;
        
       
        
        if(Auth::check()){
            $userId = Auth::id();
            $events = Event::where("status",'0')->skip($offset)->take($limit)->orderBy('id',"DESC")->get();

            $events->map(function ($events) use($userId){
                 $eventId = $events->id;
                 $events->favourite = DB::table("favorites")->where("event_id",$eventId)->where("user_id",$userId)->where("status",1)->count();
            });

        }else{
            $events = Event::where("status",'0')->skip($offset)->take($limit)->orderBy('id',"DESC")->get();    
        }


        $getEventsLatLong = $this->Event->getEventsLatLong();
        $allActiveGenere  = Genre::where("status",1)->orderBy('id',"DESC")->select("id","name")->get();
        
        /*TrendingEvents--*/

         /*Get Favourite--*/
        if(Auth::check()){
            $userId = Auth::id();
            $getMostLikedEventsQuery = "SELECT * FROM `events` WHERE id in(SELECT `receiver_id` FROM `likes` WHERE `receiver_type` = 'E' AND `status` = 1 GROUP BY receiver_id ORDER BY count(`receiver_id`) DESC) LIMIT 6";

          $mostLikedEvents = collect(DB::select( DB::raw($getMostLikedEventsQuery) ));

            $mostLikedEvents->map(function ($mostLikedEvents) use($userId){
                 $eventId = $mostLikedEvents->id;
                 $mostLikedEvents->favourite = DB::table("favorites")->where("event_id",$eventId)->where("user_id",$userId)->where("status",1)->count();
            });

        }else{
          $getMostLikedEventsQuery = "SELECT * FROM `events` WHERE id in(SELECT `receiver_id` FROM `likes` WHERE `receiver_type` = 'E' AND `status` = 1 GROUP BY receiver_id ORDER BY count(`receiver_id`) DESC) LIMIT 6";

          $mostLikedEvents = DB::select( DB::raw($getMostLikedEventsQuery) );    
        }

	   $eventList = DB::table("events")->get();
        return view('users.home',['eventList' => $eventList,'events' => $events,'getEventsLatLong' => $getEventsLatLong,'generes' => $allActiveGenere,'page' => $page,'trending_events' => $mostLikedEvents]);
    }
    public function loadMoreEvents(Request $request){
        $limit = Config::get('constants.PAGINATION_LIMIT');
        if(isset($request['page_no'])){
            $page = $request['page_no'];
        }else{
            $page = 0;
        }

        $offset = $page*$limit;

        if(Auth::check()){
            $userId = Auth::id();
            $events = Event::where("status",'0')->skip($offset)->take($limit)->orderBy('id',"DESC")->get();

            $events->map(function ($events) use($userId){
                 $eventId = $events->id;
                 $events->favourite = DB::table("favorites")->where("event_id",$eventId)->where("user_id",$userId)->where("status",1)->count();
            });

        }else{
            $events = Event::where("status",'0')->skip($offset)->take($limit)->orderBy('id',"DESC")->get();    
        }
        
         if(count($events) > 0){
             return view('events.render.load_more_events',['events' => $events]);
         }else{
            return 0;
         }
    }




    public function editAdminProfileForm(){
       $user = Auth::user();
        $userId = $user->id;
        $fName = $user->fname;
        $lName = $user->lname;
        $userImage = $user->image;
        $userDetails = UserDetail::where('user_id', $userId)->first();
        $age = $userDetails->age;
        $gender = $userDetails->gender;
        $nickName = $userDetails->nick_name;
        $genreId = $userDetails->genre_id;
        $artistId = $userDetails->artists_id;
        $stateId = $userDetails->state_id;
        $maritalStatus = $userDetails->marital_status;
        $genres = Genre::where(['status' => '1'])->get();
        $artists = Artist::where(['status' => '1'])->get();
        $states = State::where(['status' => '1'])->get();

        $data = array(
                    "fname" => $fName,
                    "lname" => $lName,
                    "nickname" => $nickName,
                    "gender" => $gender,
                    "age" => $age,
                    "profile_image" => $userImage,
                    "genreId" => $genreId,
                    "artistId" => $artistId,
                    "marital_status" => $maritalStatus,
                    "stateId" => $stateId,
                    );
        return view('admin.edit-profile', ['genres' => $genres, 'artists' => $artists, 'states' => $states])->with($data);
    }


    public function otherUserProfile($id){
        $user = Auth::user();
        $loggedInUserId = $user->id;
        if($id == $loggedInUserId){
           /* $userId = $user->id;
            $userName = $user->fname.' '.$user->lname;
            $userImage = $user->image;
            $userDetails = UserDetail::where('user_id', $userId)->first();
            $age = $userDetails->age;
            $gender = $userDetails->gender;
            $genreId = $userDetails->genre_id;
            $artistId = $userDetails->artists_id;
            $stateId = $userDetails->state_id;
            $maritalStatus = $userDetails->marital_status;
            $artist = Artist::where(['id' => $artistId])->select('name')->first();
            $genre = Genre::where(['id' => $genreId])->select('name')->first();
            $state = State::where(['id' => $stateId])->select('name')->first();
            $newsFeed = DB::table('feeds')
                                ->join('users', 'feeds.user_id', '=', 'users.id')
                                ->join('user_details', 'feeds.user_id', '=', 'user_details.user_id')
                                //->leftJoin('likes', 'feeds.id', '=', 'likes.receiver_id')
                                ->select('feeds.image As feed_image', 'feeds.video As feed_video', 'feeds.id As feed_id', 'feeds.created_at As feeds_created', 'users.image As user_image', 'users.*', 'feeds.*', 'user_details.*')
                                ->where(['feeds.user_id' => $userId])
                                ->OrderBy('feeds.created_at', 'Desc')->get();
            //echo'<pre>';print_r($newsFeed);die;
            $likes = DB::table('feeds')
                                ->leftJoin('likes', 'feeds.id', '=', 'likes.receiver_id')
                                ->select('feeds.id As feed_id', 'likes.user_id As liked_by_id', 'likes.status As like_status', DB::raw("(select COUNT(id) As total_likes from likes where status='1' and receiver_type ='N' and receiver_id=feeds.id) as total_like"))
                                ->where(['likes.status' => '1'])
                                ->OrderBy('feeds.created_at', 'Desc')->get();
            //echo'<pre>';print_r($likes);die;
            $likesArr = array();
            foreach ($likes as $key => $like) {
                $likesArr[$like->feed_id] = $like->total_like;
            }
            //echo'<pre>';print_r($likesArr);die;
            $comments = DB::table('feeds')
                        ->leftJoin('comments', 'feeds.id', '=', 'comments.receiver_id')
                        ->select('feeds.id As feed_id', DB::raw("(select COUNT(id) As total_comment from comments where receiver_type ='N' and receiver_id=feeds.id) as total_comment"))
                        ->where(['feeds.user_id' => $userId])
                        ->OrderBy('comments.created_at', 'Desc')->get();

            $totalComment = $feedLikes = $feedComment = array();
            foreach ($comments as $key => $comment) {
                $totalComment[$comment->feed_id] = $comment->total_comment;
            }
            //echo'<pre>';print_r($totalComment);die;
            $followings = $followers = 0;
            $follower = DB::table('follows')->where(['following_id' => $userId, 'status' => '1'])->get();
            $following = DB::table('follows')->where(['follower_id' => $userId, 'status' => '1'])->get();
            if($follower){
                $followers = count($follower);
            }
            if($following){
                $followings = count($following);
            }
            $data = array(
                        "user_name" => $userName,
                        "gender" => $gender,
                        "age" => $age,
                        "profile_image" => $userImage,
                        "genre" => $genre->name,
                        "artist" => $artist->name,
                        "marital_status" => $maritalStatus,
                        "state" => $state->name,
                        'followers' => $followers,
                        'followings' => $followings,
                        );
            return view('users.my-profile', ['data' => $data, 'newsFeed' => $newsFeed, 'totalComment' => $totalComment, 'likesArr' => $likesArr])->with($data);
	*/ 
 		return Redirect::to('/my-profile');
        }else{
            $userInfo = DB::table('users')
                                    ->leftjoin('user_details',"users.id", "=", "user_details.user_id")
                                    ->join('genres', 'user_details.genre_id', '=', 'genres.id')
                                    ->join('artists', 'user_details.artists_id', '=', 'artists.id')
                                    ->join('states', 'user_details.state_id', '=', 'states.id')
                                    ->select('users.*', 'user_details.*', 'genres.name As genre', 'artists.name As artist', 'states.name As state')
                                    ->where(["users.id" => $id])
                                    ->first();
            //echo'<pre>';print_r($userInfo);die;
            $blockedUser = DB::table('blocked_users')->where(['user_id' => $loggedInUserId, 'blocked_user_id' => $id, 'status' => 1])->first();
            $follow = DB::table('follows')->where(['follower_id' => $loggedInUserId, 'following_id' => $id, 'status' => '1'])->first();
            $followers = DB::table('follows')->where(['following_id' => $id, 'status' => '1'])->get();
           
$followings = DB::table('follows')->where(['follower_id' => $id, 'status' => '1'])->get();
            $userInfo->followers = $userInfo->followings = $userInfo->block_user = $userInfo->follows = 0;

            $newsFeed = DB::table('feeds')
                            ->join('users', 'feeds.user_id', '=', 'users.id')
                            ->join('user_details', 'feeds.user_id', '=', 'user_details.user_id')
                            
                            ->select('feeds.image As feed_image', 'feeds.video As feed_video', 'feeds.id As feed_id', 'feeds.created_at As feeds_created', 'users.image As user_image', 'users.*', 'feeds.*', 'user_details.*')
                            ->where(['feeds.user_id' => $id])
                            ->OrderBy('feeds.created_at', 'Desc')->get();
            //echo'<pre>';print_r($newsFeed);die;
            // $likes = DB::table('feeds')
            //                     ->leftJoin('likes', 'feeds.id', '=', 'likes.receiver_id')
            //                     ->select('feeds.id As feed_id', 'likes.user_id As liked_by_id', 'likes.status As like_status', DB::raw("(select COUNT(id) As total_likes from likes where status='1' and receiver_type ='N' and receiver_id=feeds.id) as total_like"))
            //                     ->where(['likes.status' => '1'])
            //                     ->OrderBy('feeds.created_at', 'Desc')->get();
            // //echo'<pre>';print_r($likes);die;
            // $likesArr = array();
            // foreach ($likes as $key => $like) {
            //     $likesArr[$like->feed_id] = $like->total_like;
            // }
            // //echo'<pre>';print_r($likesArr);die;
            $comments = DB::table('feeds')
                    ->leftJoin('comments', 'feeds.id', '=', 'comments.receiver_id')
                    ->select('feeds.id As feed_id', DB::raw("(select COUNT(id) As total_comment from comments where receiver_type ='N' and receiver_id=feeds.id) as total_comment"))
                    ->OrderBy('comments.created_at', 'Desc')->get();

            $totalComment = $feedLikes = $feedComment = array();
            foreach ($comments as $key => $comment) {
                $totalComment[$comment->feed_id] = $comment->total_comment;
            }

            $checkFollow = DB::table('follows')->where('follower_id',$loggedInUserId)->where('following_id',$id)->where('status',1)->first();
            if(isset($checkFollow) && !empty($checkFollow)){
                $userInfo->follows = 1;
            }else{
 		$userInfo->follows = 0;
	   }

            if($blockedUser){
                $userInfo->block_user = 1;
            }else{
		 $userInfo->block_user = 0;
	    }

            if(count($followers) >0){
                $userInfo->followers = count($followers);
            }else{
		$userInfo->followers = 0;
	     }

            if(count($followings) >0){
                $userInfo->followings = count($followings);
            }else{
			 $userInfo->followings = 0;
		}
           
            $userInfo->user_id = $id;

            $cityId = $userInfo->city_id;
            $getCityName = DB::table("cities")->where("id",$cityId)->select("name")->first();
            if(isset($getCityName) && !empty($getCityName)){
                $userInfo->city_name = $getCityName->name;
            }else{
                $userInfo->city_name = "";
            }
            return view('users.other-user-profile', ['userInfo' => $userInfo, 'newsFeed' => $newsFeed, 'totalComment' => $totalComment]);
        }
    }

    public function likeNews(Request $request){
        $feedId = $request->get('feed_id');
        $receiverId = $request->get('receiver_id');
        $userId = Auth::id();
        $checkLike = Like::where(['user_id' => $userId, 'receiver_id' => $feedId, 'receiver_type' => 'N'])->first();
        if($checkLike){
            $updateNewsLike = Like::where(['user_id' => $userId, 'receiver_id' => $feedId, 'receiver_type' => 'N'])->update(['status' => 1]);
        }else{
            $newsLike = Like::insert(['user_id' => $userId, 'receiver_id' => $feedId, 'receiver_type' => 'N', 'status' => 1]);
            $postsUserId = Feed::where(['id' => $feedId])->first();
            $checkNotificationSetting = NotificationSetting::where(['user_id' => $postsUserId->user_id, 'notification_type' => 'L'])->first();
            if($checkNotificationSetting->status == 1){
                $notification = Notification::insert(['sender_id' => $userId, 'receiver_id' => $receiverId, 'feed_id' => $feedId, 'notification_for' => 'L']);
            }
        }
        $Likes = Like::where(['receiver_id' => $feedId, 'receiver_type' => 'N', 'status' => 1])->get();
        $totalLikes = count($Likes);
        return response()->json(['success' => true, 'totalLikes' => $totalLikes]);
    }

    public function setings(){
        $loggedInUser = Auth::user();
        //$blockedUserList = BlockedUser::where(['user_id' => $loggedInUser->id, 'status' => '1'])->get();
        $blockedUserList = DB::table('blocked_users')->join('users', 'users.id', '=', 'blocked_users.blocked_user_id')->select('users.fname', 'users.lname', 'users.image', 'blocked_users.status As blocked_status', 'blocked_users.*')->where(['blocked_users.status' => '1', 'blocked_users.user_id' => $loggedInUser->id])->get();
        $notificationSetting = NotificationSetting::where(['user_id' => $loggedInUser->id])->get();
        //echo'<pre>';print_r($notificationSetting);die;
        //$follow = $comment = $like = 1;
        foreach ($notificationSetting as $key => $setting) {
            if($setting->notification_type == 'F'){
                $follow = $setting->status;
            }
            if($setting->notification_type == 'C'){
                $comment = $setting->status;
            }
            if($setting->notification_type == 'L'){
                $like = $setting->status;
            }
        }
        return view('users.setting',['blockedUser' => $blockedUserList, 'like' => $like, 'comment' => $comment, 'follow' => $follow]);
    }

    public function newsComment(Request $request){
        $userId = Auth::id();
        $feedId = $request->get('feed_id');
        $receiverId = $request->get('receiver_id');
        $comment = $request->get('comment');
        $commentInsert = Comment::insert(['user_id' => $userId, 'receiver_id' => $feedId, 'receiver_type' => 'N', 'comment' => $comment, 'created_at' => date('Y-m-d H:i:s')]);
        $postsUserId = Feed::where(['id' => $feedId])->first();
        $checkNotificationSetting = NotificationSetting::where(['user_id' => $postsUserId->user_id, 'notification_type' => 'C'])->first();
        if($checkNotificationSetting->status == '1'){
            $notification = Notification::insert(['sender_id' => $userId, 'receiver_id' => $receiverId, 'feed_id' => $feedId, 'notification_for' => 'C', 'comment' => $comment]);
        }
        if($commentInsert){
            //$comments = array('user_id' => $userId, 'comment' => $comment, 'date_time' => )
            return response()->json(['success' =>true]);
            //return response()->json(['success' =>true, 'comments' => $comments]);
        }
    }

    public function followSetting(Request $request){
        $userId = Auth::id();
        $status = $request->get('followed');
        $checkSettings = NotificationSetting::where(['user_id' => $userId, 'notification_type' => 'F'])->first();
        if($checkSettings){
            $followSetting = NotificationSetting::where(['user_id' => $userId, 'notification_type' => 'F'])->update(['status' => $status]);
        }else{
            $followSetting = NotificationSetting::insert(['user_id' => $userId, 'notification_type' => 'F', 'status' => $status]);
        }
        return response()->json(['success' =>true]);
    }

    public function replyComment(Request $request){
        $userId = Auth::id();
        $status = $request->get('comment');
        $checkSettings = NotificationSetting::where(['user_id' => $userId, 'notification_type' => 'C'])->first();
        if($checkSettings){
            $followSetting = NotificationSetting::where(['user_id' => $userId, 'notification_type' => 'C'])->update(['status' => $status]);
        }else{
            $followSetting = NotificationSetting::insert(['user_id' => $userId, 'notification_type' => 'C', 'status' => $status]);
        }
        return response()->json(['success' =>true]);
    }

    public function likePost(Request $request){
        $userId = Auth::id();
        $status = $request->get('like');
        $checkSettings = NotificationSetting::where(['user_id' => $userId, 'notification_type' => 'L'])->first();
        if($checkSettings){
            $followSetting = NotificationSetting::where(['user_id' => $userId, 'notification_type' => 'L'])->update(['status' => $status]);
        }else{
            $followSetting = NotificationSetting::insert(['user_id' => $userId, 'notification_type' => 'L', 'status' => $status]);
        }
        return response()->json(['success' =>true]);
    }

    public function follow(Request $request){
        $followerId = Auth::id();
        $followingId = $request->get('following_id');
        $checkFollow = Follow::where(['follower_id' => $followerId, 'following_id' => $followingId])->first();
        if($checkFollow){
            $follow = Follow::where(['follower_id' => $followerId, 'following_id' => $followingId])->update(['status' => '1']);

        }else{
            $follow = Follow::insert(['follower_id' => $followerId, 'following_id' => $followingId, 'status' => '1']);   
        }
        $checkNotificationSetting = NotificationSetting::where(['user_id' => $followingId, 'notification_type' => 'F'])->first();
        if($checkNotificationSetting->status == 1){
            $notification = Notification::insert(['sender_id' => $followerId, 'receiver_id' => $followingId, 'feed_id' => '0', 'notification_for' => 'F']);
        }
        $follower = Follow::where(['following_id' => $followingId, 'status' => '1'])->get();
        $followers = count($follower);
        return response()->json(['success' =>true, 'followers' => $followers]);
    }

    public function unFollow(Request $request){
        $followerId = Auth::id();
        $followingId = $request->get('following_id');
        $checkFollow = Follow::where(['follower_id' => $followerId, 'following_id' => $followingId])->first();
        if($checkFollow){
            $follow = Follow::where(['follower_id' => $followerId, 'following_id' => $followingId])->update(['status' => '0']);
        }
        $follower = Follow::where(['following_id' => $followingId, 'status' => '1'])->get();
        $followers = count($follower);
        return response()->json(['success' =>true, 'followers' => $followers]);
    }

    public function notifications(){
        $userId = Auth::id();
        $notifications = DB::table('notifications')
                                ->join('users', 'notifications.sender_id', '=', 'users.id')
                                ->leftjoin('feeds', 'notifications.feed_id', '=', 'feeds.id')
                                ->select('users.fname', 'users.lname', 'users.image', 'notifications.*', 'feeds.title')
                                ->where('notifications.receiver_id', '=', $userId)
                                ->where('notifications.sender_id', '!=' , $userId)
                                ->OrderBy('notifications.created_at', 'Desc')->get();

        //echo'<pre>';print_r($notifications);die;
        return view('users.notification', ['notifications' => $notifications]);
    }

    public function getCities(Request $request){
        $stateId  = $request->get('state_id');
        $getCitiesOfState = DB::table("cities")->where("status",'1')->where("state_id",$stateId)->get();
       
        $options="<option value=''>Select City</option>";
        if(isset($getCitiesOfState) && !$getCitiesOfState->isEmpty()){
            foreach($getCitiesOfState as $cities){
                if($stateId == "1"){
                    $selected = "Selected";
                }else{
                    $selected = "";
                }
                $options .= "<option value='".$cities->id."' $selected>".$cities->name."</option>";
            }
        }
        echo $options;
    }

}
