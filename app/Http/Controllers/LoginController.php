<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserDetail;
use App\Event;
use App\Genre;
use App\Artist;
use App\State;
use Hash;
use Auth;
use Redirect;

class LoginController extends Controller
{
    public function login(Request $request){
    	$validateData = $request->validate([
                                            'email' => 'required',
                                            'password' => 'required'
                                            ]);
        $userEmail = $request->get('email');
        $userPassword = $request->get('password');
        if (Auth::attempt(['email' => $userEmail, 'password' => $userPassword, 'user_type' => 'A'])) {
            $totalUser = $activeUsers = $events = 0;
            $totalUser = User::where(['user_type' => 'U'])->count();
            $activeUsers = User::where(['user_type' => 'U','status' => '1'])->count();
            $events = Event::where(['status' => '0'])->count();
            $suspendedUsers = User::where(['user_type' => 'U','status' => '0'])->count();
            return view('admin.dashboard', ['totalUser' => $totalUser, 'activeUsers' => $activeUsers, 'events' => $events, 'suspendedUsers' => $suspendedUsers]);
        }else{
            Auth::logout();
            return Redirect::back()->with('message','Incorrect Email or Password.');
        }
    }

    public function signUp(Request $request){
        $validateData = $request->validate([
                                            'email' => 'required',
                                            'password' => 'required|confirmed',
                                            ]);
        //echo'<pre>';print_r($request->all());die;
        $userEmail = $request->get('email');
        $checkEmail = User::where('email', $userEmail)->select("id")->first();
        if(!empty($checkEmail)){
            return Redirect::back()->with(['message' => 'Email Already Registered.']);
            die;
        }
        $fName = $request->get('f_name');
        $lName = $request->get('l_name');
        $password = Hash::make($request->get('password'));
        $userInsert = User::insertGetId(['email' => $userEmail, 'password' => $password, 'fname' => $fName, 'lname' => $lName, 'register_type' => 'E', 'user_type' => 'A']);
        if($userInsert){
            Auth::loginUsingId($userInsert);
            return Redirect::to('admin/dashboard');
            //return view('users.create-profile');
        }else{
            return Redirect::back()->with(['message' => 'Something went wrong.']);
        }

    }
    public function logout(){
        Auth::logout();
        return Redirect::to('/admin');
    }
}
