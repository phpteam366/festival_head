<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Genre;
use App\Artist;
use App\Like;
use App\Favorite;
use App\Comment;
use App\User;
use App\Feed;
use App\Feedback;
use App\NotificationSetting;
use DB;
use Redirect;
use Auth;
use Config;
class EventController extends Controller
{
 
    private $Event;
    public function __construct(){
        $this->Event = new Event();
    }
    /* Admin---*/
    public function eventlist(){
        $getAllPendingEvents= $this->Event->getEventsList(0);
        return view('events.event-list',compact('getAllPendingEvents'));
    }

    public function upcomingEventsList(){
        $getAllUpcomingEvents= $this->Event->getEventsList(1);
        return view('events.upcoming-event-list',compact('getAllUpcomingEvents'));   
    }

    public function addEventForm(){
        $getAllArtists = Artist::where("status",'1')->orderBy('id',"DESC")->get();
        $getAllGenere  = Genre::where("status",'1')->orderBy('id',"DESC")->get();
        return view('events.add-event',compact('getAllArtists','getAllGenere'));
    }
    public function editEventForm($eventId){
        $getAllArtists = Artist::where("status",'1')->orderBy('id',"DESC")->get();
        $getAllGenere  = Genre::where("status",'1')->orderBy('id',"DESC")->get();
        $eventDetails  = Event::find($eventId);
        return view('events.edit-event',compact('getAllArtists','getAllGenere','eventDetails'));
    }
   
    
     public function addEvent(Request $request){

        $validatedData = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'location_gmap' => 'required',
            'ticket_url' => 'required',
            'artist' => 'required',
            'genere_id' => 'required',
        ]);

        $eventTitle       = $request->get("title");
        $eventDescription = $request->get("description");
        $eventLocation    = $request->get("location");
        $eventlLatitude   = $request->get("lat_gmap");
        $eventLongitude   = $request->get("long_gmap");
        $eventTicketUrl   = $request->get("ticket_url");
        $eventGenere      = $request->get("genere_id");
        $eventArtists     = $request->get("artist");
        if(!empty($eventArtists)){
            $artists = implode(",",$eventArtists);
        }
        if(!empty($eventGenere)){
            $genres = implode(",",$eventGenere);
        }

         $eventDateTime = $request->get("event_date")." ".$request->get("event_time");


        $imageName = "";
        if ($request->hasFile('event_image')) {
            $image = $request->file('event_image');
            $originalName = $image->getClientOriginalExtension();
            $name = time().'.'.$originalName;
            $destinationPath = public_path('/images/events_images');
            $image->move($destinationPath, $name);
            $imageName = $name;
        }

        
        $insertEvent = Event::insertGetId([
                        'name'        => $eventTitle,
                        'description' => $eventDescription,
                        'location'    => $eventLocation,
                        'latitude'    => $eventlLatitude,
                        'longitude'   => $eventLongitude,
                        'genre_id'    => $genres,
                        'image'       => $imageName,
                        'artists_id'  => $artists,
                        'ticket_url'  => $eventTicketUrl,
                        'date_time'   => $eventDateTime,
                        'status'     => '0'
                    ]);
        if($insertEvent){
             $chatData = [
                            'chat_type' => "G",
                            'group_name' => $eventTitle,
                            'event_id' =>   $insertEvent 
                        ];
                /*Insert Chat as group--*/
                DB::table("chats")->insert($chatData );

            return Redirect::to('/admin/events')->with('success',"Event added successfully");
         }else{
            return Redirect::back()->with('error',"Event failed to add");
         }

    }

    public function updateEvent(Request $request){

        $validatedData = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'location_gmap' => 'required',
            'ticket_url' => 'required',
            'artist' => 'required',
            'genere_id' => 'required',
        ]);
	    $eventId          = $request->get("event_id");
        $eventTitle       = $request->get("title");
        $eventDescription = $request->get("description");
        $eventLocation    = $request->get("location");
        $eventlLatitude   = $request->get("lat_gmap");
        $eventLongitude   = $request->get("long_gmap");
        $eventTicketUrl   = $request->get("ticket_url");
        $eventGenere      = $request->get("genere_id");
        $eventArtists     = $request->get("artist");
        if(!empty($eventArtists)){
             $artists = implode(",",$eventArtists);
        }
        if(!empty($eventGenere)){
             $generes = implode(",",$eventGenere);
        }

        $eventDateTime = $request->get("event_date")." ".$request->get("event_time");


        $imageName = $request->get('old_image');
        if ($request->hasFile('event_image')) {
            $image = $request->file('event_image');
            $originalName = $image->getClientOriginalExtension();
            $name = time().'.'.$originalName;
            $destinationPath = public_path('/images/events_images');
            $image->move($destinationPath, $name);
            $imageName = $name;
        }

        $updatedArray = [
                            'name'        => $eventTitle,
                            'description' => $eventDescription,
                            'location'    => $eventLocation,
                            'latitude'    => $eventlLatitude,
                            'longitude'   => $eventLongitude,
                            'genre_id'    => $generes,
                            'image'       => $imageName,
                            'artists_id'  => $artists,
                            'ticket_url'  => $eventTicketUrl,
                            'date_time'   => $eventDateTime,
                            'status'     => '0',
                            'updated_at' => date("Y-m-d H:i:s")
                        ];

        $updateEvent = Event::where('id',$eventId)->update($updatedArray);
        if($updateEvent){
		return redirect::to('admin/upcoming-events')->with('success',"Event updated successfully");
        }else{
            return Redirect::back()->with('error',"Event updated faoled");
        }
 
    }

    public function viewEvent($id){
        $event = DB::table("events")
                ->join("genres","genres.id", "=","events.genre_id")
                ->leftjoin("artists",\DB::raw("FIND_IN_SET(artists.id,events.artists_id)"),">",\DB::raw("'0'"))
                ->select("events.*","genres.name as genere_name",  \DB::raw("GROUP_CONCAT(artists.name) as artists_name"))
                ->where(['events.status' => 1, 'events.id' => $id])
                ->first();

                $genereIds = explode(",",$event->genre_id);
                $genereName = DB::table("genres")->select(DB::raw("GROUP_CONCAT(genres.name) as genres_name"))->whereIn("id",$genereIds)->first();
                if(!empty($genereName)){
                    $genere = $genereName->genres_name;
                }else{
                    $genere = "";
                }
                $event->genere_name = $genere;


                //echo'<pre>';print_r($event);die;
        return view('events.viewEvent', ['event' => $event]);
    }

    public function artistList(){
        $artists = Artist::all();
        return view('events.artist', ["artists" => $artists]);
    }

    public function genreList(){
        $genres = Genre::all();
        return view('events.genre', ["genres" => $genres]);   
    }

    public function viewArtist($id){
        $artists = Artist::where('id', $id)->first();
        return view('events.viewArtist', ['artists' => $artists]);
    }

    public function viewGenre($id){
        $genres = Genre::where('id', $id)->first();
        return view('events.viewGenre', ['genres' => $genres]);
    }

    public function saveArtist(Request $request){
        $name = $request->get('name');
        $status = $request->get('status');

        if($request->hasFile('image')){
            $image = $request->file('image');
            $orignalName = $image->getClientOriginalName();
            $imageName = time().$orignalName;
            $destinationPath = public_path('/images/');
            $image->move($destinationPath, $imageName);
        }else{
            $imageName = "dummy.png";
        }

        $insertArtist = Artist::insert(['name' => $name, 'image' => $imageName, 'status' => $status]);
        if($insertArtist){
            return Redirect::to('/admin/artist')->with('message', 'Artist Added Successfully');
        }else{
            return Redirect::back()->with('message', 'Something went wrong');
        }
    }

    public function saveGenre(Request $request){
        $name = $request->get('name');
        $status = $request->get('status');

        $insertGenre = Genre::insert(['name' => $name, 'status' => $status]);
        if($insertGenre){
            return Redirect::to('/admin/genre')->with('message', 'Genre Added Successfully');
        }else{
            return Redirect::back()->with('message', 'Something went wrong');
        }
    }

    public function editArtist($id){
        $artists = Artist::where('id', $id)->first();
        return view('events.editArtist',['artists' => $artists]);
    }

    public function editGenre($id){
        $genres = Genre::where('id', $id)->first();
        return view('events.editGenre',['genres' => $genres]);
    }

    public function updateArtist(Request $request){
        $artistId = $request->get('id');
        $name = $request->get('name');
        $status = $request->get('status');
        if($request->hasFile('image')){
            $image = $request->file('image');
            $orignalName = $image->getClientOriginalName();
            $imageName = time().$orignalName;
            $destinationPath = public_path('/images/');
            $image->move($destinationPath, $imageName);
        }else{
            $imageName = "dummy.png";
        }

        $insertArtist = Artist::where('id', $artistId)->update(['name' => $name, 'image' => $imageName, 'status' => $status]);
        if($insertArtist){
            return Redirect::to('/admin/artist')->with('message', 'Artist Updated Successfully');
        }else{
            return Redirect::back()->with('message', 'Something went wrong');
        }
    }

    public function updategenre(Request $request){
        $genreId = $request->get('id');
        $name = $request->get('name');
        $status = $request->get('status');

        $insertGenre = Genre::where('id', $genreId)->update(['name' => $name, 'status' => $status]);
        if($insertGenre){
            return Redirect::to('/admin/genre')->with('message', 'Genre Updated Successfully');
        }else{
            return Redirect::back()->with('message', 'Something went wrong');
        }
    }

    public function deleteArtist($id){
        $artist = Artist::where('id', $id)->delete();
        if($artist){
            return Redirect::to("/admin/artist")->with("message", "Artist Successfully deleted");
        }else{
            return Redirect::back()->with("message", "Something went wrong");
        }
    }

    public function deleteGenre($id){
        $genre = Genre::where('id', $id)->delete();
        if($genre){
            return Redirect::to("/admin/genre")->with("message", "Genre Successfully deleted");
        }else{
            return Redirect::back()->with("message", "Something went wrong");
        }
    }

    public function eventDetails($id){
        $userId = Auth::id();
        $event = Event::where('id', $id)->first();
        $genreIds = explode(',', $event->genre_id);
        $artistIds = explode(',', $event->artists_id);
        $genres = Genre::whereIn('id', $genreIds)->get();
        foreach ($genres as $key => $genre) {
            $genreNames[] = $genre->name;
        }
        $musicGenre = implode(' , ', $genreNames);
        $artists = Artist::whereIn('id', $artistIds)->get();
        $likes = Like::where(['receiver_id' => $event->id, 'receiver_type' => 'E', 'status' => 1])->get();
        $userLike = Like::where(['user_id' => $userId, 'receiver_id' => $event->id, 'receiver_type' => 'E', 'status' => 1])->first();
        $eventComment = Comment::where(['receiver_id' => $event->id, 'receiver_type' => 'E'])->OrderBy('created_at', 'Desc')->get();
        //$commentCount = Comment::where(['receiver_id' => $event->id, 'receiver_type' => 'E'])->get();
        $users = User::all();
        $userImg = $userArr = array();
        foreach ($users as $key => $user) {
            $userArr[$user->id] =  $user->fname.' '.$user->lname;   
            $userImg[$user->id] = $user->image;
        }
        if($userLike){
            $event['user_like'] = 1;
        }else{
            $event['user_like'] = 0;
        }
        $feedback = DB::table('feedbacks')
                            ->join('users', 'feedbacks.user_id', '=', 'users.id')
                            ->select('users.fname', 'users.lname', 'users.image', DB::raw('(select avg(ratings) from feedbacks as rating_average) As rating_average'), 'feedbacks.*')
                            ->where(['feedbacks.event_id' => $id])->get();
                            //echo'<pre>';print_r($feedback);
        $totalFeedback = '0';
        $ratings = '0';
        if(!empty($feedback)){
            $totalFeedback = count($feedback);
            if(!empty($totalFeedback)){
                $ratings = floor($feedback[0]->rating_average);
            }
        }
        $event['comment_count'] = count($eventComment);
        $event['comment'] = $eventComment;
        $event['genre'] = $musicGenre;
        $event['artist'] = $artists;
        $event['like'] = count($likes);
        //echo'<pre>';print_r($event->artist);die;
        return view('events.event-details', ['event' => $event, 'userArr' => $userArr, 'userImg' => $userImg, 'feedback' => $feedback, 'totalFeedback' => $totalFeedback, 'ratings' => $ratings]);
    }


    public function deleteEvent($eventId){
        if(Event::where("id",$eventId)->delete()){
            return Redirect::back()->with('success',"Event deleted successfully");  
        }
        return Redirect::back()->with('error',"Something went wrong");
    }


    public function likeEvent(Request $request){
        $userId = $request->get('user_id');
        $receiverId = $request->get('event_id');
        $receiverType = $request->get('receiver_type');
        $checkLike = Like::where(['user_id' => $userId, 'receiver_id' => $receiverId, 'receiver_type' => $receiverType])->first();
        if($checkLike){
            if($checkLike->status == 0){
                $updatLike = Like::where(['user_id' => $userId, 'receiver_id' => $receiverId, 'receiver_type' => $receiverType])->update(["status" => 1]);
            }
        }else{
            $likeInsert = Like::insert(['user_id' => $userId, 'receiver_id' => $receiverId, 'receiver_type' => $receiverType, 'status' => 1]);
        }
        $like = Like::where(['receiver_id' => $receiverId, 'receiver_type' => $receiverType, 'status' => 1])->get();
        $eventLike = count($like);
        return response()->json(['success' => true, 'like' => $eventLike]);
    }

    public function unlikeEvent(Request $request){
        $userId = $request->get('user_id');
        $receiverId = $request->get('event_id');
        $receiverType = $request->get('receiver_type');
        $checkLike = Like::where(['user_id' => $userId, 'receiver_id' => $receiverId, 'receiver_type' => $receiverType])->first();
        if($checkLike){
            $updatLike = Like::where(['user_id' => $userId, 'receiver_id' => $receiverId, 'receiver_type' => $receiverType])->update(["status" => 0]);
        }
        $like = Like::where(['receiver_id' => $receiverId, 'receiver_type' => $receiverType, 'status' => 1])->get();
        $eventLike = count($like);
        return response()->json(['success' => true, 'like' => $eventLike]);
    }

    public function favouriteEvent(Request $request){
        $userId = Auth::id();
        $status = $request->get('status');
        $eventId = $request->get('eventId');
        $checkInsert = Favorite::where(['user_id' => $userId, 'event_id' => $eventId])->first();
        if($checkInsert){
           $updatFavorite = Favorite::where(['user_id' => $userId, 'event_id' => $eventId])->update(['status' => $status]);
        }else{
            $favouriteInsert = Favorite::insert(['user_id' => $userId, 'event_id' => $eventId, 'status' => 1]);
        }
    }

    public function unfavouriteEvent(Request $request){
        $userId = $request->get('user_id');
        $eventId = $request->get('event_id');
        $checkInsert = Favorite::where(['user_id' => $userId, 'event_id' => $eventId])->first();
        if($checkInsert){
            $updatFavorite = Favorite::where(['user_id' => $userId, 'event_id' => $eventId])->update(["status" => 0]);
        }
        return response()->json(['success' => true]);
    }

    public function favouriteEventList(Request $request){
        $userId = Auth::id();
        $favoriteEvents = Favorite::where(['user_id' => $userId,'status' => 1])->orderBy('updated_at',"Desc")->get();
	if(isset($favoriteEvents) && ! $favoriteEvents->isEmpty()){ 
		$eventIds = array();
		foreach ($favoriteEvents as $key => $favoriteEvent) {
		    $eventIds[] = $favoriteEvent->event_id;
		}

		$ids_ordered = implode(',', $eventIds);
		$events = Event::wherein('id', $eventIds)->orderByRaw(DB::raw("FIELD(id, $ids_ordered)"))->get(); //->orderByRaw('FIELD(id, '. $eventsss.')')
	}else{
		 $events = array();
	}
        return view('events.favorites', ["events" => $events]);
    }
    
    

    /* Filter with  location--*/
/*    function filterEvents(Request $request){

        try{
            $locations = array();
            $genere="";$event="";
            if(isset($request['event_latitude'])){
                $miles = Config::get("constants.FILTER_MILES");
                $latitude  = $request['event_latitude'];
                $longitude = $request['event_longitude'];
                $location = $request['location'];
                
                $locations['location'] = $location;
                $locations['latitude'] = $latitude;
                $locations['longitude'] = $longitude;
                 
                $sql = "SELECT  latitude,longitude,date_time FROM events WHERE `events`.`status` = '0' AND
                            latitude BETWEEN ({$latitude} - ({$miles}*0.018)) AND ({$latitude} + ({$miles}*0.018)) AND
                            longitude BETWEEN ({$longitude} - ({$miles}*0.018)) AND ({$longitude} + ({$miles}*0.018))";

                $getEventsLatLong = DB::select(DB::raw($sql));

            }else if(isset($request['genere'])){
                $getEventsLatLong = DB::table("events");

                if($request['genere'] != ""){
                     $genere = $request['genere'];
                     $getEventsLatLong = $getEventsLatLong->where('genre_id',$genere);
                }
                if($request['event'] != ""){
                     $event = $request['event'];
                     $getEventsLatLong = $getEventsLatLong->where('id',$event);
                }
                $getEventsLatLong->where("status",'0')->select("latitude","longitude","date_time");
                $getEventsLatLong = $getEventsLatLong->get();
                
            }else{
                $getEventsLatLong = $this->Event->getEventsLatLong();
            }

            $events = Event::where("status",1)->orderBy('id',"DESC")->get();
            $allActiveGenere  = Genre::where("status",1)->orderBy('id',"DESC")->select("id","name")->get();

            
        if(Auth::check()){
            $userId = Auth::id();
            $getMostLikedEventsQuery = "SELECT * FROM `events` WHERE id in(SELECT `receiver_id` FROM `likes` WHERE `receiver_type` = 'E' AND `status` = 1 GROUP BY receiver_id ORDER BY count(`receiver_id`) DESC) LIMIT 6";

          $mostLikedEvents = collect(DB::select( DB::raw($getMostLikedEventsQuery) ));

            $mostLikedEvents->map(function ($mostLikedEvents) use($userId){
                 $eventId = $mostLikedEvents->id;
                 $mostLikedEvents->favourite = DB::table("favorites")->where("event_id",$eventId)->where("user_id",$userId)->where("status",1)->count();
            });

        }else{
          $getMostLikedEventsQuery = "SELECT * FROM `events` WHERE id in(SELECT `receiver_id` FROM `likes` WHERE `receiver_type` = 'E' AND `status` = 1 GROUP BY receiver_id ORDER BY count(`receiver_id`) DESC) LIMIT 6";

          $mostLikedEvents = DB::select( DB::raw($getMostLikedEventsQuery) );    
        }
         $eventList = DB::table('events')->get();
        return view('users.home',['eventList'=>$eventList,'events' => $events,'getEventsLatLong' => $getEventsLatLong,'generes' => $allActiveGenere,'locations' => $locations,'trending_events' => $mostLikedEvents]);
    }catch (\Exception $e) {
            return Redirect::to('/');
        }
}*/






function filterEvents(Request $request){

        try{
            $locations = array();
            $genere="";$event="";
            if(isset($request['event_name'])){
               
                $eventName = $request['event_name'];
                 
                $sql = "SELECT  latitude,longitude,date_time FROM events WHERE `events`.`status` = '0' AND
                           `events`.`name` LIKE '%$eventName%'";

                $getEventsLatLong = DB::select(DB::raw($sql));

                $sqlToSelectEvent = "SELECT  `events`.* FROM events WHERE `events`.`status` = '0' AND
                           `events`.`name` LIKE '%$eventName%'";

                $events = DB::select(DB::raw($sqlToSelectEvent));

            }else if(isset($request['genere'])){

                $getEventsLatLong = DB::table("events");

                if($request['genere'] != ""){
                     $genere = $request['genere'];
                     //$getEventsLatLong = $getEventsLatLong->where('genre_id',$genere);
                     $getEventsLatLong = $getEventsLatLong->whereRaw("find_in_set($genere,events.genre_id)");
                }
                if($request['event'] != ""){
                     $event = $request['event'];
                     $getEventsLatLong = $getEventsLatLong->where('id',$event);
                }
                $getEventsLatLong->where("status",'0')->select("latitude","longitude","date_time");
                $getEventsLatLong = $getEventsLatLong->get();



                $getEvents = DB::table("events");

                if($request['genere'] != ""){
                     $genere = $request['genere'];
                     $getEvents = $getEvents->whereRaw("find_in_set($genere,events.genre_id)");
                }
                if($request['event'] != ""){
                     $event = $request['event'];
                     $getEvents = $getEvents->where('id',$event);
                }
                $getEvents->where("status",'0')->select("*")->orderBy("events.id","DESC");
                $getEvents = $getEvents->get();
                $events = $getEvents;


            }else{
                $getEventsLatLong = $this->Event->getEventsLatLong();
                $events = Event::where("status",1)->orderBy('id',"DESC")->get();
            }



             if(Auth::check()){
               $userId = Auth::id();
               $events = collect($events);
               $events->map(function ($events) use($userId){
                     $eventId = $events->id;
                     $events->favourite = DB::table("favorites")->where("event_id",$eventId)->where("user_id",$userId)->where("status",1)->count();
                });

            }else{
                $events = $events;    
            }



            $allActiveGenere  = Genre::where("status",1)->orderBy('id',"DESC")->select("id","name")->get();

             /*Get Favourite--*/
        if(Auth::check()){
             $userId = Auth::id();
             $getMostLikedEventsQuery = "SELECT * FROM `events` WHERE id in(SELECT `receiver_id` FROM `likes` WHERE `receiver_type` = 'E' AND `status` = 1 GROUP BY receiver_id ORDER BY count(`receiver_id`) DESC) LIMIT 6";

             $mostLikedEvents = collect(DB::select( DB::raw($getMostLikedEventsQuery) ));

            $mostLikedEvents->map(function ($mostLikedEvents) use($userId){
                 $eventId = $mostLikedEvents->id;
                 $mostLikedEvents->favourite = DB::table("favorites")->where("event_id",$eventId)->where("user_id",$userId)->where("status",1)->count();
            });

        }else{
          $getMostLikedEventsQuery = "SELECT * FROM `events` WHERE id in(SELECT `receiver_id` FROM `likes` WHERE `receiver_type` = 'E' AND `status` = 1 GROUP BY receiver_id ORDER BY count(`receiver_id`) DESC) LIMIT 6";
          $mostLikedEvents = DB::select( DB::raw($getMostLikedEventsQuery) );    
        }


        $eventList = DB::table('events')->get(); /*get all events for Search--*/
        return view('users.home',['eventList'=>$eventList,'events' => $events,'getEventsLatLong' => $getEventsLatLong,'generes' => $allActiveGenere,'locations' => $locations,'trending_events' => $mostLikedEvents]);
    }catch (\Exception $e) {
            return Redirect::to('/');
        }
}







    public function eventComment(Request $request){
        //echo'<pre>';print_r($request->all());die;
        $userId = $request->get('user_id');
        $eventId = $request->get('event_id');
        $comment = $request->get('comment');
        $commentInsert = Comment::insert(['user_id' => $userId, 'receiver_id' => $eventId, 'receiver_type' => 'E', 'comment' => $comment, 'created_at' => date('Y-m-d H:i:s')]);
        if($commentInsert){

            //$comments = array('user_id' => $userId, 'comment' => $comment, 'date_time' => )
            return response()->json(['success' =>true]);
            //return response()->json(['success' =>true, 'comments' => $comments]);
        }
    }

    public function addEventNewsFeed(Request $request){
        $userId = Auth::id();
        
        $newsFeed = DB::table('feeds')
                    ->join('users', 'feeds.user_id', '=', 'users.id')
                    ->join('user_details', 'feeds.user_id', '=', 'user_details.user_id')
                    /*->join('user_details', 'feeds.user_id', '=', 'user_details.user_id')*/
                    ->select('feeds.id as feed_id','feeds.image As feed_image', 'feeds.video As feed_video', 'feeds.title', 'feeds.description', 'feeds.id As feed_id', 'feeds.created_at As feeds_created', 'users.*', 'user_details.*')
                    ->OrderBy('feeds.created_at', 'Desc')->get();
        
        //echo'<pre>';print_r($newsFeed);die;
        $comments = DB::table('feeds')
                    ->join('users', 'feeds.user_id', '=', 'users.id')
                    ->leftJoin('comments', 'feeds.id', '=', 'comments.receiver_id')
                    ->select('feeds.id As feed_id', 'users.fname', 'users.lname', 'users.image As user_image', DB::raw("(select COUNT(id) As total_comment from comments where receiver_type ='N' and receiver_id=feeds.id) as total_comment"), 'comments.created_at As commented_at', 'comments.*')
                    ->OrderBy('comments.created_at', 'Desc')->get();

        $totalComment = $feedLikes = $feedComment = array();
        foreach ($comments as $key => $comment) {
            $feedComment[$comment->feed_id][] = $comment;
            $totalComment[$comment->feed_id] = $comment->total_comment;
        }

        $likes = DB::table('feeds')
                    ->leftJoin('likes', 'feeds.id', '=', 'likes.receiver_id')
                    ->select('feeds.id As feed_id', DB::raw("(select COUNT(id) As total_like from likes where receiver_type ='N' and receiver_id=feeds.id) as total_like"))->get();
        //echo'<pre>';print_r($newsFeed);die;
        foreach ($likes as $key => $like) {
            $feedLikes[$like->feed_id] = $like->total_like;
        }
        
        return view('events.news-feed', ['newsFeed' => $newsFeed, 'feedComment' => $feedComment, 'feedLikes' => $feedLikes, 'totalComment' => $totalComment, 'comments' => $comments]);
    }

    public function addNewsFeed(Request $request){
        $userId = Auth::id();
        $title = $request->get('title');
        $description = $request->get('description');
        $filetype = "";
        if($request->hasFile('files')){
            $mime = $request->file('files')->getMimeType();
            //echo '<pre>';print_r($request->files);die;
            if(strstr($mime, "video/")){
                $filetype = "video";
            }else if(strstr($mime, "image/")){
                $filetype = "image";
            }
            $imageVideo = $request->file('files');
            $orignalName = $imageVideo->getClientOriginalName();
            $imageVideoName = time().$orignalName;
            $destinationPath = public_path('/images/news_feeds/');
            $imageVideo->move($destinationPath, $imageVideoName);

        }else{
            $imageVideoName = "";
        }
        if($filetype == 'image'){
            $data = ['user_id' => $userId, 'title' => $title, 'image' => $imageVideoName, 'video' => '', 'description' => $description, 'status' => 1, 'created_at' => date('Y-m-d H:i:s')];
        }elseif($filetype == 'video'){
            $data = ['user_id' => $userId, 'title' => $title, 'image' => '', 'video' => $imageVideoName, 'description' => $description, 'status' => 1, 'created_at' => date('Y-m-d H:i:s')];
        }else{
            $data = ['user_id' => $userId, 'title' => $title, 'image' => '', 'video' => '', 'description' => $description, 'status' => 1, 'created_at' => date('Y-m-d H:i:s')];
        }
        //echo'<pre>';print_r($data);die;
        $newsFeed = Feed::insert($data);
        if($newsFeed){
            return Redirect::back();
        }
    }

    public function newsFeedDetails($id){
        $feedDetail = DB::table('feeds')
                                        ->join('users', 'feeds.user_id', '=', 'users.id')
                                        ->select('feeds.*', 'users.image As user_image')
                                        ->where(['feeds.id' => $id])->first();
        // $comments = DB::table('feeds')
        //                     ->join('users', 'feeds.user_id', '=', 'users.id')
        //                     ->join('user_details', 'feeds.user_id', '=', 'user_details.user_id')
        //                     //->join('notification_settings', 'feeds.user_id', '=', 'notification_settings.user_id')
        //                     //->leftJoin('likes', 'feeds.id', '=', 'likes.receiver_id')
        //                     ->leftJoin('comments', 'feeds.id', '=', 'comments.receiver_id')
        //                     ->select('feeds.image As feed_image', 'feeds.id As feed_id', 'feeds.created_at As feeds_created', 'users.image As user_image', 'users.*', 'feeds.*', 'user_details.*', 'comments.user_id As commented_by_id', 'comments.comment', 'comments.created_at As commented_at')
        //                     ->where(['feeds.id' => $id])
        //                     ->OrderBy('comments.created_at', 'Desc')->get();

        $comments = DB::table('comments')
                            ->join('users', 'comments.user_id', '=', 'users.id')
                            ->join('user_details', 'comments.user_id', '=', 'user_details.user_id')
                            //->join('notification_settings', 'feeds.user_id', '=', 'notification_settings.user_id')
                            //->leftJoin('likes', 'feeds.id', '=', 'likes.receiver_id')
                            ->Join('feeds', 'comments.receiver_id', '=', 'feeds.id')
                            ->select('feeds.image As feed_image', 'feeds.id As feed_id', 'feeds.created_at As feeds_created', 'users.image As user_image', 'users.*', 'feeds.*', 'user_details.*', 'comments.user_id As commented_by_id', 'comments.comment', 'comments.created_at As commented_at')
                            ->where(['feeds.id' => $id])
                            ->OrderBy('comments.created_at', 'Desc')->get();
                            //echo'<pre>';print_r($feedDetail);die;
        $likes = Like::where(['receiver_id' => $id,'receiver_type' => 'N'])->get();
        $totalLikes = count($likes);
        $Comments = Comment::where(['receiver_id' => $id,'receiver_type' => 'N'])->get();
        $totalComments = count($Comments);
        return view('events.news-feed-details',['feedDetail' => $feedDetail, 'comments' => $comments, 'totalComments' => $totalComments, 'totalLikes' => $totalLikes]);
    }

    public function feedback(Request $request){
        $eventId = $request->get('event_id');
        $ratings = $request->get('star');
        $comment = $request->get('comment');
        $userId = Auth::id();
        $checkFeedback = Feedback::where(['user_id' => $userId, 'event_id' => $eventId])->first();
        //echo'<pre>';print_r($checkFeedback);die;
        if($checkFeedback){
            return response()->json(['message' => 'already added']);
        }else{
            $feedback = Feedback::insert(['user_id' => $userId, 'event_id' => $eventId, 'ratings' => $ratings, 'reviews' => $comment]);
        }
        return response()->json(['success' => true]);
    }

  public function feedComments(Request $request){
        $feedId = $request->get('feed_id');
       // $limit  = Config::get('constants'); 

       /* $comments = DB::table('feeds')
                    ->join('users', 'feeds.user_id', '=', 'users.id')
                    ->leftJoin('comments', 'feeds.id', '=', 'comments.receiver_id')
                    ->select('feeds.id As feed_id', 'users.fname', 'users.lname', 'users.image As user_image', DB::raw("(select COUNT(id) As total_comment from comments where receiver_type ='N' and receiver_id=feeds.id) as total_comment"), 'comments.*')
                    ->where(['feeds.id' => $feedId])
                    ->OrderBy('comments.created_at', 'Desc')->get();*/
         $comments = DB::table('comments')
            ->join('users', 'comments.user_id', '=', 'users.id')
            ->select('comments.receiver_id As feed_id', 'users.fname', 'users.lname', 'users.image As user_image', DB::raw("(select COUNT(id) As total_comment from comments where receiver_type ='N' and receiver_id=comments.receiver_id) as total_comment"), 'comments.*')
            ->where(['comments.receiver_id' => $feedId,'comments.receiver_type' => 'N'])
            ->OrderBy('comments.created_at', 'Desc')
            ->get();
        //echo "<pre>";print_r($comments);die;
        return response()->json(['data' => $comments]);
    }
}
