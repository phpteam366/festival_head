<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
   protected $table="messages";
   /*Get message data with chat id--*/
    public function getMessageData($chatId,$offset,$limit){
        $messages = DB::table("messages")
                    ->join("users as sender","sender.id","=","messages.sender_id")
                    ->where("messages.chat_id",$chatId)
                    ->offset($offset)
                    ->take($limit)
                     ->select("messages.*","sender.image","sender.fname as sender_fname","sender.lname as sender_lname")
                    ->orderBy("messages.id","DESC")
                    ->get();
        
        $messages = $messages->toArray();
        return array_reverse($messages, true);
    }

	 public function getNewMessages($chatId,$lastId){
        $messages = DB::table("messages")
                        ->where("chat_id",$chatId)
                        ->where('id', '>', $lastId)
                        ->get();
       
        $messages = $messages->toArray();
        return array_reverse($messages, true);
    }
}
