<!DOCTYPE html>
<html lang="en">
<head>
  <title>Festival Head</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{URL::to('/')}}/public/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{URL::to('/')}}/public/css/style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<section class="haeder_main">
	<div class="container">
		<nav class="navbar navbar-inverse navbr_home">
		  <div class="container-fluid">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>                        
			  </button>
			  <a class="navbar-brand" href="index.html"><img src="{{URL::to('/')}}/public/images/logo.png"></a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<div class="secrh_filed pull-left">
					<input type="text" class="form-control" placeholder="Search Location">
				</div>
				<div class="btn_othr_prfol pull-right">
					<ul class="nav navbar-nav">
						<li><a href="login.html" class="btn btn_blush block_btn">Login</a></li>
						<li><a href="signup.html" class="btn block_btn">Sign Up</a></li>
					</ul>
				</div>
			</div>
		  </div>
		</nav>
	</div>
</section>

<section class="filtr_section">
	<div class="container">
		<div class="col-md-12">
			<div class="filter_section_inner">
				<ul>
					<li>
						<div class="filed_sction">
							<select class="form-control">
								<option>Select Users</option>
								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
							</select>
						</div>
					</li>
					<li>
						<div class="filed_sction">
							<select class="form-control">
								<option>Select Event</option>
								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
							</select>
						</div>
					</li>
					<li>
						<div class="filed_sction">
							<select class="form-control">
								<option>Select Genre</option>
								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
							</select>
						</div>
					</li>
					<li>
						<button class="btn_filter">Search</button>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<section class="map_section">
	<img src="{{URL::to('/')}}/public/images/map_image.png">
</section>

<section class="event_section">
	<div class="inner_contnt_sction text-center">
		<h1>Event LIST</h1>
		<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form,  by injected humour</p>
	</div>
	<div class="container">
		<div class="row">
			<div class="event_section_outer">
				<div class="col-md-4 col-sm-6">
					<div class="event_section_inner">
						
							<div class="inner_event_img">
								<img src="{{URL::to('/')}}/public/images/img1.png">
									<div class="date_home">
										<h3>21 </h3>
										<p>February</p>
									</div>
									<div class="hert_like">
										<a href="#"><div class="plus heart minus"></div></a>
									</div>
							</div>
							<div class="inner_event_descrption">
								<h3>Northwest Folklife Festival</h3>
								<p>Lorem Ipsum is simply dummy text of the printing </p>
								<h6><img src="{{URL::to('/')}}/public/images/location.png">Ct Eureka, California(CA), 95501</h6>
									<div class="evnt_button">
										<ul>
											<li><a class="btn_yellow btn_css text-uppercase">Buy tickets</a></li>
											<li><a href="event_details.html" class="btn_green btn_css text-uppercase">view details</a></li>
										</ul>
									</div>
							</div>
						
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="event_section_inner">
						<div class="inner_event_img">
							<img src="{{URL::to('/')}}/public/images/img1.png">
								<div class="date_home">
									<h3>10 </h3>
									<p>November</p>
								</div>
								<div class="hert_like">
									<a href="#"><div class="plus heart"></div></a>
								</div>
						</div>
						<div class="inner_event_descrption">
							<h3>Chirstmas Party Event</h3>
							<p>Lorem Ipsum is simply dummy text of the printing </p>
							<h6><img src="{{URL::to('/')}}/public/images/location.png">Ct Eureka, California(CA), 95501</h6>
								<div class="evnt_button">
									<ul>
										<li><a class="btn_yellow btn_css text-uppercase">Buy tickets</a></li>
										<li><a href="event_details.html" class="btn_green btn_css text-uppercase">view details</a></li>
									</ul>
								</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="event_section_inner">
						<div class="inner_event_img">
							<img src="{{URL::to('/')}}/public/images/img1.png">
								<div class="date_home">
									<h3>21 </h3>
									<p>August</p>
								</div><div class="hert_like">
									<a href="#"><div class="plus heart"></div></a>
								</div>
						</div>
						<div class="inner_event_descrption">
							<h3>Wedding Ceremony</h3>
							<p>Lorem Ipsum is simply dummy text of the printing </p>
							<h6><img src="{{URL::to('/')}}/public/images/location.png">Ct Eureka, California(CA), 95501</h6>
								<div class="evnt_button">
									<ul>
										<li><a class="btn_yellow btn_css text-uppercase">Buy tickets</a></li>
										<li><a href="event_details.html" class="btn_green btn_css text-uppercase">view details</a></li>
									</ul>
								</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="event_section_inner">
						<div class="inner_event_img">
							<img src="{{URL::to('/')}}/public/images/img1.png">
								<div class="date_home">
									<h3>01</h3>
									<p>July</p>
								</div>
								<div class="hert_like">
									<a href="#"><div class="plus heart minus"></div></a>
								</div>
						</div>
						<div class="inner_event_descrption">
							<h3>Hessler Street Fair</h3>
							<p>Lorem Ipsum is simply dummy text of the printing </p>
							<h6><img src="{{URL::to('/')}}/public/images/location.png">Ct Eureka, California(CA), 95501</h6>
								<div class="evnt_button">
									<ul>
										<li><a class="btn_yellow btn_css text-uppercase">Buy tickets</a></li>
										<li><a href="event_details.html" class="btn_green btn_css text-uppercase">view details</a></li>
									</ul>
								</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="event_section_inner">
						<div class="inner_event_img">
							<img src="{{URL::to('/')}}/public/images/img1.png">
								<div class="date_home">
									<h3>24</h3>
									<p>March</p>
								</div>
								<div class="hert_like">
									<a href="#"><div class="plus heart"></div></a>
								</div>
						</div>
						<div class="inner_event_descrption">
							<h3>Festival Latinoamericano</h3>
							<p>Lorem Ipsum is simply dummy text of the printing </p>
							<h6><img src="{{URL::to('/')}}/public/images/location.png">Ct Eureka, California(CA), 95501</h6>
								<div class="evnt_button">
									<ul>
										<li><a class="btn_yellow btn_css text-uppercase">Buy tickets</a></li>
										<li><a href="event_details.html" class="btn_green btn_css text-uppercase">view details</a></li>
									</ul>
								</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="event_section_inner">
						<div class="inner_event_img">
							<img src="{{URL::to('/')}}/public/images/img1.png">
								<div class="date_home">
									<h3>24</h3>
									<p>April</p>
								</div>
								<div class="hert_like">
									<a href="#"><div class="plus heart minus"></div></a>
								</div>
						</div>
						<div class="inner_event_descrption">
							<h3>North Texas Irish Festival</h3>
							<p>Lorem Ipsum is simply dummy text of the printing </p>
							<h6><img src="{{URL::to('/')}}/public/images/location.png">Ct Eureka, California(CA), 95501</h6>
								<div class="evnt_button">
									<ul>
										<li><a class="btn_yellow btn_css text-uppercase">Buy tickets</a></li>
										<li><a href="event_details.html" class="btn_green btn_css text-uppercase">view details</a></li>
									</ul>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="view_more text-center">
					<button class="btn btn_more">VIEW MORE</button>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="how_wrks_sction event_section">
	<div class="inner_contnt_sction text-center">
		<h1>How It works</h1>
		<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form,  by injected humour</p>
	</div>
	<div class="container">
		<div class="row">
			<div class="wrk_section_outer">
				<div class="wrk_sctn_inner">
					<div class="wrk_section_outer text-center">
						<div class="how_wrk_img">
							<img src="images/create_account.png">
						</div>
						<div class="how_work_des">
							<h3>Create free account</h3>
							<p>Lorem Ipsum is simply dummy text of the printing </p>
						</div>
					</div>
				</div>
				<div class="wrk_sctn_inner">
					<div class="wrk_section_outer text-center">
						<div class="how_wrk_img">
							<img src="images/search_icon.png">
						</div>
						<div class="how_work_des">
							<h3>Search Events</h3>
							<p>Lorem Ipsum is simply dummy text of the printing </p>
						</div>
					</div>
				</div>
				<div class="wrk_sctn_inner">
					<div class="wrk_section_outer text-center">
						<div class="how_wrk_img">
							<img src="images/buy_tickets.png">
						</div>
						<div class="how_work_des">
							<h3>Buy tickets</h3>
							<p>Lorem Ipsum is simply dummy text of the printing </p>
						</div>
					</div>
				</div>
				<div class="wrk_sctn_inner">
					<div class="wrk_section_outer text-center">
						<div class="how_wrk_img">
							<img src="images/buy_tickets.png">
						</div>
						<div class="how_work_des">
							<h3>Find Friends</h3>
							<p>Find new and old friends to join and share with you the best live music experience.</p>
						</div>
					</div>
				</div>
				<div class="wrk_sctn_inner">
					<div class="wrk_section_outer text-center">
						<div class="how_wrk_img">
							<img src="images/post_moemories.png">
						</div>
						<div class="how_work_des">
							<h3>Post memories</h3>
							<p>Lorem Ipsum is simply dummy text of the printing </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="event_section">
	<div class="inner_contnt_sction text-center">
		<h1>Most Trending Event </h1>
		<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form,  by injected humour</p>
	</div>
	<div class="container">
		<div class="row">
			<div class="event_section_outer">
				<div class="col-md-4 col-sm-6">
					<div class="event_section_inner">
						<div class="inner_event_img">
							<img src="images/img1.png">
								<div class="date_home">
									<h3>21</h3>
									<p>February</p>
								</div>
								<div class="hert_like">
									<a href="#"><div class="plus heart"></div></a>
								</div>
						</div>
						<div class="inner_event_descrption">
							<h3>New York Renaissance Faire</h3>
							<p>Lorem Ipsum is simply dummy text of the printing </p>
							<h6><img src="images/location.png">Ct Eureka, California(CA), 95501</h6>
								<div class="evnt_button">
									<ul>
										<li><a class="btn_yellow btn_css text-uppercase">Buy tickets</a></li>
										<li><a href="event_details.html" class="btn_green btn_css text-uppercase">view details</a></li>
									</ul>
								</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="event_section_inner">
						<div class="inner_event_img">
							<img src="images/img1.png">
								<div class="date_home">
									<h3>07</h3>
									<p>March</p>
								</div><div class="hert_like">
									<a href="#"><div class="plus heart minus"></div></a>
								</div>
						</div>
						<div class="inner_event_descrption">
							<h3>Grand Cities Art Fest</h3>
							<p>Lorem Ipsum is simply dummy text of the printing </p>
							<h6><img src="images/location.png">Ct Eureka, California(CA), 95501</h6>
								<div class="evnt_button">
									<ul>
										<li><a class="btn_yellow btn_css text-uppercase">Buy tickets</a></li>
										<li><a href="event_details.html" class="btn_green btn_css text-uppercase">view details</a></li>
									</ul>
								</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="event_section_inner">
						<div class="inner_event_img">
							<img src="images/img1.png">
								<div class="date_home">
									<h3>28</h3>
									<p>May</p>
								</div>
								<div class="hert_like">
									<a href="#"><div class="plus heart"></div></a>
								</div>
						</div>
						<div class="inner_event_descrption">
							<h3>Wedding Ceremony</h3>
							<p>Lorem Ipsum is simply dummy text of the printing </p>
							<h6><img src="images/location.png">Ct Eureka, California(CA), 95501</h6>
								<div class="evnt_button">
									<ul>
										<li><a class="btn_yellow btn_css text-uppercase">Buy tickets</a></li>
										<li><a href="event_details.html" class="btn_green btn_css text-uppercase">view details</a></li>
									</ul>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="view_more text-center">
					<button class="btn btn_more">VIEW MORE</button>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="footer_section">
	<div class="container">
		<div class="col-md-12">
			<div class="col-md-4 col-sm-6">
				<div class="footer_inner">
					<h3>About Us</h3>
					<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="footer_inner">
					<h3>Address</h3>
					<ul>
						<li><i class="fa fa-map-marker" aria-hidden="true"></i> Suite # 505, Fifth Floor, Al-Qadir Heights, New Garden Town, Lahore.</li>
						<li><i class="fa fa-phone" aria-hidden="true"></i> Tel +92-42-38911784</li>
					</ul>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="footer_inner">
					<h3>Follow Us</h3>
					<ul class="social_links">
						<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<p class="copyright_text text-center">&#169; copyright 2017-2018 website.com</p>
</section>

<script src="{{URL::to('/')}}/public/js/jquery.min.js"></script>
<script src="{{URL::to('/')}}/public/js/bootstrap.min.js"></script>
<script>
$(".plus").click(function(){
 $(this).toggleClass("minus")  ; 
})
</script>
</body>
</html>
