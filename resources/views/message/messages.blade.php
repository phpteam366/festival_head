@extends('layouts.inner')
@section('content')
@php
 $userId = Auth::id();
 $receiver_id = "";
 $chat_id     = "";
 $user_Id = "0";
 $chatsId = "";
 $chatType = "S";
@endphp
<div class="main">
	<section class="message_page inner_height">
		<div class="container height100">
			<div class="white_box height100">
				<div class="message_listbox mostly-customized-scrollbar">
					<ul>
					@forelse($chatUsers as $users)
					@php
					  $chatsId = $users->id;
					@endphp
					 @if($users->chat_type != "G")
							@php
								 if($userId == $users->sender_id){
								       $userName = $users->receiver_fname." ".$users->receiver_lname;
								       $user_Id   = $users->receiver_id;
								       $image    = $users->receiver_image;
								    }else{
								       $userName = $users->sender_fname." ".$users->sender_lname;
								       $user_Id   = $users->sender_id;
								       $image    = $users->sender_image;
								    }

							    if($receiver_id == ""){
							    	$receiver_id = $user_Id;
							    }

							    if($chat_id == ""){
							    	$chat_id = $users->id;
							    }

							    $chatType = "S";

							@endphp
						@else
						    @php
						    	$chatType = "G";
						    	$image = $users->event_image;
						    	$userName = $users->group_name;
						    	$chat_id = $users->id;
						    @endphp
						@endif
						<li class="chat_users" data-user-id="{{$user_Id}}" data-chat-id="{{$chatsId}}">
						@if($users->chat_type != "G")
							<a href="{{URL::to('/')}}/messages/{{$user_Id}}">
						@else
						    <a href="{{URL::to('/')}}/messages?event_id={{$users->event_id}}">
						@endif
							@if($image != "" && $chatType != "G")
								<img src="{{URL::to('/')}}/public/images/profile_image/{{$image}}" alt="" />
							@elseif($image != "" && $chatType == "G")
							    <img src="{{URL::to('/')}}/public/images/events_images/{{$image}}" alt="" />
							@else
								<img src="{{URL::to('/')}}/public/images/profile_image/dummy.png" alt="" />
							@endif
							<h5>{{$userName}}</h5>
							<h6>@if(!empty($users->message))  {{ TimeElapsed::getElapsedTime($users->created_at) }} @endif</h6>
							<p>{{$users->message}}</p>
						</a>
						</li>
						@empty
						 <li>No user to chat</li>
					@endforelse
					</ul>
				</div>
				<div class="messages_boxslist height100">


					<div class="chat_box height100 msg_chat_messages message_show_div">
					
					@forelse($messages as $message)
					  @if($userId == $message->sender_id)
					   	<div class="left_chatbox right_chatbox" data-message-id="{{$message->id}}">
						
							<div class="chatbox_outer">
									<p class="sender_name_right">{{$message->sender_fname}} {{$message->sender_lname}}</p>
								<div class="message_bubble">
									{{$message->message}}
								</div>
								<span class="message_time">{{ TimeElapsed::getElapsedTime($message->created_at) }}</span>
							</div>
						</div>
						@else
					 <div class="left_chatbox" data-message-id="{{$message->id}}">
					 		
							<div class="chatbox_outer">
								<p class="sender_name_right">{{$message->sender_fname}} {{$message->sender_lname}}</p>
								<div class="message_bubble">
									{{$message->message}}
								</div>
								<span class="message_time">{{ TimeElapsed::getElapsedTime($message->created_at) }}</span>
							</div>
						</div>
						@endif
					 @empty
					  <p class="no_message"> No Message sent/recieved</p>
					 @endforelse
					
					</div>
					</div>

					<div class="write_messagebox">
						<input type="hidden" name="receiver_id" class="receiver_id" value="{{$receiver_id}}"/>
			            <input type="hidden" name="offset" class="offset_messages" value="0"/>
			            <input type="hidden" name="chat_id" class="message_chat_id" value="{{$chat_id}}"/>
			            <input type="hidden" name="chat_type" class="message_chat_type" value="{{$chatType}}"/>
						<input type="text" value="" placeholder="Write a message..." name="message_input" class="message_input" />
						<a class="message_send_button" href="javascript:void(0)"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>
					</div>
				
			</div>
		</div>
	</section>
</div>
@stop
