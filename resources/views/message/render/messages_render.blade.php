@php
  $userId = Auth::id();
@endphp
@forelse($messages as $message)
    @if($userId == $message->sender_id)
      <div class="left_chatbox right_chatbox" data-message-id="{{$message->id}}">
      <div class="chatbox_outer">
        <div class="message_bubble">
          {{$message->message}}
        </div>
        <span class="message_time">{{ TimeElapsed::getElapsedTime($message->created_at) }}</span>
      </div>
    </div>
    @else
   <div class="left_chatbox" data-message-id="{{$message->id}}">
      <div class="chatbox_outer">
        <div class="message_bubble">
          {{$message->message}}
        </div>
        <span class="message_time">{{ TimeElapsed::getElapsedTime($message->created_at) }}</span>
      </div>
    </div>
    @endif
   @empty
            
@endforelse