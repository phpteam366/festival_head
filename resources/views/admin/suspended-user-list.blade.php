@extends('layouts.admin.comman')
@section('content')
<div class="content-wrapper">
	<section class="content">
		<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            <form action="{{URL::to('/')}}/admin/addUser" type="post">
              <h3 class="box-title">Suspended Users</h3>&nbsp;
              <!--<input type="submit" class="btn btn-primary pull-right" data-toggle="modal" data-target="#Add-user" value="Add User"> -->
            </form>
            </div>
            @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{Session::get('message')}}</p>
            @endif
            
              <table id="data-table" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Status</th>
                  <th>Date & Time</th>
                  <th>Actions</th>
                </tr>

                </thead>
                <tbody>
                  @forelse($users as $data)
                    <tr>
                      <td>{{ ucfirst($data->fname) }}</td>
                      <td><a href="mailto:{{ ($data->email) }}">{{ ($data->email) }}</a></td>
                      <td>
                        <input type="checkbox" data-id="{{ $data->id }}" class="subAdminStatus" data-toggle="toggle" data-on="Active
                        " data-off="Inactive" data-onstyle="primary" data-offstyle="danger" data-token="{{ csrf_token() }}" {{ ($data->status == 1)?"checked":"" }} />
                      </td>
                      <td>{{ $data->created_at }}</td>
                      <td>
                        <a href="{{ URL::to('admin/viewUser').'/'.$data->id }}" class="btn btn-success" title="View User"><i class="fa fa-eye"></i></a>
                        <!-- <a href="{{ URL::to('admin/editUser').'/'.$data->id }}" class="btn btn-success" title="Edit User"><i class="fa fa-pencil"></i></a> -->
                        <a href="{{ URL::to('admin/activeUser').'/'.$data->id }}" class="btn btn-success" title="Active User" onclick='return confirm("Do you really want to active this user")'><i class="fa fa-male"></i></a>
                        <!-- <a href="{{ URL::to('admin/deleteUser').'/'.$data->id }}" class="btn btn-danger" title="Delete User" onclick='return confirm("Do you really want to delete this user")'><i class="fa fa-trash-o"></i></a>  -->
                      </td>
                    </tr>   
                    @empty
                     <td>No Suspended Users!!!</td>
                  @endforelse
              </tbody>
            </table>
            {{$users->links()}}
          </div>
        </div>
      </div>
    </div>
	</section>
</div>
@stop