@extends('layouts.admin.comman')
@section('content')
<div class="content-wrapper">
<section class="content-header">
      
<div class="row">
  <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <div class="box">
            <div class="box-header">
              <h3 class="box-title">Change Password</h3>
             
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             
        <div class="col-md-12 col-sm-12 col-xs-12">
        @if (Session::has('message'))
          <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
      <form action="{{URL::to('/')}}/change-password" method="post" enctype="multipart/form-data" class="validate-me">
       @csrf
          <div class="form-group">
            <label>Old password</label>
            <input type="password" name="old_password" class="form-control no-space" placeholder="Enter old password" required="" autocomplete="off">
          </div>
          <div class="form-group">
            <label>New password</label>
            <input type="password" name="password" id="password" class="form-control no-space" placeholder="Enter new password" minlength="8" maxlength="32" id="newpass" required="" autocomplete="off">
          </div>
          <div class="form-group">
            <label>Confirm password</label>
            <input type="password" name="password_confirmation" class="form-control no-space" equalto="#password" minlength="8" maxlength="32" placeholder="Confirm password" required="" autocomplete="off">
          </div>
          <div class="form-group">
            <input type="submit" name="submit" value="Update" class="btn btn-primary pull-right">
          </div>
         </form>
        </div>
       </div>
      </div>
     </div>
    </div>
  </section>
</div>
  	</section>
 </div>
@endsection