@extends('layouts.admin.comman')
@section('content')
<div class="content-wrapper">
	<section class="content">
		<div class="box-header">
          @if($users->status == 1)
            <h3>Active User Details</h3>
            <a class="pull-right btn btn-success" href="{{ URL::to('admin/users') }}"> << Back</a>
          @else
            <h3>Suspended User Details</h3>
            <a class="pull-right btn btn-success" href="{{ URL::to('admin/suspendedUsers') }}"> << Back</a>
          @endif
          	
        </div>
		<div class="box-body">
		      	@if(Session::has('message'))
	            	<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{Session::get('message')}}</p>
            	@endif
              <table id="data-table" class="table table-bordered table-hover">
                <tr>
                  <th> First Name</th>
                  <td>{{ ucfirst($users->fname) }}</td>
              	</tr>
              	<tr>
                  <th> Last Name</th>
                  <td>{{ ucfirst($users->lname) }}</td>
              	</tr>
              	<tr>
                  <th> Nick Name</th>
                  <td>{{ ucfirst($userData->nick_name) }}</td>
              	</tr>
              	<tr>
                  <th> Age</th>
                  <td>{{ ($userData->age) }}</td>
              	</tr>
              	<tr>
                  <th> Gender</th>
                  <td>{{ ($userData->gender) }}</td>
              	</tr>
              	<tr>
                  <th> Relationship Status</th>
                  <td>@if($userData->marital_status == "Unmarried") Single @else {{$userData->marital_status}} @endif</td>
                </tr>
                <tr>
                  <th> City</th>
                  <td>{{$userData->city_name}}</td>
                </tr>
              	<tr>
                  	<th>Status</th>
                  	<td>
                        <input type="checkbox" data-id="{{ $users->id }}" class="subAdminStatus" data-toggle="toggle" data-on="Active
                        " data-off="Inactive" data-onstyle="primary" data-offstyle="danger" data-token="{{ csrf_token() }}" {{ ($users->status == 1)?"checked":"" }} />
                  	</td>
                  </tr>
              	<tr>
                  	<th>Date & Time</th>
                  	<td>{{ $users->created_at }}</td>
                </tr>

            </table>
          </div>
	</section>
</div>
@stop