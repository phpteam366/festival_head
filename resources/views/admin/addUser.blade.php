@extends('layouts.admin.comman')
@section('content')
<div class="content-wrapper">
	<section class="content">
		<div class="containaer">
    <h3>Add User</h3>
    @if(Session::has('message'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{Session::get('message')}}</p>
    @endif
    <div class="col-md-8 col-sm-12 col-xs-12">
      <form action="{{URL::to('admin/saveUser')}}" method="post" enctype="multipart/form-data">
      @csrf
      <div class="form-group">
        	<label>First Name</label>
        	<input type="text" name="f_name" class="form-control" placeholder="Enter First Name" required />
    	</div>
    	<div class="form-group">
        	<label>Last Name</label>
        	<input type="text" name="l_name" class="form-control" placeholder="Enter Last Name" required />
    	</div>
    	<div class="form-group">
        	<label>Nick Name</label>
        	<input type="text" name="nick_name" class="form-control" placeholder="Enter Nick Name" required />
    	</div>
    	<div class="form-group">
        	<label>Email</label>
        	<input type="Email" name="email" class="form-control" placeholder="Enter email" required />
    	</div>
    	<div class="form-group">
        	<label>Password</label>
        	<input type="Password" name="password" class="form-control" placeholder="Enter password" required />
          @if ($errors->has('password'))
              <div class="error">{{ $errors->first('password') }}</div>
          @endif
    	</div>
    	<div class="form-group">
        	<label>Confirm Password</label>
        	<input type="Password" name="password_confirmation" class="form-control" placeholder="Confirm  password" required />
    	</div>
    	<div class="form-group">
  	  	<label>Image</label>
  	  	<input type='file' name="image" id="imageUpload" accept=".png, .jpg, .jpeg" />
    	</div>
    	<div class="form-group">
        	<label>Age</label>
        	<input type="text" name="age" class="form-control" placeholder="Enter Age" required />
    	</div>
      <div class="form-group">
          <label>Marital Status</label>
  		<select name="marital_status" class="form-control" required>
  			<option>Select Marital Status</option>
  			<option value="Married">Married</option>
  			<option value="Unmarried">Unmarried</option>
  			<option value="In relationship">In relationship</option>
  		</select>
      </div>
      <div class="form-group">
  		<label>Gender</label>
  		<select name="gender" class="form-control" required>
  			<option>Select Gender</option>
  			<option value="Male">Male</option>
  			<option value="Female">Female</option>
  		</select>
      </div>
      <div class="form-group">
  		<label>Artists/Band </label>
  		<select name="artists" class="form-control" required>
  			<option>Select Artists/Band </option>
  			<option value="1">A</option>
  			<option value="2">B</option>
  		</select>
      </div>

      <div class="form-group">
      <label>Artists/Band </label>
      <select name="genre" class="form-control" required>
        <option>Select Genre </option>
        <option value="1">A</option>
        <option value="2">B</option>
      </select>
      </div>

    	<div class="form-group">
  		<label>State</label>
  		<select name="state" class="form-control" required>
  			<option>Select State</option>
  			<option value="1">1</option>
  			<option value="2">2</option>
  		</select>
      </div>

    	<div class="form-group">
        	<label>Status</label>
        	<input type="radio" name="status" value="1" checked />&nbsp;Enable&ensp;
        	<input type="radio" name="status" value="0" />&nbsp;Disable
  	</div>
      <div class="form-group">
        	<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
      </div>
      </form>
    </div>
</div>
	</section>
</div>
@stop