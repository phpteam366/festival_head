@extends('layouts.admin.comman')
@section('content')
<div class="content-wrapper">
	<section class="content">
		<div class="row">
        <div class="col-xs-12">
          @if(Session::has('error'))
               <div class="alert alert-danger">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ Session::get('error') }}
          </div>
        @endif
        @if(Session::has('success'))
          <div class="alert alert-success">
             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
               {{ Session::get('success') }}
          </div>
        @endif
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Update User</h3>
              <a class="pull-right btn btn-success" href="{{ URL::to('admin/users') }}"> << Back</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             
              <div class="col-md-8 col-sm-12 col-xs-12">
              <form action="{{ URL::to('/')}}/admin/updateUser" class="edit_user" method="post">
               @csrf
                  <div class="form-group">
                    <label> First Name</label>
                    <input type="hidden" name="id" value="{{ $user->id }}">
                    <input type="text" name="f_name" class="form-control" required placeholder="Enter First Name" value="{{$user->fname}}" />
                  </div>

                  <div class="form-group">
                    <label> Last Name</label>
                    <input type="text" name="l_name" class="form-control" required placeholder="Enter Last Name" value="{{$user->lname}}" />
                  </div>

                  <div class="form-group">
                    <label>Nick Name</label>
                    <input type="text" name="nick_name" class="form-control" value="{{$userData->nick_name}}">
                  </div>

                  <div class="form-group">
                    <label>Image</label>
                    <input type='file' name="image" id="imageUpload" accept=".png, .jpg, .jpeg" />
                  </div>
                  <div class="form-group">
                      <label>Age</label>
                      <input type="text" name="age" class="form-control" value="{{$userData->age}}" placeholder="Enter Age" required />
                  </div>
                  <div class="form-group">
                    <label>Marital Status</label>
                    <select name="marital_status" class="form-control" required>
                      <option>Select Marital Status</option>
                      <option value="Married" @if($userData->marital_status == 'Married') selected @endif >Married</option>
                      <option value="Unmarried" @if($userData->marital_status == 'Unmarried') selected @endif >Unmarried</option>
                      <option value="In relationship" @if($userData->marital_status == 'In relationship') selected @endif >In relationship</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Gender</label>
                    <select name="gender" class="form-control" required>
                      <option>Select Gender</option>
                      <option value="Male" @if($userData->gender == 'Male') selected @endif >Male</option>
                      <option value="Female" @if($userData->gender == 'Female') selected @endif >Female</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Artists/Band </label>
                    <select name="artists" class="form-control" required>
                      <option>Select Artists/Band </option>
                      <option value="1" @if($userData->artists_id == '1') selected @endif >A</option>
                      <option value="2" @if($userData->artists_id == '2') selected @endif >B</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label>Music/Genre </label>
                    <select name="genre" class="form-control" required>
                      <option>Select Genre </option>
                      <option value="1" @if($userData->genre_id == '1') selected @endif >A</option>
                      <option value="2" @if($userData->genre_id == '2') selected @endif >B</option>
                    </select>
                  </div>

                  <div class="form-group">
                  <label>State</label>
                  <select name="state" class="form-control" required>
                    <option>Select State</option>
                    <option value="1" @if($userData->state_id == '1') selected @endif >1</option>
                    <option value="2" @if($userData->state_id == '2') selected @endif >2</option>
                  </select>
                  </div>
                  
                  <div class="form-group">
                    <label>Status</label>&ensp;
                    <input type="radio" name="status" value="1" {{($user->status==1)?"checked":""}}>&nbsp;Active &ensp; 
                    <input type="radio" name="status" value="0" {{($user->status==0)?"checked":""}}>&nbsp;Inactive   
                  </div>

                  <div class="form-group">
                    <input type="submit" name="submit" value="Update" class="btn btn-primary pull-right"/>
                  </div>
              </form>
              </div>
          </div>
        </div>
      </div>
    </div>
	</section>
</div>
@stop