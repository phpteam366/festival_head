<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>FestivalHead | Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{URL::to('/')}}/public/admin/css/bootstrap.min.css">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{URL::to('/')}}/public/admin/css/font-awesome.min.css">
 
  <!-- Theme style -->
  <link rel="stylesheet" href="{{URL::to('/')}}/public/admin/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{URL::to('/')}}/public/admin/css/_all-skins.min.css">
  
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{URL::to('/')}}/public/admin/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <link rel="stylesheet" href="{{URL::to('/')}}/public/admin/plugins/timepicker/bootstrap-timepicker.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css">
  <!-- Daterange picker -->
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{URL::to('/')}}/public/admin/css/bootstrap3-wysihtml5.min.css">
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"></link>
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="{{URL::to('/')}}/public/css/custom.css">
   <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css"/>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<script>
 var base_url = "{{URL::to('/')}}";
</script>
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Festival Head</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
         
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{URL::to('/')}}/public/images/profile_image/{{Auth::user()->image}}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{Auth::user()->fname.' '.Auth::user()->lname}}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-bottom">
                  <a href="{{URL::to('/')}}/admin/edit-profile" class="btn btn-default btn-flat">Edit Profile</a>
                </div>
                <div class="pull-bottom">
                  <a href="{{URL::to('/')}}/admin/change-password" class="btn btn-default btn-flat">Change Password</a>
                </div>
                <div class="pull-bottom">
                  <a href="{{URL::to('/')}}/admin/logout" class="btn btn-default btn-flat">Log out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
         
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- <div class="user-panel">
        <div class="pull-left image">
          <img src="{{URL::to('/')}}/public/images/profile_image/{{Auth::user()->image}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->fname.' '.Auth::user()->lname}}</p>
        </div>
      </div> -->
     
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
     
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active">
          <a href="{{URL::to('/')}}/admin/dashboard">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <!-- <li>
          <a href="{{URL::to('/')}}/admin/users">
            <i class="fa fa-user"></i> <span>Users</span>
          </a>
        </li> -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Users</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{URL::to('/')}}/admin/users"><i class="fa fa-circle-o"></i>Active Users</a></li>
            <li class="active"><a href="{{URL::to('/')}}/admin/suspendedUsers"><i class="fa fa-circle-o"></i>Suspended Users</a></li>
            <!-- <li class=><a href="{{URL::to('/')}}/admin/outdated-events"><i class="fa fa-circle-o"></i> Outdated Events</a></li> -->
          </ul>
        </li>
        <li>
          <a href="{{URL::to('/')}}/admin/genre">
            <i class="fa fa-user"></i> <span>Music Genre</span>
          </a>
        </li>
        <li>
          <a href="{{URL::to('/')}}/admin/upcoming-events">
            <i class="fa fa-calendar"></i> <span>Events</span>
          </a>
        </li>

        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-calendar"></i> <span>Events</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{URL::to('/')}}/admin/events"><i class="fa fa-circle-o"></i>Ongoing Events</a></li>
            <li class="active"><a href="{{URL::to('/')}}/admin/upcoming-events"><i class="fa fa-circle-o"></i>Upcoming Events</a></li>
            <li class=><a href="{{URL::to('/')}}/admin/outdated-events"><i class="fa fa-circle-o"></i> Outdated Events</a></li>
          </ul>
        </li> -->



        <li>
          <a href="{{URL::to('/')}}/admin/artist">
            <i class="fa fa-user"></i> <span>Artists</span>
          </a>
        </li>
      </ul>
      
    </section>
    <!-- /.sidebar -->
  </aside>
  @yield('content')
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
     
    </div>
    <strong>Copyright &copy; {{date("Y")}} <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
 
  
</div>

<script src="{{URL::to('/')}}/public/admin/js/jquery.min.js"></script>
<script src="{{URL::to('/')}}/public/admin/js/bootstrap.min.js"></script>
<script src="{{URL::to('/')}}/public/js/jquery.validate.js"></script>
<script src="{{URL::to('/')}}/public/admin/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>


<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.min.js"></script>  -->


<script type="text/javascript" src="{{URL::to('/')}}/public/admin/js/moment.min.js"></script>
<script src="{{URL::to('/')}}/public/admin/plugins/timepicker/bootstrap-timepicker.min.js"></script> 
<script src="{{URL::to('/')}}/public/js/jquery.validate.js"></script>
<script src="{{URL::to('/')}}/public/admin/js/adminlte.min.js"></script>
<script src="{{URL::to('/')}}/public/admin/js/bootstrap-confirmation.min.js"></script>
<!-- Datatables -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>


<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
 
@php
 $googleKey = Config::get("constants.GOOGLE_AUTOCOMPLETE_KEY");
@endphp
<script src="https://maps.googleapis.com/maps/api/js?key={{$googleKey}}&v=3.exp&sensor=false&libraries=places"></script>

<script src="{{URL::to('/')}}/public/js/custom-admin.js"></script>
<script src="{{URL::to('/')}}/public/js/custom.js"></script>
</body>
</html>
