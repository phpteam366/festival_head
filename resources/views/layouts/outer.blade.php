<!DOCTYPE html>
<html lang="en">
<head>
  <title>Festival Head</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{URL::to('/')}}/public/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{URL::to('/')}}/public/css/style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{URL::to('/')}}/public/css/custom.css">
</head>
<body>
@php
 $routeName = \Request::route()->getName();
@endphp
<section class="haeder_main">
	<div class="container">
		<nav class="navbar navbar-inverse navbr_home">
		  <div class="container-fluid">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>                        
			  </button>
			  <a class="navbar-brand" href="{{URL::to('/')}}"><img src="{{URL::to('/')}}/public/images/logo.png"></a>
			</div>

			<div class="collapse navbar-collapse" id="myNavbar">
				<!-- @php
					$locationName=""; $locationLat="";$locationLong ="";
					if(isset($locations) && !empty($locations)){
					  $locationName = $locations['location'];
					  $locationLat = $locations['latitude'];
					  $locationLong = $locations['longitude'];
					}
				@endphp -->
				@php
				$eventName = "";
				if(isset($_GET['event_name'])){
					$eventName = $_GET['event_name'];
				}
				@endphp
				<div class="secrh_filed pull-left">
				@if($routeName =="home" || $routeName =="home_filter")
				<!-- <form name="search_with_location" class="search_with_location" action="{{URL::to('/')}}/events/filter">
					<input type="text" class="form-control" id="event_location" placeholder="Search Location" name="location" value="{{$locationName}}">
					<input type="hidden" id="city2"   name="" />
					<input type="hidden" name="event_latitude" id="cityLat" value="{{$locationLat}}" />
					<input type="hidden" name="event_longitude" id="cityLng"  value="{{$locationLong}}"/>
				</form> -->

				<form name="search_with_location" class="search_with_location" action="{{URL::to('/')}}/events/filter">
					<input type="text" class="form-control" id="event_name12" placeholder="Search Event" name="event_name" value="{{$eventName}}" autocomplete="off">
				</form>
				@endif
				</div>
				<div class="btn_othr_prfol pull-right">
					<ul class="nav navbar-nav">
						<li><a href="{{URL::to('/')}}/login" class="btn btn_blush block_btn">Login</a></li>
						<li><a href="{{URL::to('/')}}/sign-up" class="btn block_btn">Sign Up</a></li>
					</ul>
				</div>
			</div>
		  </div>
		</nav>
	</div>
</section>

@yield('content')

@include('elements.footer')

<script type="text/javascript">
	var baseUrl = '{{URL::to('/')}}';

</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="{{URL::to('/')}}/public/js/jquery.validate.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="{{URL::to('/')}}/public/js/custom.js"></script>
<script>
$(document).on('click','.plus',function(){
	$(this).toggleClass("minus")  ; 
});
</script>
@stack('custom-scripts')

@php
 $googleKey = Config::get("constants.GOOGLE_AUTOCOMPLETE_KEY");
@endphp
<script src="https://maps.googleapis.com/maps/api/js?key={{$googleKey}}&v=3.exp&sensor=false&libraries=places&callback=initialize" async defer></script>
<script>
function initialize() {
 if ($(".map_section").length > 0) {
	   initMap();
	   autoComplete();
	}
}
</script>
<script src="{{URL::to('/')}}/public/js/custom.js"></script>
<script src="{{URL::to('/')}}/public/js/user_custom.js"></script>
<script src="{{URL::to('/')}}/public/js/map.js"></script>
<script src="{{URL::to('/')}}/public/js/messages.js"></script>
</body>
</html>
