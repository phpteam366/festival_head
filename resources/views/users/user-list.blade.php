@extends('layouts.admin.comman')
@section('content')
<div class="content-wrapper">
	<section class="content">
		<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            <form action="{{URL::to('/')}}/user/add" type="post">
              <h3 class="box-title">Users</h3>&nbsp;
              <i class="fa fa-plus"></i><input type="submit" class="btn btn-primary " data-toggle="modal" data-target="#Add-user" value="Add User">
            </form>
            </div>
            
            @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{Session::get('message')}}</p>
            @endif
            <!-- /.box-header -->
            <div class="box-body">
                @if(Session::has('error'))
                <div class="alert alert-danger">
                 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                   {{ Session::get('error') }}
                </div>
              @endif
              	@if(Session::has('success'))
                <div class="alert alert-success">
                   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                     {{ Session::get('success') }}
                </div>
            	@endif
              <table id="data-table" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Status</th>
                  <th>Date added</th>
                  <th>Actions</th>
                </tr>

                </thead>
                <tbody>
                  @forelse($users as $data)
                    <tr>
                      <td>{{ ucfirst($data->fname) }}</td>
                      <td><a href="mailto:{{ ($data->email) }}">{{ ($data->email) }}</a></td>
                      <td>
                        <input type="checkbox" data-id="{{ $data->id }}" class="subAdminStatus" data-toggle="toggle" data-on="Active
                        " data-off="Inactive" data-onstyle="primary" data-offstyle="danger" data-token="{{ csrf_token() }}" {{ ($data->status == 1)?"checked":"" }} />
                      </td>
                      <td>{{ $data->created_at }}</td>
                      <td>
                        <a href="{{ URL::to('/editUser').'/'.$data->id }}" class="btn btn-success" title="Edit admin"><i class="fa fa-pencil"></i>jkjk</a>
                        
                         <a href="{{ URL::to('/deleteUser').'/'.$data->id }}" class="btn btn-danger" title="Delete admin" onclick='return confirm("Do you really want to delete this user")'><i class="fa fa-trash-o"></i></a> 
                      </td>
                    </tr>   
                    @empty
                     <td>No Users!!!</td>
                  @endforelse
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
	</section>
</div>
@stop