@extends('layouts.outer')
@section('content');
<section class="inner_linkpages">
	<div class="container">
		<div class="col-md-10 col-md-offset-1">
			<div class="ryt_innerlink creteprofile_inner">
			<form action="{{URL::to('/')}}/create-profile" class="validate-me" method="post" enctype="multipart/form-data">
			@csrf
				<h3 class="text-center">Create Profile</h3>
				<div class="upload_img">
					 <div class="avatar-upload">
						<div class="avatar-preview">
							<div id="imagePreview" style="background-image: url('{{URL::to('/')}}/public/images/uplaod_transparnt.png');">
							</div>
						</div>
						<div class="avatar-edit">
							<input type='file' name="image" id="imageUpload" accept=".png, .jpg, .jpeg" onchange="validateFileType()" />
							<label for="imageUpload">Upload</label>
						</div>
					</div>
				</div>
				@if (Session::has('message'))
				   <div class="alert alert-info">{{ Session::get('message') }}</div>
				@endif																																			
				<div class="row">
					<div class="form-group col-md-6 col-sm-6">
						<label>First Name</label>
						<input type="text" name="f_name" class="form-control" placeholder="Enter First Name" required>
					</div>
					<div class="form-group col-md-6 col-sm-6">
						<label>Last Name</label>
						<input type="text" name="l_name" class="form-control" placeholder="Enter Last Name" required>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6 col-sm-6">
						<label>Nick Name</label>
						<input type="text" name="nick_name" class="form-control" placeholder="Enter Nick Name" required>
					</div>
					<div class="form-group col-md-6 col-sm-6">
						<label>Marital Status</label>
						<select name="marital_status" class="form-control" required>
							<option>Select Marital Status</option>
							<option value="Married">Married</option>
							<option value="Unmarried">Single</option>
							<option value="In relationship">In relationship</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6 col-sm-6">
						<label>Age</label>
						<input type="text" id="age" name="age" class="form-control input-digits" placeholder="Enter Your Age" required>
					</div>
					<div class="form-group col-md-6 col-sm-6">
						<label>Gender</label>
						<select name="gender" class="form-control" required>
							<option>Select Gender</option>
							<option value="Male">Male</option>
							<option value="Female">Female</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6 col-sm-6">
						<label>Music Genre</label>
						<select name="genre" class="form-control" required>
							<option>Select Music Genre</option>
							@forelse($genres as $genre)
							<option value="{{$genre->id}}">{{$genre->name}}</option>
							@empty
							@endforelse
						</select>
					</div>
					<div class="form-group col-md-6 col-sm-6">
						<label>Artists/Band </label>
						<select name="artists" class="form-control" required>
							<option>Select Artists/Band </option>
							@forelse($artists as $artist)
							<option value="{{$artist->id}}">{{$artist->name}}</option>
							@empty
							@endforelse
						</select>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6">
						<label>State</label>
						<select name="state" class="form-control state_seletced" required>
							<option value="">Select State</option>
							@forelse($states as $state)
							<option value="{{$state->id}}">{{$state->name}}</option>
							@empty
							@endforelse
						</select>
					</div>

					<div class="form-group col-md-6">
						<label>City</label>
						<select name="city" class="form-control city_list" required>
							<option value="">Select City</option>
						</select>
					</div>



				</div>
				<div class="innerryt_linkbtn text-center">
					<input type="submit" class="btn btn_more" value="SUBMIT" >
				</div>
				</form>
			</div>
		</div>
	</div>
</section>

@stop

