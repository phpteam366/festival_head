@extends('layouts/outer')
@section('content')
<section class="inner_linkpages crete_accunt">
	<div class="container">
		<div class="col-md-10 col-md-offset-1">
			<div class="inner_linkouter">
				<div class="col-md-5 paddg_0_left col-sm-5">
					<div class="left_innerlink">
						<div class="left_innlinkcontnt">
							<h2>Welcome Back!</h2>
							<p>To keep connected with us please login with your personal info </p>
							<a class="left_innerbtn" href="{{URL::to('/')}}/login">Login</a>
						</div>
					</div>
				</div>
				<div class="col-md-7 col-sm-7">
						<div class="ryt_innerlink">
							<div class="inner_topsection">
								<h2 class="text-center">Create Account</h2>
								<ul class="text-center">
									<li><a href="{{URL::to('/')}}/login/facebook" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li><a href="{{URL::to('/')}}/login/google" class="google"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
									<li><a href="{{URL::to('/')}}/login/linkedin" class="linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
								</ul>
								<p class="text-center">or use your email for registration</p>
							</div>
							<form action="{{URL::to('/')}}/sign-up" method="post" class="validate-me">
							@csrf
							@if (Session::has('message'))
							   <div class="alert alert-info">{{ Session::get('message') }}</div>
							@endif
							<div class="form-group">
								<label>Email</label>
								<input type="email" name="email" class="form-control" placeholder="Enter Email" required autocomplete="off">
							</div>
							<div class="form-group">
								<label>Password</label>
								<input type="password" name="password" class="form-control no-space" placeholder="Enter Password" id="password" maxlength="32" minlength="8" required autocomplete="off" >
							</div>
							<div class="form-group">
								<label>Confirm Password</label>
								<input type="password" name="password_confirmation" class="form-control no-space" minlength="8" maxlength="32" placeholder="Enter Password" equalto="#password" required autocomplete="off" >
							</div>
							<div class="innerryt_linkbtn text-center">
								<input type="submit" name="signup" class="btn btn_more" value="SIGN UP" >
							</div>
							</form>
						</div>
				</div>
			</div>
		</div>
	</div>
</section>

@stop

