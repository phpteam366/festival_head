@extends('layouts.inner')
@section('content')
<section class="inner_linkpages">
	<div class="container">
		<div class="col-md-10 col-md-offset-1">
			<div class="ryt_innerlink creteprofile_inner">
			<form action="{{URL::to('/')}}/edit-profile" class="validate-me" method="post" enctype="multipart/form-data">
			@csrf
				<h3 class="text-center">Edit Profile</h3>
				<div class="upload_img edt_prof">
					 <div class="avatar-upload">
						<div class="avatar-preview">
							<div id="imagePreview" style="background-image: url('{{URL::to('/')}}/public/images/profile_image/{{$profile_image}}');">
							</div>
						</div>
						<div class="avatar-edit">
							<input type='file' name="image" id="imageUpload" accept=".png, .jpg, .jpeg" onchange="validateFileType()" />
							<label for="imageUpload">Change</label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6 col-sm-6">
						<label>First Name</label>
						<input type="text" name="f_name" class="form-control" value="{{$fname}}">
					</div>
					<div class="form-group col-md-6 col-sm-6">
						<label>Last Name</label>
						<input type="text" name="l_name" class="form-control" value="{{$lname}}">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6 col-sm-6">
						<label>Nick Name</label>
						<input type="text" name="nick_name" class="form-control" value="{{$nickname}}">
					</div>
					<div class="form-group col-md-6 col-sm-6">
						<label>Relationship Status</label>
						<select name="marital_status" class="form-control">
							<option>Select Relationship Status</option>
							<option value="Married" @if($marital_status == 'Married') selected @endif >Married</option>
							<option value="Unmarried" @if($marital_status == 'Unmarried') selected @endif >Single</option>
							<option value="In relationship" @if($marital_status == 'In relationship') selected @endif >In Relationship
						</select>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6 col-sm-6">
						<label>Age</label>
						<input type="text" name="age" class="form-control input-digits" value="{{$age}}">
					</div>
					<div class="form-group col-md-6 col-sm-6">
						<label>Gender</label>
						<select name="gender" class="form-control">
							<option>Select Gender</option>
							<option @if($gender == 'Male') selected @endif >Male</option>
							<option @if($gender == 'Female') selected @endif >Female</option>
							</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6 col-sm-6">
						<label>Music Genre</label>
						<select name="genre" class="form-control" required>
							<option>Select Music Genre</option>
							@forelse($genres as $genre)
							<option value="{{$genre->id}}" @if($genreId == $genre->id) selected @endif >{{$genre->name}}</option>
							@empty
							@endforelse
						</select>
					</div>
					<div class="form-group col-md-6 col-sm-6">
						<label>Artists/Band </label>
						<select name="artists" class="form-control" required>
							<option>Select Artists/Band </option>
							@forelse($artists as $artist)
							<option value="{{$artist->id}}" @if($artistId == $artist->id) selected @endif >{{$artist->name}}</option>
							@empty
							@endforelse
						</select>
					</div>
				</div>
				<!-- <div class="row">
					<div class="form-group col-md-6 col-sm-6">
						<label>Music Genre</label>
						<select name="genre" class="form-control">
							<option>Select Music Genre</option>
							<option selected>1</option>
							<option>2</option>
						</select>
					</div>
					<div class="form-group col-md-6 col-sm-6">
						<label>Artists/Band </label>
						<select name="artists" class="form-control">
							<option>Select Artists/Band </option>
							<option selected>1</option>
							<option>B</option>
						</select>
					</div>
				</div> -->
				<div class="row">
					<div class="form-group col-md-6">
						<label>State</label>
						<select name="state" class="form-control state_seletced">
							<option>Select State</option>
							@forelse($states as $state)
							<option value="{{$state->id}}" @if($stateId == $state->id) selected @endif >{{$state->name}}</option>
							@empty
							@endforelse
						</select>
					</div>

					<div class="form-group col-md-6">
						<label>City</label>
						<select name="city" class="form-control city_list" required>
							<option value="">Select City </option>
							@forelse($cities as $city)
							 	<option value="{{$city->id}}" @if($cityId == $city->id) selected @endif>{{$city->name}}</option>
							 @empty
							@endforelse
						</select>
					</div>
				</div>
				<div class="innerryt_linkbtn text-center">
					<button class="btn btn_more" type="submit">Update</button>
				</div>
				</form>
			</div>
		</div>
	</div>
</section>
@stop