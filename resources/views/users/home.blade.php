@php
if(Auth::check() && Auth::user()->user_type == 'U')
	$layout = 'layouts.inner';
else
	$layout = 'layouts.outer';
@endphp
@extends($layout)
@section('content')
<section class="filtr_section inner_filtr_pddg">
	<div class="container">
		<div class="col-md-12">
			<div class="filter_section_inner">
			<form action="{{URL::to('/')}}/events/filter" >
				<ul>
					
					<li>
						<div class="filed_sction">
							<select  class="select_event form-control" name="event">
								<option value="">Select Event</option>
								@forelse($eventList as $event)
								  <option @if(isset($_GET['event'])) @if($_GET['event'] == $event->id) selected @endif @endif value="{{$event->id}}">{{$event->name}}</option>
								 @empty
								@endforelse
							</select>
						</div>
					</li>
					<li>
						<div class="filed_sction">
							<select  class="select_genere form-control" name="genere">
								<option  value="">Select Genre</option>
								@forelse($generes as $genere)
								    <option @if(isset($_GET['genere'])) @if($_GET['genere'] == $genere->id) selected @endif @endif value="{{$genere->id}}">{{$genere->name}}</option>
								  @empty
								@endforelse
							</select>
						</div>
					</li>
					<li>
						<button type="button" class="btn_filter filter_events_home">Search</button>
					</li>
				</ul>
				</form>
			</div>
		</div>
	</div>
</section>


<section class="how_wrks_sction event_section">
	<div class="inner_contnt_sction text-center">
		<h1>Head Out to an Amazing Musical Night…With Amazing People</h1>
		<p>Find the hottest musical events in town, and people to go with you. Festival Head connects you to people heading to the same musical event as you. Enjoy together!</p>
	</div>
</section>


@php
$eventsLatLong = "[";
foreach($getEventsLatLong as $eventLatLong){
	$eventDateTime = $eventLatLong->date_time;
	$eventdDate    = date("d M Y",strtotime($eventDateTime));
	$eventdTime    = date("h:i A",strtotime($eventDateTime));

	$eventLatitude = $eventLatLong->latitude;
	$eventLongitude = $eventLatLong->longitude;

	$dateFaIcon = '<i class="fa fa-calendar" aria-hidden="true"></i>';
	$timeFaIcon  = '<i class="fa fa-clock-o" aria-hidden="true"></i>';

	$eventsLatLong .= "['<b>$dateFaIcon $eventdDate<br>\ $timeFaIcon $eventdTime</b>',   $eventLatitude, $eventLongitude]".",";
}
$eventsLatLong .= "]";

/*
[
 ['Philz Coffee<br>\
    801 S Hope St A, Los Angeles, CA 90017<br>\
   <a href="https://goo.gl/maps/L8ETMBt7cRA2">Get Directions</a>',   34.046438, -118.259653],
    ['Philz Coffee<br>\
    525 Santa Monica Blvd, Santa Monica, CA 90401<br>\
   <a href="https://goo.gl/maps/PY1abQhuW9C2">Get Directions</a>', 34.017951, -118.493567],
    ['Philz Coffee<br>\
    146 South Lake Avenue #106, At Shoppers Lane, Pasadena, CA 91101<br>\
    <a href="https://goo.gl/maps/eUmyNuMyYNN2">Get Directions</a>', 34.143073, -118.132040],
    ['Philz Coffee<br>\
    21016 Pacific Coast Hwy, Huntington Beach, CA 92648<br>\
    <a href="https://goo.gl/maps/Cp2TZoeGCXw">Get Directions</a>', 33.655199, -117.998640],
    ['Philz Coffee<br>\
    252 S Brand Blvd, Glendale, CA 91204<br>\
   <a href="https://goo.gl/maps/WDr2ef3ccVz">Get Directions</a>', 34.142823, -118.254569]
  ]
*/
@endphp
<section class="map_section">
	<div id="map"></div>
</section>

<section class="event_section">
	<div class="inner_contnt_sction text-center">
		<h1>Event LIST</h1>
		<!-- <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form,  by injected humour</p> -->
	</div>
	<div class="container">
		<div class="row">
			<div class="event_section_outer all-pending-events">
				@php
				if(count($events) >0){
				@endphp
				@forelse($events as $data)
				<div class="col-md-4 col-sm-6">
					<div class="event_section_inner">
						<div class="inner_event_img">
							<div class="inner_event_mainimg">
								<img src="{{URL::to('/')}}/public/images/events_images/{{$data->image}}">
							</div>
								<div class="date_home">
									<h3>{{date('d', strtotime($data->date_time))}} </h3>
									<p>{{date('M', strtotime($data->date_time))}}</p>
								</div>
								@if(Auth::user())
								<div class="hert_like">
									<input type="hidden" name="eventId" id="eventId" value="{{$data->id}}">
									<input type="hidden" name="userId" id="userId" value="{{Auth::id()}}">
									@if(Auth::check())
										@if($data->favourite == 0)
										<a href="javascript:void(0)" class="event_favourite" data-event-id="{{$data->id}}"><div class="plus heart"></div></a>
										@else
										<a href="javascript:void(0)" class="event_favourite" data-event-id="{{$data->id}}"><div class="plus heart minus"></div></a>
										@endif
									@endif
								</div>
								@endif
						</div>
						<div class="inner_event_descrption">
							<h3>
							@php
							if(strlen($data->name) > 25){
								$eventName = substr($data->name, 0, 25)." ...";
							}else{
								$eventName = $data->name;
							}
							@endphp
							{{$eventName}}
							</h3>
							<p class="event_desc">
								@php
								if(strlen($data->description) > 60){
									$eventDescription = substr($data->description, 0, 60)." ...";
								}else{
									$eventDescription = $data->description;
								}
								@endphp
								{{$eventDescription}}
							</p>
							@php
								if(strlen($data->location) > 30){
									$eventLocation = substr($data->location, 0, 30)." ...";
								}else{
									$eventLocation = $data->location;
								}
							@endphp
							<h6><img src="{{URL::to('/')}}/public/images/location.png">{{$eventLocation}}</h6>
								<div class="evnt_button">
									<ul>
										<li><a href="{{$data->ticket_url}}" target="__blank" class="btn_yellow btn_css text-uppercase">Buy tickets</a></li>
										<li><a href="{{URL::to('/')}}/event-details/{{$data->id}}" class="btn_green btn_css text-uppercase">view details</a></li>
									</ul>
								</div>
						</div>
					</div>
				</div>	
				@empty
				<div class="text-center">No Events!!!</div>
				@endforelse

				@php
				}else{
				@endphp
				<div class="text-center">No Events!!!</div>
				@php
				}
				@endphp
			</div>
			@php
				 $limit = Config::get('constants.PAGINATION_LIMIT');
				 $totalEventsShowing = count($events);
			@endphp
			@if($totalEventsShowing >= $limit)
			 @php
			 	if(!isset($type)){
			 @endphp
			<div class="row" style="clear:both">
				<div class="col-md-12">
					<div class="view_more text-center">
						<button class="btn btn_more load_more_events" @isset($page) data-page-no="{{$page+1}}"  @endisset>VIEW MORE</button>
					</div>
				</div>
			</div>
			  @php
			  }
			 @endphp
			@endif
		</div>
	</div>
</section>

<section class="how_wrks_sction event_section">
	<div class="inner_contnt_sction text-center">
		<h1>How It works</h1>
		<p>Just sign up and you’re halfway there. You can easily find events, buy tickets and meet people going to the same event. Once you’re done, you can share your memories here.</p>
	</div>
	<div class="container">
		<div class="row">
			<div class="wrk_section_outer">
				<div class="wrk_sctn_inner">
					<div class="wrk_section_outer text-center">
						<div class="how_wrk_img">
							<img src="{{URL::to('/')}}/public/images/create_account.png">
						</div>
						<div class="how_work_des">
							<h3>Create free account</h3>
							<p>Sign up with your email, Facebook or LinkedIn.</p>
						</div>
					</div>
				</div>
				<div class="wrk_sctn_inner">
					<div class="wrk_section_outer text-center">
						<div class="how_wrk_img">
							<img src="{{URL::to('/')}}/public/images/search_icon.png">
						</div>
						<div class="how_work_des">
							<h3>Search Events</h3>
							<p>Search for your favourite artist/band here and see upcoming events.</p>
						</div>
					</div>
				</div>
				<div class="wrk_sctn_inner">
					<div class="wrk_section_outer text-center">
						<div class="how_wrk_img">
							<img src="{{URL::to('/')}}/public/images/buy_tickets.png">
						</div>
						<div class="how_work_des">
							<h3>Buy tickets</h3>
							<p>Get tickets to your favourite artist/band here.</p>
						</div>
					</div>
				</div>
				<div class="wrk_sctn_inner">
					<div class="wrk_section_outer text-center">
						<div class="how_wrk_img">
							<img src="{{URL::to('/')}}/public/images/find_frends.png">
						</div>
						<div class="how_work_des">
							<h3>Find Friends</h3>
							<p> Find new and old friends to share the best live music experiences with.</p>
						</div>
					</div>
				</div>
				<div class="wrk_sctn_inner">
					<div class="wrk_section_outer text-center">
						<div class="how_wrk_img">
							<img src="{{URL::to('/')}}/public/images/post_moemories.png">
						</div>
						<div class="how_work_des">
							<h3>Post memories</h3>
							<p>Share pictures and videos with the Festival Head community.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@if(isset($trending_events))
<section class="event_section">
	<div class="inner_contnt_sction text-center">
		<h1>Most Trending Event </h1>
	</div>
	<div class="container">
		<div class="row">
			<div class="event_section_outer">
			<!---mostly liked-->

			@forelse($trending_events as $trending)
				<div class="col-md-4 col-sm-6">
					<div class="event_section_inner">
						<div class="inner_event_img">
							<div class="inner_event_mainimg">
								<img src="{{URl::to('/')}}/public/images/events_images/{{$trending->image}}">
							</div>
								<div class="date_home">
									<h3>{{date('d', strtotime($trending->date_time))}} </h3>
									<p>{{date('M', strtotime($trending->date_time))}}</p>
								</div>
								

								@if(Auth::user())
								<div class="hert_like">
									<input type="hidden" name="eventId" id="" value="{{$trending->id}}">
									<input type="hidden" name="userId" id="" value="{{Auth::id()}}">
									@if($trending->favourite == 0)
									<a href="javascript:void(0)" class="event_favourite" data-event-id="{{$trending->id}}"><div class="plus heart"></div></a>
									@else
									<a href="javascript:void(0)" class="event_favourite" data-event-id="{{$trending->id}}"><div class="plus heart minus"></div></a>
									@endif
								</div>
								@endif
						</div>
						<div class="inner_event_descrption">
							<h3>@php
							if(strlen($trending->name) > 25){
								$eventName = substr($trending->name, 0, 25)." ...";
							}else{
								$eventName = $trending->name;
							}
							@endphp
							{{$eventName}}</h3>
							<p>
								@php
								if(strlen($trending->description) > 60){
									$eventDescription = substr($trending->description, 0, 60)." ...";
								}else{
									$eventDescription = $trending->description;
								}
								@endphp
								{{$eventDescription}}
							</p>
							@php
								if(strlen($trending->location) > 30){
									$eventLocation = substr($trending->location, 0, 30)." ...";
								}else{
									$eventLocation = $trending->location;
								}
							@endphp
							<h6><img src="{{URL::to('/')}}/public/images/location.png">{{$eventLocation}}</h6>
								<div class="evnt_button">
									<ul>
										<li><a href="{{$trending->ticket_url}}" target="__blank" class="btn_yellow btn_css text-uppercase">Buy tickets</a></li>
										<li><a href="{{URL::to('/')}}/event-details/{{$trending->id}}" class="btn_green btn_css text-uppercase">view details</a></li>
									</ul>
								</div>
						</div>
					</div>
				</div>
				@empty
				 <div class="text-center">No Trending Events !!</div>
				@endforelse
				
			</div>
		</div>



		<div class="row hide">
			<div class="col-md-12">
				<div class="view_more text-center">
					<button class="btn btn_more">VIEW MORE</button>
				</div>
			</div>
		</div>
	</div>
</section>
@endif
@stop

@push('custom-scripts')
<style>
      #map {
        height: 559px;
      }
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
<script>
    function initMap() {
  var center = {lat: -20.9175738, lng: 142.70279559999994};
  var locations = {!! $eventsLatLong !!};
var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 4,
    center: center,
     minZoom: 4
  });
var infowindow =  new google.maps.InfoWindow({});
var marker, count;
for (count = 0; count < locations.length; count++) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[count][1], locations[count][2]),
      map: map
      /*title: locations[count][0]*/
    });
	google.maps.event.addListener(marker, 'click', (function (marker, count) {
      return function () {
        infowindow.setContent(locations[count][0]);
        infowindow.open(map, marker);
      }
    })(marker, count));
  }
}
    </script>
@endpush
