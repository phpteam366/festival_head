@extends('layouts.inner')
@section('content')
<section class="fav_sctn_iner notfctn_sction height_100">
	<div class="noti">
		<div class="container">
			<h1>Notifications</h1>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="bottom-revews_detl">
							<div class="revews_detl_inner">
								<ul>
									@forelse($notifications as $notification)
									<li>
										<div class="revw_detl_thumb">
											<div class="revw_thumb_img">
												<img src="{{URL::to('/')}}/public/images/profile_image/{{$notification->image}}">
											</div>
											<div class="ryt_info_detl">
												<div class="head_evnt">
													<h3><a href="{{URL::to('/')}}/other-user-profile/{{$notification->sender_id}}">{{$notification->fname.' '.$notification->lname}}</a> @if($notification->notification_for == 'L')like your post <a href="{{URL::to('/')}}/news-feed-details/{{$notification->feed_id}}">{{$notification->title}}.</a>
													@elseif($notification->notification_for == 'C')commented on your post <a href="{{URL::to('/')}}/news-feed-details/{{$notification->feed_id}}">{{$notification->title}}.</a>
													@elseif($notification->notification_for == 'F')follows you.
													@endif</h3>
													<span>{{ TimeElapsed::getElapsedTime($notification->created_at) }}</span>
												</div>
												@if($notification->comment != '')
												<p>{{$notification->comment}}</p>
												@endif
											</div>
										</div>
									</li>
									@empty
									<li>
										<div class="revw_detl_thumb">
											<div class="revw_thumb_img">
											</div>
											<div class="ryt_info_detl">
												<div class="head_evnt">
												No Notifications!!!
												</div>
											</div>
										</div>
									</li>
									@endforelse
								</ul>
							</div>
						</div>
				</div>
		</div>
		
	</div>
</section>
@stop