@extends('layouts/inner')
@section('content')
<style>
.btn.btn_more.submit_comment_news {
	line-height: 31px;
	max-width: 206px;
	font-size: 18px;
	margin-top: 20px;
}
</style>
<section class="myprofile_section">
	<div class="container">
		<div class="col-md-4">
			@if (Session::has('message'))
				  <div class="alert alert-info">{{ Session::get('message') }}</div>
		   @endif
			<div class="left_myprofile">
				<div class="prof_top_img">
					<div class="usr_prfle_imgs"><img src="{{URL::to('/')}}/public/images/profile_image/{{$profile_image}}"></div>
				</div>
				<div class="user_profile_info text-center">
					<div class="flowers_listng">
						<ul>
							<li>
								<h3>{{$followers}}</h3>
								<h5>Follower</h5>
							</li>
							<li>
								<h3>{{$followings}}</h3>
								<h5>Following</h5>
							</li>
						</ul>
						<div class="abs_cnter">
							<img src="{{URL::to('/')}}/public/images/profile_image/{{$profile_image}}">
						</div>
					</div>
					<ul class="text-center">
						<li><a href="{{URL::to('/')}}/edit-profile"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
						<li><a href="{{URL::to('/')}}/setting"><i class="fa fa-cog" aria-hidden="true"></i></a></li>
					</ul>
					<h3>{{$user_name}}</h3>
					<table class="table">
						<tr>
							<th>Gender</th>
							<td>{{$gender}}</td>
						</tr>
						<tr>
							<th>Age</th>
							<td>{{$age}} Years</td>
						</tr>
						<tr>
							<th>Music Genre</th>
							<td>{{$genre}}</td>
						</tr>
						<tr>
							<th>Artists/Band </th>
							<td>{{$artist}}</td>
						</tr>
						<tr>
							<th>Relationship Status</th>
							<td>@if($marital_status == "Unmarried") Single @else {{$marital_status}} @endif</td>
						</tr>
						<tr>
							<th>State</th>
							<td>{{$state}}</td>
						</tr>
						<tr>
							<th>City</th>
							<td>{{$city}}</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-8">
		@forelse($newsFeed as $news)
			<div class="bottom-revews_detl profile_post_listng revews_detl_inner">
				<ul>
									<li>
										<div class="revw_detl_thumb">
											<div class="revw_thumb_img">
												<img src="{{URL::to('/')}}/public/images/profile_image/{{$news->user_image}}">
											</div>
											<div class="ryt_info_detl">
												<div class="head_evnt">
													<h3><a href="{{URL::to('/')}}/news-feed-details/{{$news->feed_id}}">{{$news->title}}</a></h3>
													<span>{{ TimeElapsed::getElapsedTime($news->feeds_created) }}</span>
												</div>
												<p>{{$news->description}}</p>
											</div>
										</div>
									</li>
				</ul>
				<div class="post_img">
					
					@if($news->feed_image == '' && $news->feed_video != '')
					@php
					  $extnsion = explode('.',$news->feed_video);
					  if(isset($extnsion) && !empty($extnsion[1])){
					  	$ext = $extnsion[1];
					  }else{
					  	$ext = "mp4";
					  }
					@endphp
						<video class="video_news" controls>
					  <source src="{{URL::to('/')}}/public/images/news_feeds/{{$news->feed_video}}" type="video/{{$ext}}">
					Your browser does not support the video.
					</video>
					@elseif($news->feed_video == '' && $news->feed_image != '')
<div class="post_img_singls">
						<img src="{{URL::to('/')}}/public/images/news_feeds/{{$news->feed_image}}"></div>
					@endif

				</div>
				<div class="post_commt_profl cmnt_here">
					<h3><a href="" style="text-decoration:underline" class="feed_comment" data-toggle="modal" data-target="#myModal" data-news-id='{{$news->feed_id}}'>Comments</a> (<span class="total_count{{$news->feed_id}}">{{$totalComment[$news->feed_id]}}</span>)</h3>
					<ul>
						<li><a href="" class="like_news" data-news-id='{{$news->feed_id}}' data-user-id='{{$news->user_id}}'><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a><span id="total_like">@if (array_key_exists($news->feed_id, $likesArr))
									    {{$likesArr[$news->feed_id]}}
									@else
									    0
									@endif
									</span></li>
						<li><a href="#"><i class="fa fa-share-alt feed_share" data-news-id='{{$news->feed_id}}' data-toggle="modal" data-target="#myModals" aria-hidden="true"></i></a><span></span></li>
					</ul>
					<div class="cmmnt_area">
						<div class="comment_boxxx">
							<textarea class="form-control news_comment news_comment_{{$news->feed_id}}" value="" data-feed-id = "{{$news->feed_id}}" data-user-id='{{$news->user_id}}' placeholder="Write comment here..."></textarea>
							<button class="btn btn_more submit_comment_news" data-feed-id = "{{$news->feed_id}}" data-user-id='{{$news->user_id}}'>Post</button>
						</div>
					</div>
				</div>
			</div>
		@empty
				No News Feeds!!!
		@endforelse
		</div>
	</div>
</section>
<div class="review_popup modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center">Comments</h4>
        </div>
        <div class="modal-body commnt_popup">
			<div class="bottom_descption_evnt bottom-revews_detl">
				<div class="revews_detl_inner">
					<ul class="comment_section">
						
					</ul>
				</div>
			</div>
        </div>
        <div class="modal-footer">
          	<div class="view_more text-center">
				<!-- <button class="btn btn_more">VIEW ALL</button> -->
			</div>
        </div>
      </div>
    </div>
</div>
<div class="review_popup modal fade" id="myModals" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center">Share</h4>
        </div>
        <div class="modal-body">
         	<div class="bottom_descption_evnt bottom-revews_detl share_link">
				<div class="revews_detl_inner">
					<ul class="social_icon_list">
						<li>
							<a href="" class="facebook_share" target="_blank">
								<img src="{{URL::to('/')}}/public/images/facebook.png">
								<p>Facebook</p>
							</a>
						</li>
						<li>
							<a href="" class="google_share" target="_blank">
								<img src="{{URL::to('/')}}/public/images/google.png">
								<p>Google</p>
							</a>
						</li>
						<li>
							<a href="" class="linkedin_share" target="_blank">
								<img src="{{URL::to('/')}}/public/images/linkedin.png">
								<p>Linkedin</p>
							</a>
						</li>
					</ul>
				</div>
			</div>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
@stop
