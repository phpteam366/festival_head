@php
if(Auth::check() && Auth::user()->user_type == 'U')
	$layout = 'layouts.inner';
else
	$layout = 'layouts.outer';
@endphp
@extends($layout)
@section('content')
<section class="event_detail_section">
	<div class="container">
		<div class="row">
			<div class="event_detail_outer">
				<div class="col-md-8">
					<div class="evnt_detl_img">
						<div class="ent_detal_singlimg">
							<img src="{{URL::to('/')}}/public/images/event_detail_img.png">
						</div>
						<div class="like_count">
							<h4>267</h4>
							<h6><i class="fa fa-thumbs-up" aria-hidden="true"></i></h6>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="event_detl_info">
						<h2>Chirstmas Party Event</h2>
						<div class="review_top">
							<ul>
								<li><i class="fa fa-star" aria-hidden="true"></i></li>
								<li><i class="fa fa-star" aria-hidden="true"></i></li>
								<li><i class="fa fa-star" aria-hidden="true"></i></li>
								<li><i class="fa fa-star" aria-hidden="true"></i></li>
								<li><i class="fa fa-star" aria-hidden="true"></i></li>
							</ul>
							<span>310<button href="" class="btn btn-link btn_revw" data-toggle="modal" data-target="#myModal" style="text-decoration:underline">Reviews</button></span>
						</div>
						<div class="addres_info_evnt">
							<ul>
								<li>
									<h4>Location</h4>
									<h5>Ct Eureka, California(CA), 95501</h5>
								</li>
								<li>
									<h4>Date and Time</h4>
									<h5>07 March 2019, 3:00 pm </h5>
								</li>
								<li>
									<h4>Genre</h4>
									<h5>Rock & Pop Music, Classic Rock</h5>
								</li>
							</ul>
							<div class="socl_btn">
								<ul>
									<li><button class="btn_top btn_red"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>like</button></li>
									<li><button class="btn_top btn_blue"><i class="fa fa-share-alt" aria-hidden="true"></i>Share</button></li>
								</ul>
							</div>
							<button class="btn btn_tickt">buy tickets</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="bottom_sction_detal_evnt">
	<div class="container">
		<div class="row">
			<div class="bottom_descption_evnt">
				<div class="col-md-8">
					<div class="descptn_sctn">
						<h3>Description</h3>
						<p>Here at Eventa, we’re all about Christmas. It’s without doubt the best time of the year and
						the perfect time to party and let your hair down with friends and colleagues. If you’ve been
						tasked with the role organising the festive celebration this year, then don’t worry as we’ve
						got you covered with our fantastic range Christmas party packages in over 50 destinations
						across the UK! With 10+ years’ worth of experience, we’re confident that you’ll find a
						Christmas party on our website that you and your group will love!</p>
					</div>
					<div class="commnt_sctn">
						<h3>Comments (213)</h3>
						<div class="cmnt_here">
							<textarea class="form-control" placeholder="Write comment here..."></textarea>
						</div>
						<div class="bottom-revews_detl">
							<div class="revews_detl_inner">
								<ul>
									<li>
										<div class="revw_detl_thumb">
											<div class="revw_thumb_img">
												<img src="{{URL::to('/')}}/public/images/review1.png">
											</div>
											<div class="ryt_info_detl">
												<div class="head_evnt">
													<h3>Mark Smith</h3>
													<span>1 day ago</span>
												</div>
												<p>Here at Eventa, we’re all about Christmas. It’s without doubt the best time of the
												year and the perfect time to party and let your hair down with friends.</p>
											</div>
										</div>
									</li>
									<li>
										<div class="revw_detl_thumb">
											<div class="revw_thumb_img">
												<img src="{{URL::to('/')}}/public/images/review2.png">
											</div>
											<div class="ryt_info_detl">
												<div class="head_evnt">
													<h3>Lucy Kim</h3>
													<span>2 days ago</span>
												</div>
												<p>Here at Eventa, we’re all about Christmas. It’s without doubt the best time of the
												year and the perfect time to party and let your hair down with friends.</p>
											</div>
										</div>
									</li>
									<li>
										<div class="revw_detl_thumb">
											<div class="revw_thumb_img">
												<img src="{{URL::to('/')}}/public/images/review3.png">
											</div>
											<div class="ryt_info_detl">
												<div class="head_evnt">
													<h3>Sam wik</h3>
													<span>5 days ago</span>
												</div>
												<p>Here at Eventa, we’re all about Christmas. It’s without doubt the best time of the
												year and the perfect time to party and let your hair down with friends.</p>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="view_more text-center">
						<button class="btn btn_more">VIEW MORE</button>
					</div>
				</div>
				<div class="col-md-4 padding_0r">
					<div class="artist_ryt">
						<h3>Artists Performing </h3>
						<ul>
							<li>
								<div class="artist-img">
									<div class="artst_imgs"><img src="{{URL::to('/')}}/public/images/artist1.png"></div>
									<p>Mike Shinoda</p>
								</div>
							</li>
							<li>
								<div class="artist-img">
									<div class="artst_imgs"><img src="{{URL::to('/')}}/public/images/artist2.png"></div>
									<p>Joe Hahn</p>
								</div>
							</li>
							<li>
								<div class="artist-img">
									<div class="artst_imgs"><img src="{{URL::to('/')}}/public/images/artist3.png"></div>
									<p>Chester Benning </p>
								</div>
							</li>
							<li>
								<div class="artist-img">
									<div class="artst_imgs"><img src="{{URL::to('/')}}/public/images/artist4.png"></div>
									<p>Rob Bourdon</p>
								</div>
							</li>
							<li>
								<div class="artist-img">
									<div class="artst_imgs"><img src="{{URL::to('/')}}/public/images/artist5.png"></div>
									<p>Dave Farrell</p>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="review_popup modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center">feedback</h4>
        </div>
        <div class="modal-body">
			<div class="bottom_descption_evnt bottom-revews_detl">
				<div class="revews_detl_inner">
					<ul>
						<li>
							<div class="revw_detl_thumb">
								<div class="revw_thumb_img">
									<img src="{{URL::to('/')}}/public/images/review1.png">
								</div>
								<div class="ryt_info_detl">
									<div class="head_evnt">
										<h3>Mark Smith</h3>
										<span>1 day ago</span>
									</div>
									<p>Here at Eventa, we’re all about Christmas. It’s without doubt the best time of the
									year and the perfect time to party and let your hair down with friends.</p>
								</div>
							</div>
						</li>
						<li>
							<div class="revw_detl_thumb">
								<div class="revw_thumb_img">
									<img src="{{URL::to('/')}}/public/images/review2.png">
								</div>
								<div class="ryt_info_detl">
									<div class="head_evnt">
										<h3>Lucy Kim</h3>
										<span>2 days ago</span>
									</div>
									<p>Here at Eventa, we’re all about Christmas. It’s without doubt the best time of the
									year and the perfect time to party and let your hair down with friends.</p>
								</div>
							</div>
						</li>
						<li>
							<div class="revw_detl_thumb">
								<div class="revw_thumb_img">
									<img src="{{URL::to('/')}}/public/images/review3.png">
								</div>
								<div class="ryt_info_detl">
									<div class="head_evnt">
										<h3>Sam wik</h3>
										<span>5 days ago</span>
									</div>
									<p>Here at Eventa, we’re all about Christmas. It’s without doubt the best time of the
									year and the perfect time to party and let your hair down with friends.</p>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
        </div>
        <div class="modal-footer">
          	<div class="view_more text-center">
				<button class="btn btn_more">VIEW ALL</button>
			</div>
        </div>
      </div>
    </div>
</div>

@stop