@extends('layouts.inner')
@section('content')
<section class="setting_section">
	<div class="container">
		<div class="row">
		<h3 class="setting-text"><strong>SETTINGS</strong></h3>
		<div class="col-md-3 col-sm-12">

			<div class="tab_section_left">

				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#notifictn">Notification</a></li>
					<li><a data-toggle="tab" href="#blck_membr">Block Members</a></li>
					<li><a data-toggle="tab" href="#chng_pswd">Change Password</a></li>
					<li><a data-toggle="tab" href="#abt_us">About Us</a></li>
				</ul>
			</div>
		</div>
		<div class="col-md-9 col-sm-12">
			<div class="tab_cntnt_ryt">
				<div class="tab-content">
					<div id="notifictn" class="tab-pane fade in active">
						<div class="inner_notifctn">
						  <h3>Notifications Settings</h3>
						  @if (Session::has('message'))
							  	<div class="alert alert-info">{{ Session::get('message') }}</div>
						   	@endif
							<div class="seetg_all_full">
								<div class="notfctn_countr">
									<div class="col-md-12">
										<div class="col-md-5 col-sm-5">
											<div class="radio_labl">
												<p>Someone followed you</p>
											</div>
										</div>
										<div class="col-md-7 col-sm-7 text-center">
											<div class="radio">
												<input id="radio-1" class="followed_you" value="1" name="followed_you" type="radio" @if($follow == '1') checked @endif>
												<label for="radio-1" class="radio-label">Yes</label>
											</div>
											<div class="radio">
												<input id="radio-2" class="followed_you" value="0" name="followed_you" type="radio" @if($follow == '0') checked @endif>
												<label  for="radio-2" class="radio-label">No</label>
											</div>
										</div>
									</div>
								</div>
								<div class="notfctn_countr">
									<div class="col-md-12">
										<div class="col-md-5 col-sm-5">
											<div class="radio_labl">
												<p>Comment your Post</p>
											</div>
										</div>
										<div class="col-md-7 col-sm-7 text-center">
											<div class="radio">
												<input id="radio-3" class="replied_comment" value="1" name="replied_comment" type="radio" @if($comment == '1') checked @endif>
												<label for="radio-3" class="radio-label">Yes</label>
											</div>
											<div class="radio">
												<input id="radio-4" class="replied_comment" value="0" name="replied_comment" type="radio" @if($comment == '0') checked @endif>
												<label  for="radio-4" class="radio-label">No</label>
											</div>
										</div>
									</div>
								</div>
								<div class="notfctn_countr">
									<div class="col-md-12">
										<div class="col-md-5 col-sm-5">
											<div class="radio_labl">
												<p>Like your Post</p>
											</div>
										</div>
										<div class="col-md-7 col-sm-7 text-center">
											<div class="radio">
												<input id="radio-5" class="like_post" value="1" name="like_post" type="radio" @if($like == '1') checked @endif>
												<label for="radio-5" class="radio-label">Yes</label>
											</div>
											<div class="radio">
												<input id="radio-6" class="like_post" value="0" name="like_post" type="radio" @if($like == '0') checked @endif>
												<label  for="radio-6" class="radio-label">No</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="blck_membr" class="tab-pane fade">
						<h3>Block Members</h3>
						<div class="block_sctn_inner">
							<ul>
							@forelse($blockedUser as $user)
								<li>
									<div class="blck_mbr_outer text-center">
										<div class="blck_mbr_img">
											<img src="{{URL::to('/')}}/public/images/profile_image/{{$user->image}}">
										</div>
										<p>{{$user->fname.' '.$user->lname}}</p>
										<button class="btn block_btn" id="unblock_user" data-blocked-user-id = "{{$user->blocked_user_id}}">Unblock</button>
									</div>
								</li>
							@empty
								<li>
									<div class="blck_mbr_outer text-center">
									No Blocked Members!!!
									</div>
								</li>
							@endforelse
							</ul>
						</div>
					</div>
					<div id="chng_pswd" class="tab-pane fade">
						<form action="{{URL::to('/')}}/change-password" method="post" class="validate-me">
						@csrf
							<h3>Change Password</h3>
							<div class="chng_pswd_sctn">
								<!-- <h4 class="text-center">Enter your old password and new password to change your password</h4> -->
								<div class="form-group">
									<label>Old Password</label>
									<input type="password" name="old_password" placeholder="Enter Your Old Password" class="form-control no-space" required autocomplete="off">
								</div>
								<div class="form-group">
									<label>New Password</label>
									<input type="password" name="password" id="password" placeholder="Enter Your New Password" class="form-control no-space" minlength="8" maxlength="32" required autocomplete="off">
								</div>
								<div class="form-group">
									<label>Confirm Password</label>
									<input type="password" name="password_confirmation" placeholder="ReEnter Your Password" equalto="#password" class="form-control no-space" minlength="8" maxlength="32" required autocomplete="off">
								</div>
								<div class="innerryt_linkbtn text-center">
									<button class="btn btn_more" type="submit">Submit</button>
								</div>
							</div>
						</form>
					</div>
					<div id="abt_us" class="tab-pane fade">
						<h3>About Us</h3>
						<div class="inner_abt_sctn">
							<ul>
								<li>
									<h4>1914 translation by H. Rackham</h4>
									<p>It is a long established fact that a reader will be distracted by the readable content of a page when
									looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution
									of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
								</li>
								<li>
									<h4>The standard Lorem Ipsum passage, used since the 1500s</h4>
									<p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default
									model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various
									versions have evolved over the years, sometimes by accident, sometimes on purpose (injected
									humour and the like).</p>
								</li>
								<li>
									<h4>Section 1.10.33 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h4>
									<p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default
									model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various
									versions have evolved over the years, sometimes by accident, sometimes on purpose (injected
									humour and the like).</p>
								</li>
							</ul>
						</div>
					</div>
				  </div>
			</div>
		</div>
		</div>
	</div>
</section>
@stop
