@extends('layouts.outer')
@section('content')
<section class="inner_linkpages">
	<div class="container">
		<div class="col-md-10 col-md-offset-1">
			<div class="inner_linkouter">
				<div class="col-md-5 paddg_0_left col-sm-5">
					<div class="left_innerlink">
						<div class="left_innlinkcontnt">
							<h2>Hello friend!</h2>
							<p>Enter your personal details and start journey with us</p>
							<a class="left_innerbtn" href="{{URL::to('/')}}/sign-up">Signup</a>
						</div>
					</div>
				</div>
				<div class="col-md-7 col-sm-7">
						<div class="ryt_innerlink">
							<div class="inner_topsection">
								<h2 class="text-center">Login</h2>
								<ul class="text-center">
									<li><a href="{{URL::to('/')}}/login/facebook" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li><a href="{{URL::to('/')}}/login/google" class="google"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
									<li><a href="{{URL::to('/')}}/login/linkedin" class="linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
								</ul>
								<p class="text-center">or use your email account</p>
							</div>
							@if (Session::has('message'))
								  <div class="alert alert-info">{{ Session::get('message') }}</div>
						   @endif
							<form action="{{URL::to('/')}}/login" method="post" class="validate-me">
							@csrf
								
								<div class="form-group">
									<label>Email</label>
									<input type="email" name="email" class="form-control" placeholder="Enter Email" required="">
								</div>
								<div class="form-group">
									<label>Password</label>
									<input type="password" name="password" class="form-control no-space" placeholder="Enter Password" required >
								</div>
								<h5 class="text-center"><a href="{{URL::to('/')}}/forgot-password" class="forgot-password">Forgot your password</a></h5>
								<div class="innerryt_linkbtn text-center">
									<input class="btn btn_more" type="submit" value="LOGIN">
								</div>
							</form>
						</div>
				</div>
			</div>
		</div>
	</div>
</section>

@stop
