@php
 $start=0;
@endphp
@forelse($events as $data)

<div class="col-md-4 col-sm-6">
					<div class="event_section_inner">
						<div class="inner_event_img">
							<div class="inner_event_mainimg">
								<img src="{{URL::to('/')}}/public/images/events_images/{{$data->image}}">
							</div>
								<div class="date_home">
									<h3>{{date('d', strtotime($data->date_time))}} </h3>
									<p>{{date('M', strtotime($data->date_time))}}</p>
								</div>
								@if(Auth::user())
								<div class="hert_like">
									<input type="hidden" name="eventId" id="eventId" value="{{$data->id}}">
									<input type="hidden" name="userId" id="userId" value="{{Auth::id()}}">
									@if($data->favourite == 0)
									<a href="javascript:void(0)" class="event_favourite" data-event-id="{{$data->id}}"><div class="plus heart"></div></a>
									@else
									<a href="javascript:void(0)" class="event_favourite" data-event-id="{{$data->id}}"><div class="plus heart minus"></div></a>
									@endif
								</div>
								@endif
						</div>
						<div class="inner_event_descrption">
							<h3>
							@php
							if(strlen($data->name) > 25){
								$eventName = substr($data->name, 0, 25)." ...";
							}else{
								$eventName = $data->name;
							}
							@endphp
							{{$eventName}}
							</h3>
							<p class="event_desc">
								@php
								if(strlen($data->description) > 60){
									$eventDescription = substr($data->description, 0, 60)." ...";
								}else{
									$eventDescription = $data->description;
								}
								@endphp
								{{$eventDescription}}
							</p>
							@php
								if(strlen($data->location) > 30){
									$eventLocation = substr($data->location, 0, 30)." ...";
								}else{
									$eventLocation = $data->location;
								}
							@endphp
							<h6><img src="{{URL::to('/')}}/public/images/location.png">{{$eventLocation}}</h6>
								<div class="evnt_button">
									<ul>
										<li><a href="{{$data->ticket_url}}" target="__blank" class="btn_yellow btn_css text-uppercase">Buy tickets</a></li>
										<li><a href="{{URL::to('/')}}/event-details/{{$data->id}}" class="btn_green btn_css text-uppercase">view details</a></li>
									</ul>
								</div>
						</div>
					</div>
				</div>	
@php
 $start++;
@endphp
 @empty
@endforelse

@php
 $limit = Config::get("constants.PAGINATION_LIMIT");
if($start < $limit){
   @endphp
   <style>
    .load_more_events{
    	display:none;
    }
   </style>
   @php
 }
@endphp
