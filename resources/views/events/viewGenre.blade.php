@extends('layouts.admin.comman')
@section('content')
<div class="content-wrapper">
	<section class="content">
		<div class="box-header">
    <h3>Genre Details</h3>
			<a class="pull-right btn btn-success" href="{{ URL::to('admin/genre') }}"> << Back</a>
		</div>
		<div class="box-body">
		      	@if(Session::has('message'))
	            	<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{Session::get('message')}}</p>
            	@endif
              <table id="data-table" class="table table-bordered table-hover">
                <tr>
                  <th>Name</th>
                  <td>{{ ucfirst($genres->name) }}</td>
              	</tr>
              	<tr>
                  <th>Status</th>
                  <td>
                        <input type="checkbox" data-id="{{ $genres->id }}" class="subAdminStatus" data-toggle="toggle" data-on="Active
                        " data-off="Inactive" data-onstyle="primary" data-offstyle="danger" data-token="{{ csrf_token() }}" {{ ($genres->status == 1)?"checked":"" }} />
                      </td>
                  </tr>
              	<tr>
                  <th>Date & Time</th>
                  <td>{{ $genres->created_at }}</td>
                </tr>

            </table>
          </div>
	</section>
</div>
@stop