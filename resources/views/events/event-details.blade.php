@php
if(Auth::check())
	$layout = 'layouts.inner';
else
	$layout = 'layouts.outer';
@endphp
@extends($layout)
@section('content')
<section class="event_detail_section">
	<div class="container">
		<div class="row">
			<div class="event_detail_outer">
				<div class="col-md-8">
					<div class="evnt_detl_img">
							<div class="ent_detal_singlimg">
							<img src="{{URL::to('/')}}/public/images/events_images/{{$event->image}}">
						</div>
						<div class="like_count">
							<h4 id="like">{{$event->like}}</h4>
							<h6><i class="fa fa-thumbs-up" aria-hidden="true"></i></h6>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="event_detl_info">
						<h2>{{$event->name}}</h2>
						<div class="review_top">
							<ul>
								@for ($i = 0; $i < 5; ++$i)
								<li><i class="fa fa-star{{$ratings <= $i ?'-o':''}}" aria-hidden="true"></i></li>
								@endfor
							</ul>
							<span>{{$totalFeedback}}<button href="" class="btn btn-link btn_revw" data-toggle="modal" data-target="#myModal" style="text-decoration:underline">Reviews</button></span>
						</div>
						<div class="addres_info_evnt">
							<ul>
								<li>
									<h4>Location</h4>
									<h5>{{$event->location}}</h5>
								</li>
								<li>
									<h4>Date and Time</h4>
									<h5>{{date('d M Y, h:i A', strtotime($event->date_time))}}</h5>
								</li>
								<li>
									<h4>Genre</h4>
									<h5>{{$event->genre}}</h5>
								</li>
							</ul>
							<div class="socl_btn">
								<ul>

								@php
								
									if($event->user_like == 0){
									  $like = "";
									  $unlike = "hide";
									}else{
										$like = "hide";
										$unlike = "";
									}
								@endphp
<li class="like_event {{$like}}"><button class="btn_top btn_red" onclick="likeEvent()"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>like</button></li>
									
									<li class="unlike_event {{$unlike}}"><button class="btn_top btn_red" onclick="unlikeEvent()"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i>Unlike</button></li>
									<li><button class="btn_top btn_blue" data-toggle="modal" data-target="#myModals"><i class="fa fa-share-alt" aria-hidden="true"></i>Share</button></li>
									<li><a href="{{URL::to('/')}}/messages?event_id={{$event->id}}"><button class="btn_top btn_message"><i class="fa fa-comment" aria-hidden="true"></i>Message</button></a></li>

								</ul>
							</div>
							<a href="{{$event->ticket_url}}" target="__blank" class="btn btn_tickt">Buy tickets</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="bottom_sction_detal_evnt">
	<div class="container">
		<div class="row">
			<div class="bottom_descption_evnt">
				<div class="col-md-8">
					<input type="hidden" name="userId" id="userId" value="{{Auth::id()}}">
					<input type="hidden" name="eventId" id="eventId" value="{{$event->id}}">
					<div class="descptn_sctn">
						<h3>Description</h3>
						<p>{{$event->description}}</p>
					</div>
					<div class="commnt_sctn">
						<h3>Comments ({{$event->comment_count}})</h3>
						<div class="cmnt_here">
							<input type="hidden" name="userId" id="userId" value="{{Auth::id()}}">
							<textarea class="form-control" id="comment" placeholder="Write comment here..."></textarea>
						</div>
						<button class="btn btn_more pull-right" onclick="eventComment({{$event->id}})">Post</button>
						<div class="bottom-revews_detl">
							<div class="revews_detl_inner">
								<ul>
									@forelse($event->comment as $comment)
									<li>
										<div class="revw_detl_thumb">
											<div class="revw_thumb_img">
												<a href="{{URL::to('/')}}/other-user-profile/{{$comment->user_id}}"><img src="{{URL::to('/')}}/public/images/profile_image/{{$userImg[$comment->user_id]}}" /></a>
											</div>
											<div class="ryt_info_detl">
												<div class="head_evnt">
													<h3><a href="{{URL::to('/')}}/other-user-profile/{{$comment->user_id}}">{{$userArr[$comment->user_id]}}</a></h3>
													<span>{{ TimeElapsed::getElapsedTime($comment->created_at) }}</span>
												</div>
												<p>{{$comment->comment}}</p>
											</div>
										</div>
									</li>
									@empty
									@endforelse
								</ul>
							</div>
						</div>
					</div>
					<!-- <div class="view_more text-center">
						<button class="btn btn_more">VIEW MORE</button>
					</div> -->
				</div>
				<div class="col-md-4 padding_0r">
					<div class="artist_ryt">
						<h3>Artists Performing </h3>
						<ul>
						@forelse($event->artist as $data)
							<li>
								<div class="artist-img">
									<div class="artst_imgs"><img src="{{URL::to('/')}}/public/images/{{$data->image}}"></div>
									<p>{{$data->name}}</p>
								</div>
							</li>
						@empty

						@endforelse
						</ul>
					</div>
					<div class="feedck_sction_bottom artist_ryt">
						<h3>Feedback </h3>
						<form class="feedback_for" action="">
						@csrf
						<div class="filds_feedbck">
							<div class="str_sctn">
								<h5>Rating</h5>
								<div class="rating">
								<input id="star5" name="star" type="radio" value="5" class="radio-btn hide" />
								<label for="star5" >☆</label>
								<input id="star4" name="star" type="radio" value="4" class="radio-btn hide" />
								<label for="star4" >☆</label>
								<input id="star3" name="star" type="radio" value="3" class="radio-btn hide" />
								<label for="star3" >☆</label>
								<input id="star2" name="star" type="radio" value="2" class="radio-btn hide" />
								<label for="star2" >☆</label>
								<input id="star1" name="star" type="radio" value="1" class="radio-btn hide" />
								<label for="star1" >☆</label>
								<div class="clear"></div>
								</div>
							</div>
							</form>
							<div class="form-group">
								<textarea class="form-control" id="reviews" value="" placeholder="Write Feedback Here....."></textarea>
							</div>
							<div class="view_more text-center">
								<button class="btn btn_more rating_review" data-event-id="{{$event->id}}">SUBMIT</button>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="review_popup modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center">feedback</h4>
        </div>
        <div class="modal-body">
			<div class="bottom_descption_evnt bottom-revews_detl">
				<div class="revews_detl_inner">
					<ul>
						@forelse($feedback as $review)
						<li>
							<div class="revw_detl_thumb">
								<div class="revw_thumb_img">
									<a href="{{URL::to('/')}}/other-user-profile/{{$review->user_id}}"><img src="{{URL::to('/')}}/public/images/profile_image/{{$review->image}}" />
									</a>
								</div>
								<div class="ryt_info_detl">
									<div class="head_evnt">
										<a href="{{URL::to('/')}}/other-user-profile/{{$review->user_id}}"><h3>{{$review->fname.' '.$review->lname}}</h3></a>
										<span>{{ TimeElapsed::getElapsedTime($review->created_at) }}</span>
									</div>
									<p>{{$review->reviews}}</p>
								</div>
							</div>
						</li>
						@empty
						@endforelse
					</ul>
				</div>
			</div>
        </div>
        <div class="modal-footer">
          	<!-- <div class="view_more text-center">
				<button class="btn btn_more">VIEW ALL</button>
			</div> -->
        </div>
      </div>
    </div>
</div>
<div class="review_popup modal fade" id="myModals" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center">Share</h4>
        </div>
        <div class="modal-body">
         	<div class="bottom_descption_evnt bottom-revews_detl share_link">
				<div class="revews_detl_inner">
					<ul class="social_icon_list">
						<li>
							<a href="https://www.facebook.com/sharer/sharer.php?u={{URL::to('/')}}/event-details/{{$event->id}}" target="_blank">
								<img src="{{URL::to('/')}}/public/images/facebook.png">
								<p>Facebook</p>
							</a>
						</li>
						<li>
							<a href="https://plus.google.com/share?url={{URL::to('/')}}/event-details/{{$event->id}}" target="_blank">
								<img src="{{URL::to('/')}}/public/images/google.png">
								<p>Google</p>
							</a>
						</li>
						<li>
							<a href="https://www.linkedin.com/sharing/share-offsite/?url={{URL::to('/')}}/event-details/{{$event->id}}&source=LinkedIn" target="_blank">
								<img src="{{URL::to('/')}}/public/images/linkedin.png">
								<p>Linkedin</p>
							</a>
						</li>
					</ul>
				</div>
			</div>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
@stop