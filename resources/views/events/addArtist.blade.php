@extends('layouts.admin.comman')
@section('content')
<div class="content-wrapper">
	<section class="content-header">
		<div class="containaer">
    <h3>Add Artist</h3>
    @if(Session::has('message'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{Session::get('message')}}</p>
    @endif
    <div class="col-md-8 col-sm-12 col-xs-12">
	    <form action="{{URL::to('/')}}/admin/saveArtist" method="post" enctype="multipart/form-data">
	    @csrf
	    <div class="form-group">
	      	<label>Name</label>
	      	<input type="text" name="name" class="form-control" placeholder="Enter First Name" required />
	  	</div>
	  	<div class="form-group">
		  	<label>Image</label>
		  	<input type='file' name="image" id="imageUpload" accept=".png, .jpg, .jpeg" />
	  	</div>
	  	<div class="form-group">
	      	<label>Status</label>&ensp;
	      	<input type="radio" name="status" value="1" checked />&nbsp;Enable&ensp;
	      	<input type="radio" name="status" value="0" />&nbsp;Disable
		</div>
	    <div class="form-group">
	      	<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
	    </div>
	    </form>
    </div>
</div>
	</section>
</div>
@stop