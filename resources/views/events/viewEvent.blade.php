@extends('layouts.admin.comman')
@section('content')
<div class="content-wrapper">
	<section class="content">
		<div class="box-header">
    <h3>Event Details</h3>
			<a class="pull-right btn btn-success" href="{{ URL::to('admin/upcoming-events') }}"> << Back</a>
		</div>
		<div class="box-body">
		      	@if(Session::has('message'))
	            	<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{Session::get('message')}}</p>
            	@endif
              <table id="data-table" class="table table-bordered table-hover">
                <tr>
                  <th>Name</th>
                  <td>{{ ucfirst($event->name) }}</td>
              	</tr>
                <tr>
                  <th>Description</th>
                  <td>{{ ucfirst($event->description) }}</td>
                </tr>
                <tr>
                  <th>Image</th>
                  <td><a href="{{URL::to('/')}}/public/images/events_images/{{$event->image}}" target="__blank"><img src="{{URL::to('/')}}/public/images/events_images/{{$event->image}}" class="image-size"></a></td>
                </tr>
                <tr>
                  <th>Buy Ticket Url</th>
                  <td>{{ $event->ticket_url }}</td>
                </tr>
                <tr>
                  <th>Genre</th>
                  <td>{{ $event->genere_name }}</td>
                </tr>
                <tr>
                  <th>Artists</th>
                  <td>{{ $event->artists_name }}</td>
                </tr>
              	<tr>
                  <th>Date & Time</th>
                  <td>{{ $event->created_at }}</td>
                </tr>

            </table>
          </div>
	</section>
</div>
@stop