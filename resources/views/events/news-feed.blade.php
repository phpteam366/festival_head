@extends('layouts.inner')
@section('content')
<style>
.btn.btn_more.submit_comment_news {
	line-height: 31px;
	max-width: 206px;
	font-size: 18px;
	margin-top: 20px;
}
</style>
<section class="myprofile_section news_fed_sctn">
	<div class="container">
		<div class="col-md-10 col-md-offset-1">
			<div class="add_post_sction">
			<form action="{{URL::to('/')}}/addNewsFeed" class="validate_me_newsfeed" method="post" enctype="multipart/form-data">
			@csrf
				<h3>Add New Post</h3>
				<div class="form-group">
					<input type="text" name="title" placeholder="Add Title" class="form-control" required>
				</div>
				<div class="form-group">
					<textarea name="description" placeholder="Write description here..." class="form-control" required></textarea>
				</div>
				<div class="img_fed_btn">
					<div class="img_jpg_upld pull-left">
						<div class="box">
							<input type="file" name="files" id="file-5" class="inputfileChoose inputfile inputfile-4" data-multiple-caption="{count} files selected" required/>
							<label for="file-5"><figure>
								<img src="{{URL::to('/')}}/public/images/upload_img1.png"><img style="margin: 0px 10px;" src="{{URL::to('/')}}/public/images/upload_video.png">		
							</figure>
							
							<span>Choose a file&hellip;</span></label>
						</div>	

					<span class="file_err"></span>
<div class="progress file_chosen hide">
                        <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                            <span class="sr-only"></span>
                        </div>
                    </div>
					</div>
					<div class="btn_othr_prfol pull-right">
						<button type="submit" class="btn block_btn">POST</button>
					</div>
				</div>
				</form>
			</div>
			@forelse($newsFeed as $news)
			
			<div class="bottom-revews_detl profile_post_listng revews_detl_inner">
				<ul>
					<li>
						<div class="revw_detl_thumb">
							<div class="revw_thumb_img">
								<a href="{{URL::to('/')}}/other-user-profile/{{$news->user_id}}"><img src="{{URL::to('/')}}/public/images/profile_image/{{$news->image}}"></a>
							</div>
							<div class="ryt_info_detl">
								<div class="head_evnt">
									<h3><a href="{{URL::to('/')}}/news-feed-details/{{$news->feed_id}}">{{$news->title}}</a></h3>
									<span>{{ TimeElapsed::getElapsedTime($news->feeds_created) }}</span>
								</div>
								<p>{{$news->description}}</p>
							</div>
						</div>
					</li>
				</ul>
				<div class="post_img">
					@if($news->feed_image == '' && $news->feed_video != '')
					@php
					  $extnsion = explode('.',$news->feed_video);
					  if(isset($extnsion) && !empty($extnsion[1])){
					  	$ext = $extnsion[1];
					  }else{
					  	$ext = "mp4";
					  }
					@endphp
						<video class="video_news" controls>
					  <source src="{{URL::to('/')}}/public/images/news_feeds/{{$news->feed_video}}" type="video/{{$ext}}">
					Your browser does not support the video.
					</video>
					@elseif($news->feed_video == '' && $news->feed_image != '')
					<div class="post_img_singls">
						<img src="{{URL::to('/')}}/public/images/news_feeds/{{$news->feed_image}}">
					</div>
					@endif
				</div>
				<div class="post_commt_profl cmnt_here">
					<h3><a href="" style="text-decoration:underline" data-toggle="modal" data-target="#myModal" class="feed_comment" data-news-id='{{$news->feed_id}}'>Comments</a> (<span class="total_count{{$news->feed_id}}">{{$totalComment[$news->feed_id]}}</span>)</h3>
					<ul>
						<li><a href="" class="like_news" data-news-id='{{$news->feed_id}}' data-user-id='{{$news->user_id}}'><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a><span id="total_like{{$news->feed_id}}">{{$feedLikes[$news->feed_id]}}</span></li>
						<li><a href="#"><i class="fa fa-share-alt feed_share" data-news-id='{{$news->feed_id}}' data-toggle="modal" data-target="#myModals" aria-hidden="true"></i></a><span></span></li>
					</ul>
					<div class="cmmnt_area show_hide">
						<textarea class="form-control news_comment news_comment_{{$news->feed_id}}" value="" data-feed-id = "{{$news->feed_id}}" data-user-id='{{$news->user_id}}' placeholder="Write comment here..."></textarea>
						<button class="btn btn_more submit_comment_news" data-feed-id = "{{$news->feed_id}}" data-user-id='{{$news->user_id}}'>Post</button>
					</div>
				</div>
			</div>
			@empty
			@endforelse
			
		</div>
	</div>
</section>

<div class="review_popup modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center">Comments</h4>
        </div>
        <div class="modal-body commnt_popup">
			<div class="bottom_descption_evnt bottom-revews_detl">
				<div class="revews_detl_inner">
					<ul class="comment_section">
						
					</ul>
				</div>
			</div>
        </div>
        <div class="modal-footer">
          	<div class="view_more text-center">
				<!-- <button class="btn btn_more">VIEW ALL</button> -->
			</div>
        </div>
      </div>
    </div>
</div>
<div class="review_popup modal fade" id="myModals" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center">Share</h4>
        </div>
        <div class="modal-body">
         	<div class="bottom_descption_evnt bottom-revews_detl share_link">
				<div class="revews_detl_inner">
					<ul class="social_icon_list">
						<li>
							<a href="" class="facebook_share" target="_blank">
								<img src="{{URL::to('/')}}/public/images/facebook.png">
								<p>Facebook</p>
							</a>
						</li>
						<li>
							<a href="" class="google_share" target="_blank">
								<img src="{{URL::to('/')}}/public/images/google.png">
								<p>Google</p>
							</a>
						</li>
						<li>
							<a href="" class="linkedin_share" target="_blank">
								<img src="{{URL::to('/')}}/public/images/linkedin.png">
								<p>Linkedin</p>
							</a>
						</li>
					</ul>
				</div>
			</div>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
  
@stop
