@extends('layouts.admin.comman')
@section('content')
<div class="content-wrapper">
	<section class="content-header">
	<div class="box-header">
		<h1 class="box-title">Edit Event</h1>
	</div>
    @if(Session::has('message'))
      <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{Session::get('message')}}</p>
    @endif
	<form action="{{URL::to('/')}}/admin/update-events" method="post" class="validate-me" enctype="multipart/form-data">
	@csrf

	<input type="hidden" name="event_id" value="{{$eventDetails->id}}" />
    <div class="form-group">
      	<label>Event Title</label>
      	<input type="text" name="title" class="form-control" placeholder="Enter event title" value="{{$eventDetails->name}}" required />
  	</div>
  	<div class="form-group">
      	<label>Description</label>
      	<textarea rows="4" cols="50" name="description" class="form-control" placeholder="Enter description" required>{{$eventDetails->description}}</textarea>
  	</div>
  	

    <div class="form-group">
        <label>Location</label>
        <input id="event_location" class="form-control" name="location" type="text" value="{{$eventDetails->location}}" required>
              <input type="hidden" id="city2" name="location_gmap" value="{{$eventDetails->location}}"/>
              <input type="hidden" id="cityLat" name="lat_gmap" value="{{$eventDetails->latitude}}"/>
              <input type="hidden" id="cityLng" name="long_gmap" value="{{$eventDetails->longitude}}"/>
    </div>

@php
$eventDateTime = $eventDetails->date_time;
$eventDate     = date("Y-m-d",strtotime($eventDateTime));
$eventTime     = date("H:i",strtotime($eventDateTime));

@endphp

  	<div class="form-group">
      	<label>Date</label>
      	<input class="datepicker form-control" name="event_date" type="text" value="{{$eventDate}}" required>
  	</div>
  	<div class="form-group">
      	<label>Time</label>
      	<input class="timepicker form-control" name="event_time" type="text" value="{{$eventTime}}" required>
  	</div>

  

     <div class="form-group">
        <label>Buy Ticket Url</label>
        <input type="url" name="ticket_url" value="{{$eventDetails->ticket_url}}" class="form-control" placeholder="Enter ticket url" required />
    </div>
@php
$selectedArtists = explode(',',$eventDetails->artists_id);
@endphp
      <div class="form-group">
        <label>Artists Performing</label>
        <select name="artist[]" class="chosen_multi form-control " data-placeholder="Select a asrtists" multiple required>
        @forelse($getAllArtists as $artist)
         @php
          $selected = "";
          if(in_array($artist->id,$selectedArtists)){
             $selected = "selected";
          }
         @endphp
         <option value="{{$artist->id}}" {{$selected}}>{{$artist->name}}</option>
         @empty
        @endforelse
        </select>
        <span class="choose_artists_error"></span>
    </div>
@php
$selectedGeneres = explode(',',$eventDetails->genre_id);
@endphp
     <div class="form-group">
        <label>Genre</label>
        <select name="genere_id[]" class="chosen_multi form-control" data-title="Select Genere" multiple required>
           <option value="">Select a genere</option>
          @forelse($getAllGenere as $genere)
            <option value="{{$genere->id}}" <?php  if(in_array($genere->id, $selectedGeneres)){ ?> selected <?php } ?>>{{$genere->name}}</option>
          @empty
          @endforelse
        </select>
        <span class="genere_id_error"></span>
    </div>

     <div class="form-group">
        <label>Image</label>
        <input type="hidden" name="old_image" value="{{$eventDetails->image}}" />
        <input type="file" name="event_image" class="form-control" placeholder=""/>
    </div>


  	<div class="form-group">
      	<input type="submit" name="submit" value="Update" class="btn btn-primary" />
    </div>
	</form>
	</section>
</div>

@stop
