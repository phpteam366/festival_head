@extends('layouts.admin.comman')
@section('content')
<div class="content-wrapper">
	<section class="content">
		<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Update Genres</h3>
              <a class="pull-right btn btn-success" href="{{ URL::to('admin/genre') }}"> << Back</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             
              <div class="col-md-8 col-sm-12 col-xs-12">
              <form action="{{ URL::to('/')}}/admin/updateGenre" class="edit_user" method="post">
               @csrf
                  <div class="form-group">
                    <label> First Name</label>
                    <input type="hidden" name="id" value="{{ $genres->id }}">
                    <input type="text" name="name" class="form-control" required placeholder="Enter Name" value="{{$genres->name}}" />
                  </div>
                  <div class="form-group">
                    <label>Status</label>&ensp;
                    <input type="radio" name="status" value="1" {{($genres->status==1)?"checked":""}}>&nbsp;Active &ensp; 
                    <input type="radio" name="status" value="0" {{($genres->status==0)?"checked":""}}>&nbsp;Inactive   
                  </div>

                  <div class="form-group">
                    <input type="submit" name="submit" value="Update" class="btn btn-primary pull-right"/>
                  </div>
              </form>
              </div>
          </div>
        </div>
      </div>
    </div>
	</section>
</div>
@stop