@extends('layouts.admin.comman')
@section('content')
<div class="content-wrapper">
	<section class="content">
		<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Update Artist</h3>
              <a class="pull-right btn btn-success" href="{{ URL::to('admin/artist') }}"> << Back</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             
              <div class="col-md-8 col-sm-12 col-xs-12">
              <form action="{{ URL::to('/')}}/admin/updateArtist" class="edit_user" method="post" enctype="multipart/form-data">
               @csrf
                  <div class="form-group">
                    <label> First Name</label>
                    <input type="hidden" name="id" value="{{ $artists->id }}">
                    <input type="text" name="name" class="form-control" required placeholder="Enter Name" value="{{$artists->name}}" />
                  </div>

                  <div class="form-group">
                    <label>Image</label>
                    <input type='file' name="image" id="imageUpload" accept=".png, .jpg, .jpeg" />
                  </div>
                  <div class="form-group">
                    <label>Status</label>&ensp;
                    <input type="radio" name="status" value="1" {{($artists->status==1)?"checked":""}}>&nbsp;Active &ensp; 
                    <input type="radio" name="status" value="0" {{($artists->status==0)?"checked":""}}>&nbsp;Inactive   
                  </div>

                  <div class="form-group">
                    <input type="submit" name="submit" value="Update" class="btn btn-primary pull-right"/>
                  </div>
              </form>
              </div>
          </div>
        </div>
      </div>
    </div>
	</section>
</div>
@stop