@extends('layouts.inner')
@section('content')
<section class="event_section fav_sctn_iner height_100">
	<div class="inner_contnt_sction text-center">
		<h1>Favourites</h1>
	</div>
	<div class="container">
		<div class="row">
			<div class="event_section_outer">
			@forelse($events as $data)
				<div class="col-md-4 col-sm-6">
					<div class="event_section_inner">
						<div class="inner_event_img">
							<div class="inner_event_mainimg">
								<img src="{{URL::to('/')}}/public/images/events_images/{{$data->image}}">
							</div>
							<!-- <img src="{{URL::to('/')}}/public/images/img1.png"> -->
								<div class="date_home">
									<h3>{{date('d', strtotime($data->date_time))}} </h3>
									<p>{{date('M', strtotime($data->date_time))}}</p>
								</div>
								<div class="hert_like">
									<input type="hidden" name="eventId" id="eventId" value="{{$data->id}}">
									<input type="hidden" name="userId" id="userId" value="{{Auth::id()}}">
									<a href="#" onclick="unfavouriteEvent({{$data->id}})" ><div class="plus heart minus"></div></a>
								</div>
						</div>
						<div class="inner_event_descrption">
							<h3>
							@php
							if(strlen($data->name) > 25){
								$eventName = substr($data->name, 0, 25)." ...";
							}else{
								$eventName = $data->name;
							}
							@endphp
							{{$eventName}}
							</h3>
							<p class="event_desc">
								@php
								if(strlen($data->description) > 60){
									$eventDescription = substr($data->description, 0, 60)." ...";
								}else{
									$eventDescription = $data->description;
								}
								@endphp
								{{$eventDescription}}
							</p>
							<h6>
							@php
								if(strlen($data->location) > 30){
									$eventLocation = substr($data->location, 0, 30)." ...";
								}else{
									$eventLocation = $data->location;
								}
							@endphp
							<img src="{{URL::to('/')}}/public/images/location.png">{{$eventLocation}}</h6>
								<div class="evnt_button">
									<ul>
										<li><a href="{{$data->ticket_url}}" target="__blank" class="btn_yellow btn_css text-uppercase">Buy tickets</a></li>
										<li><a href="{{URL::to('/')}}/event-details/{{$data->id}}" class="btn_green btn_css text-uppercase">view details</a></li>
									</ul>
								</div>
						</div>
					</div>
				</div>	
				@empty
				<div class="text-center">No events in the Favourites!!!</div>
				@endforelse
			</div>
		<!-- </div> -->
		@if(count($events) > 9)
		<!-- <div class="row">
			<div class="col-md-12">
				<div class="view_more text-center">
					<button class="btn btn_more">VIEW MORE</button>
				</div>
			</div>
		</div> -->
		@endif
	</div>
</section>
@stop