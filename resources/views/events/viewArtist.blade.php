@extends('layouts.admin.comman')
@section('content')
<div class="content-wrapper">
	<section class="content">
		<div class="box-header">
    <h3>Artist Details</h3>
			<a class="pull-right btn btn-success" href="{{ URL::to('admin/artist') }}"> << Back</a>
		</div>
		<div class="box-body">
		      	@if(Session::has('message'))
	            	<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{Session::get('message')}}</p>
            	@endif
              <table id="data-table" class="table table-bordered table-hover">
                <tr>
                  <th>Name</th>
                  <td>{{ ucfirst($artists->name) }}</td>
              	</tr>
                <tr>
                  <th>Image</th>
                  <td><a href="{{URL::to('/')}}/public/images/{{$artists->image}}" target="__blank"><img src="{{URL::to('/')}}/public/images/{{$artists->image}}" class="image-size"></a></td>
                </tr>
              	<tr>
                  <th>Status</th>
                  <td>
                        <input type="checkbox" data-id="{{ $artists->id }}" class="subAdminStatus" data-toggle="toggle" data-on="Active
                        " data-off="Inactive" data-onstyle="primary" data-offstyle="danger" data-token="{{ csrf_token() }}" {{ ($artists->status == 1)?"checked":"" }} />
                      </td>
                  </tr>
              	<tr>
                  <th>Date & Time</th>
                  <td>{{ $artists->created_at }}</td>
                </tr>

            </table>
          </div>
	</section>
</div>
@stop