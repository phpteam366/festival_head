@extends('layouts.admin.comman')
@section('content')
<div class="content-wrapper">
	<section class="content-header">
	<div class="box-header">
		<h1 class="box-title">Add Event</h1>
	</div>
    @if(Session::has('message'))
      <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{Session::get('message')}}</p>
    @endif
	<form action="{{URL::to('/')}}/admin/events" method="post" class="validate-me" enctype="multipart/form-data">
	@csrf
	
    <div class="form-group">
      	<label>Event Title</label>
      	<input type="text" name="title" class="form-control" placeholder="Enter event title" required />
  	</div>
  	<div class="form-group">
      	<label>Description</label>
      	<textarea rows="4" cols="50" name="description" class="form-control" placeholder="Enter description" required></textarea>
  	</div>
  	

    <div class="form-group">
        <label>Location</label>
        <input id="event_location" class="form-control" name="location" type="text" required>
              <input type="hidden" id="city2" name="location_gmap" />
              <input type="hidden" id="cityLat" name="lat_gmap" />
              <input type="hidden" id="cityLng" name="long_gmap" />
    </div>



  	<div class="form-group">
      	<label>Date</label>
      	<input class="datepicker form-control" name="event_date" type="text" required>
  	</div>
  	<div class="form-group">
      	<label>Time</label>
      	<input class="timepicker form-control" name="event_time" type="text" required>
  	</div>

  

     <div class="form-group">
        <label>Buy Ticket Url</label>
        <input type="url" name="ticket_url" class="form-control" placeholder="Enter ticket url" required />
    </div>

      <div class="form-group">
        <label>Artists Performing</label>
        <select name="artist[]" class="chosen_multi form-control " data-placeholder="Select a artists" multiple required>
        @forelse($getAllArtists as $artist)
         <option value="{{$artist->id}}">{{$artist->name}}</option>
         @empty
        @endforelse
        </select>
        <span class="choose_artists_error"></span>
    </div>

     <div class="form-group">
        <label>Genre</label>
        <select name="genere_id[]" class="chosen_multi form-control" data-placeholder="Select Genere" multiple required>
           <option value="">Select a genere</option>
          @forelse($getAllGenere as $genere)
         <option value="{{$genere->id}}">{{$genere->name}}</option>
         @empty
        @endforelse
        </select>
        <span class="genere_id_error"></span>
    </div>

     <div class="form-group">
        <label>Image</label>
        <input type="file" name="event_image" class="form-control" placeholder="" required />
    </div>


  	<div class="form-group">
      	<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
    </div>
	</form>
	</section>
</div>

@stop