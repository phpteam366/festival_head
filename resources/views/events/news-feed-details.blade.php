@extends('layouts.inner')
@section('content')
<section class="myprofile_section news_fed_sctn">
	<div class="container">
		
	<div class="bottom-revews_detl profile_post_listng revews_detl_inner">
				<ul>
					<li>
						<div class="revw_detl_thumb">
							<div class="revw_thumb_img">
									<img src="{{URL::to('/')}}/public/images/profile_image/{{$feedDetail->user_image}}">
							</div>
							<div class="ryt_info_detl">
								<div class="head_evnt">
									<h3>{{$feedDetail->title}}</h3>
									<span>{{ TimeElapsed::getElapsedTime($feedDetail->created_at) }}</span>
								</div>
								<p>{{$feedDetail->description}}</p>
							</div>
						</div>
					</li>
				</ul>
				<div class="post_img">
					@if($feedDetail->image == '' && $feedDetail->video != '')
					@php
					  $extnsion = explode('.',$feedDetail->video);
					  if(isset($extnsion) && !empty($extnsion[1])){
					  	$ext = $extnsion[1];
					  }else{
					  	$ext = "mp4";
					  }
					@endphp
						<video class="video_news" controls>
					  <source src="{{URL::to('/')}}/public/images/news_feeds/{{$feedDetail->video}}" type="video/{{$ext}}">
					Your browser does not support the video.
					</video>
					@elseif($feedDetail->video == '' && $feedDetail->image != '')
					<div class="post_img_singls">
						<img src="{{URL::to('/')}}/public/images/news_feeds/{{$feedDetail->image}}">
					</div>
					@endif
				</div>
				<div class="post_commt_profl cmnt_here">
					<h3><a href="" style="text-decoration:underline" data-toggle="modal" data-target="#myModal">Comments</a><span>({{$totalComments}})</span></h3>
					<ul>
						<li><a href="" class="like_news" data-news-id='{{$feedDetail->id}}' data-user-id='{{$feedDetail->user_id}}'><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a><span id="total_like">{{$totalLikes}}</span></li>
						<li><a href="" data-toggle="modal" data-target="#myModals"><i class="fa fa-share-alt" aria-hidden="true"></i></a></li>
					</ul>
					<div class="cmmnt_area show_hide">
						<textarea class="form-control news_comment" value="" data-feed-id = "{{$feedDetail->id}}" data-user-id='{{$feedDetail->user_id}}' placeholder="Write comment here..."></textarea>
					</div>
					<div class="commnt_area_below slidingDiv">
						<div class="bottom-revews_detl">
							<div class="revews_detl_inner">
								<ul class="comment_list_new">
								@forelse($comments as $comment)
									<!-- s -->
								@empty
								@endforelse
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
</section>


<div class="review_popup modal fade" id="myModals" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center">Share</h4>
        </div>
        <div class="modal-body">
         	<div class="bottom_descption_evnt bottom-revews_detl share_link">
				<div class="revews_detl_inner">
					<ul class="social_icon_list">
						<li>
							<a href="https://www.facebook.com/sharer/sharer.php?u={{URL::to('/')}}/news-feed-details/{{$feedDetail->id}}" target="_blank">
								<img src="{{URL::to('/')}}/public/images/facebook.png">
								<p>Facebook</p>
							</a>
						</li>
						<li>
							<a href="https://plus.google.com/share?url={{URL::to('/')}}/news-feed-details/{{$feedDetail->id}}" target="_blank">
								<img src="{{URL::to('/')}}/public/images/google.png">
								<p>Google</p>
							</a>
						</li>
						<li>
							<a href="https://www.linkedin.com/share?url={{URL::to('/')}}/news-feed-details/{{$feedDetail->id}}&source=LinkedIn" target="_blank">
								<img src="{{URL::to('/')}}/public/images/linkedin.png">
								<p>Linkedin</p>
							</a>
						</li>
					</ul>
				</div>
			</div>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
<div class="review_popup modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center">Comments</h4>
        </div>
        <div class="modal-body commnt_popup">
			<div class="bottom_descption_evnt bottom-revews_detl">
				<div class="revews_detl_inner">
					<ul>
						@forelse($comments as $comment)
						<li>
									
							<div class="revw_detl_thumb">
								<div class="revw_thumb_img">
									<img src="{{URL::to('/')}}/public/images/profile_image/{{$comment->user_image}}">
								</div>
								<div class="ryt_info_detl">
									<div class="head_evnt">
										<h3><a href="{{URL::to('/')}}/other-user-profile/{{$comment->user_id}}">{{$comment->fname.' '.$comment->lname}}</a></h3>
										<span>{{ TimeElapsed::getElapsedTime($comment->commented_at) }}</span>
									</div>
									<p>{{$comment->comment}}</p>
								</div>
							</div>
							
						</li>
						@empty
						@endforelse
					</ul>
				</div>
			</div>
        </div>
        <div class="modal-footer">
          	<div class="view_more text-center">
				<!-- <button class="btn btn_more">VIEW ALL</button> -->
			</div>
        </div>
      </div>
    </div>
</div>
@stop