@extends('layouts.admin.comman')
@section('content')
<div class="content-wrapper">
	<section class="content-header">
		<div class="box-header">
			<form action="{{URL::to('/')}}/admin/addGenre" type="get">
		      	<h1 class="box-title">Music Genres</h1>
		      	<input type="submit" class="btn btn-primary pull-right" value="Add Genre">
	      	</form>
      	</div>
		      <div class="box-body">
		      	@if(Session::has('message'))
	            	<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{Session::get('message')}}</p>
            	@endif
              <table id="data-table" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Status</th>
                  <th>Date & Time</th>
                  <th>Actions</th>
                </tr>

                </thead>
                <tbody>
                  @forelse($genres as $data)
                    <tr>
                      <td>{{ ucfirst($data->name) }}</td>
                      <td>
                        <input type="checkbox" data-id="{{ $data->id }}" class="subAdminStatus" data-toggle="toggle" data-on="Active
                        " data-off="Inactive" data-onstyle="primary" data-offstyle="danger" data-token="{{ csrf_token() }}" {{ ($data->status == 1)?"checked":"" }} />
                      </td>
                      <td>{{ $data->created_at }}</td>
                      <td>
                      	<a href="{{ URL::to('admin/viewGenre').'/'.$data->id }}" class="btn btn-success" title="View Genre"><i class="fa fa-eye"></i></a>
                        <a href="{{ URL::to('admin/editGenre').'/'.$data->id }}" class="btn btn-success" title="Edit Genre"><i class="fa fa-pencil"></i></a>
                        
                         <a href="{{ URL::to('admin/deleteGenre').'/'.$data->id }}" class="btn btn-danger" title="Delete admin" onclick='return confirm("Do you really want to delete this user")'><i class="fa fa-trash-o"></i></a> 
                      </td>
                    </tr>   
                    @empty
                     <td>No Genres!!!</td>
                  @endforelse
              </tbody>
            </table>
          </div>

	</section>
</div>
@stop