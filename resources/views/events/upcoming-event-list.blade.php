@extends('layouts.admin.comman')
@section('content')
<div class="content-wrapper">
	<section class="content-header">
		
		<div class="box-header">
			   <h1 class="box-title">Upcoming Events</h1>
		      	<a class="btn btn-primary pull-right" href="{{URL::to('/')}}/admin/events/add">Add Event</a>
		      	
	      	
      	</div>
	</section>



	<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>
              @if(Session::has('error'))
            <div class="alert alert-danger error_class">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ Session::get('error') }}
            </div>
          @endif
          @if(Session::has('success'))
            <div class="alert alert-success success_class">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ Session::get('success') }}
            </div>
          @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="data_table table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr No.</th>
                  <th class="hide">Id</th>
                  <th>Event Name</th>
                  <th>Event Date&Time</th>
                  <th>Buy Ticket Url</th>
                  <th>Genre</th>
                  <th>Artists</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                $srNo = 0;
                @endphp
                @forelse($getAllUpcomingEvents as $event)
                  @php
                    $srNo++;
                  @endphp
                <tr>
                  <td>{{$srNo}}</td>
                  <td class="hide">{{$event->id}}</td>
                  <td>{{$event->name}}</td>
                  <td>{{$event->date_time}} </td>
                  <td>{{$event->ticket_url}} </td>
                  <td>{{$event->genere_name}} </td>
                  <td>{{$event->artists_name}} </td>
                  <td>
                        <a href="{{ URL::to('admin/viewEvent').'/'.$event->id }}" class="btn btn-success" title="View event"><i class="fa fa-eye"></i></a>
                        <a href="{{URL::to('/')}}/admin/events/{{$event->id}}/edit" class="btn btn-success" title="Edit event"><i class="fa fa-pencil"></i></a>
                       
                         <a href="{{URL::to('/')}}/admin/events/{{$event->id}}/delete" class="btn btn-danger delete_event" title="Delete Evebt" onclick="return confirm('Do you really want to delete this event')"><i class="fa fa-trash-o"></i></a> 
                      </td>


                  
                </tr>
                @empty
                @endforelse


              
                </tbody>
              </table>
              {{ $getAllUpcomingEvents->links() }}
            </div>
            <!-- /.box-body -->
          </div>
        <!-- /.col -->
      </div>
      </div>
      <!-- /.row -->
    </section>
</div>
@stop