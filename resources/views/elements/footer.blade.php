<section class="footer_section">
	<div class="container">
		<div class="col-md-12">
			<div class="col-md-4 col-sm-6">
				<div class="footer_inner">
					<h3>About Us</h3>
					<!-- <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p> -->
				     <p><a class="privacy_links" href="{{URL::to('/')}}/about-us">About Us</a></p>
				     <p><a class="privacy_links" href="{{URL::to('/')}}/privacy-policy">Privacy Policy</a></p>
				   	 <p><a class="privacy_links " href="{{URL::to('/')}}/terms-conditions">Terms and Conditions</a></p>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="footer_inner">
					<h3>Address</h3>
					<ul>
						<!-- <li><i class="fa fa-map-marker" aria-hidden="true"></i> Suite # 505, Fifth Floor, Al-Qadir Heights, New Garden Town, Lahore.</li> -->
						<li >P.O Box 811 INGHAM, QLD 4850</li>
						<li><a href="mailto:admin@festivalhead.comau " class="telephone"><i class="fa fa-envelope" aria-hidden="true"></i> admin@festivalhead.com.au</a></li>
						<li class="hide"><a href="Tel:924238911784" class="telephone"><i class="fa fa-phone" aria-hidden="true"></i> </a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="footer_inner">
					<h3>Follow Us</h3>
					<ul class="social_links">
						<li><a href="https://www.facebook.com/Festival-Head-376754819589850/?modal=admin_todo_tour" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li class="hide"><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
						<li><a href="https://twitter.com/head_festival" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="https://www.instagram.com/festival_head2019" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<p class="copyright_text text-center">FESTIVAL HEAD GROUP © <?php echo date("Y")  ?> </p>
</section>

