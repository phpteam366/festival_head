<!DOCTYPE html>
<html lang="en">
<head>
  <title>Festival Head</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{URL::to('/')}}/public/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{URL::to('/')}}/public/css/style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<section class="haeder_main">
	<div class="container">
		<nav class="navbar navbar-inverse navbr_home">
		  <div class="container-fluid">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>                        
			  </button>
			  <a class="navbar-brand" href="index.html"><img src="{{URL::to('/')}}/public/images/logo.png"></a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<div class="secrh_filed pull-left">
					<input type="text" class="form-control" placeholder="Search Location" autocomplete="off">
				</div>
				<div class="btn_othr_prfol pull-right">
					<ul class="nav navbar-nav">
						<li><a href="login.html" class="btn btn_blush block_btn">Login</a></li>
						<li><a href="signup.html" class="btn block_btn">Sign Up</a></li>
					</ul>
				</div>
			</div>
		  </div>
		</nav>
	</div>
</section>

<section class="filtr_section">
	<div class="container">
		<div class="col-md-12">
			<div class="filter_section_inner">
				<ul>
					<li>
						<div class="filed_sction">
							<select class="form-control">
								<option>Select Users</option>
								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
							</select>
						</div>
					</li>
					<li>
						<div class="filed_sction">
							<select class="form-control">
								<option>Select Event</option>
								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
							</select>
						</div>
					</li>
					<li>
						<div class="filed_sction">
							<select class="form-control">
								<option>Select Genre</option>
								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
							</select>
						</div>
					</li>
					<li>
						<button class="btn_filter">Search</button>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
