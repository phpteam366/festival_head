@extends('layouts/inner')
@section('content')
<section class="banner_conctus">
	<div class="map_area_conct">
		<img src="{{URL::to('/')}}/public/images/contact_us_img.png">
	</div>
	<div class="bottom_contct">
		<div class="container">
			<div class="col-md-12">
				<div class="contact_outer">
					<div class="col-md-6 col-sm-5">
						<div class="left_contct">
							<h2>MEET US</h2>
							<p>It is a long established fact that a reader will be distracted by the
							content of a page when looking at its layout. The point of using
							Lorem Ipsum is that it has a more-or-less normal distribution of letters,
							as opposed to using 'Content here, content here', making it look like
							readable English. </p>
							<div class="conct_leftadd">
								<h4>Address</h4>
								<ul>
									<li><i class="fa fa-map-marker" aria-hidden="true"></i> Suite # 505, Fifth Floor, Al-Qadir Heights, New Garden Town, Lahore.</li>
									<li><i class="fa fa-phone" aria-hidden="true"></i> Tel +92-42-38911784</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-7">
						<div class="ryt_contctform ryt_innerlink">
							@if (Session::has('message'))
							  	<div class="alert alert-info">{{ Session::get('message') }}</div>
						   	@endif
							<form action="{{URL::to('/')}}/contactUs" class="validate-me" method="post">
							@csrf
								<div class="ryt_contctfrm_inner">
									<h2 class="text-center">Contact Us</h2>
									<div class="form-group">
										<label>Name</label>
										<input type="text" name="name" class="form-control" placeholder="Enter Name" required>
									</div>
									<div class="form-group">
										<label>Email</label>
										<input type="email" name="email" value="{{Auth::user()->email}}" class="form-control" placeholder="Enter Email" readonly required>
									</div>
									<div class="form-group">
										<label>Subject</label>
										<input type="text" name="subject"  class="form-control" placeholder="Enter Subject" required>
									</div>
									<div class="form-group">
										<label>Message</label>
										<textarea class="form-control" name="message" placeholder="Enter Message here...." required></textarea>
									</div>
									<div class="innerryt_linkbtn text-center">
										<button class="btn btn_more" type="submit">Send</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop
