@php
if(Auth::check() && Auth::user()->user_type == 'U')
	$layout = 'layouts.inner';
else
	$layout = 'layouts.outer';
@endphp
@extends($layout)
@section('content')
<section class="event_section privacy_policy">
	<div class="inner_contnt_sction text-center">
		<h1></h1>
		<p></p>
	</div>
	<div class="container">
		<h3 class="text-center">Privacy Policy </h3>
		<h4 class="text-center"></h4>		
				<div class="col-md-12">
					<div class="contnt_inner">
						<ul>
							<li>
								<p>This following document sets forth the Privacy Policy for the Festival Head website, http://www.festivalhead.com.au.
Festival Head is committed to providing you with the best possible customer service experience. Festival Head is bound by the Privacy Act 1988 (Cth), which sets out a number of principles concerning the privacy of individuals.</p>
							</li>
							<li>
								<h4>Collection of your personal information</h4>
								<p>There are many aspects of the site which can be viewed without providing personal information, however, for access to future Festival Head customer support features you are required to submit personally identifiable information. This may include but not limited to a unique username and password, or provide sensitive information in the recovery of your lost password.</p>
							</li>
							<li>
								<h4>Sharing of your personal information</h4>
								<p>We may occasionally hire other companies to provide services on our behalf, including but not limited to handling customer support enquiries, processing transactions or customer freight shipping. Those companies will be permitted to obtain only the personal information they need to deliver the service. Festival Head takes reasonable steps to ensure that these organisations are bound by confidentiality and privacy obligations in relation to the protection of your personal information.</p>
							</li>
							<li>
								<h4>Use of your personal information</h4>
								<p>For each visitor to reach the site, we expressively collect the following non-personally identifiable information, including but not limited to browser type, version and language, operating system, pages viewed while browsing the Site, page access times and referring website address. This collected information is used solely internally for the purpose of gauging visitor traffic, trends and delivering personalized content to you while you are at this Site.<br/>
								From time to time, we may use customer information for new, unanticipated uses not previously disclosed in our privacy notice. If our information practices change at some time in the future we will use for these new purposes only, data collected from the time of the policy change forward will adhere to our updated practices.</p>
							</li>
							<li>
								<h4>Changes to this Privacy Policy</h4>
								<p>Festival Head reserves the right to make amendments to this Privacy Policy at any time. If you have objections to the Privacy Policy, you should not access or use the Site.</p>
							</li>

							<li>
								<h4>Accessing Your Personal Information</h4>
								<p>You have a right to access your personal information, subject to exceptions allowed by law. If you would like to do so, please let us know. You may be required to put your request in writing for security reasons. 
									Festival Head reserves the right to charge a fee for searching for, and providing access to, your information on a per request basis.</p>
							</li>

							<li>
								<h4>Contacting us</h4>
								<p>Festival Head welcomes your comments regarding this Privacy Policy. If you have any questions about this Privacy Policy and would like further information, please contact us by any of the following means during business hours Monday to Friday.</p>
								<p>E-mail:admin@festivalhead.com.au</p>
							</li>
							
						</ul>
					</div>
				</div>
	</div>
</section>
@endsection