@php
if(Auth::check() && Auth::user()->user_type == 'U')
	$layout = 'layouts.inner';
else
	$layout = 'layouts.outer';
@endphp
@extends($layout)
@section('content')
<section class="event_section privacy_policy">
	<div class="inner_contnt_sction text-center">
		<h1></h1>
		<p></p>
	</div>
	<div class="container">
		<h3 class="text-center">About Us </h3>
		<h4 class="text-center"></h4>		
				<div class="col-md-12">
					<div class="contnt_inner">
						<ul>
							<li>
								<p>It’s About Unforgettable Music Experiences</p>
							</li>
							<li>
								<p>An idea conceived in Newcastle – a city famous for live music venues</p>
							</li>
							<li>
								<p>It was 2015 and I had recently moved to Newcastle from my hometown. As a newcomer I didn’t know a lot of people in the city. And the people I did know, either weren’t into live music or didn’t have the same taste as me.</p>
							</li>
							<li>
								<h4>&nbsp;</h4>
								<p>So it often happened that when I wanted to go to an event, I didn’t have anyone to go with. That’s when it hit me – why not build an app or website to solve this problem?</p>
							</li>
							<li>
								<p>Imagine you want to go to a music event or festival but you’re alone that day. So you use an app that connects you to other people heading to the same festival / event. You can chat with them and go to the event together. And who knows, you can end up making some great friends who share your taste in music.</p>
							</li>

							<li>
								<p>What You Can Use Festival Head For</p>
								<p>With Festival Head you can:</p>
								<p> •	See upcoming events</p>
								<p> •	Buy tickets</p>
								<p> •	Make and follow friends</p>
								<p> •	Share / post memories </p>
								<p> •	Share your love for live music</p>
							</li>

							<li>
								<p>
Once you create an free account, you can share pictures and videos of your event experiences. <br/>
								You can also follow friends through the site.</p>
								<p>Festival Head is about bringing the music community closer together.<br/>Your next night to remember may very well start here… </p>
								<a href="{{URL::to('/')}}/sign-up">Register Now</a>
							</li>
							
						</ul>
					</div>
				</div>
	</div>
</section>
@endsection