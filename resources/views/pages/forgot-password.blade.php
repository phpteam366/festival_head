@extends('layouts.outer')
@section('content')
<section class="inner_linkpages forgot_pswd">
	<div class="container">
		<div class="col-md-10 col-md-offset-1">
			<div class="inner_linkouter">
				<div class="col-md-5 paddg_0_left col-sm-5">
					<div class="left_innerlink">
						<div class="left_innlinkcontnt">
							<h2>Hello friend!</h2>
							<p>Enter your personal details and start journey with us</p>
							<a class="left_innerbtn" href="{{URL::to('/')}}/sign-up">Signup</a>
						</div>
					</div>
				</div>
				<div class="col-md-7 col-sm-7">
					<form action="#" method="post" class="validate-me">
					@csrf
						<div class="ryt_innerlink">
							<div class="inner_topsection">
								<h2 class="text-center">Forgot Password</h2>
								<p class="text-center">Please enter registered email to reset your password</p>
							</div>
							@if (Session::has('message'))
							   <div class="alert alert-info">{{ Session::get('message') }}</div>
							@endif
							@if (Session::has('success'))
							   <div class="alert alert-success">{{ Session::get('success') }}</div>
							@endif
							<div class="form-group">
								<label>Email</label>
								<input type="email"
								name="email" class="form-control" placeholder="Enter Email" autocomplete="off" required>
							</div>
							<h5 class="text-center"><a href="{{URL::to('/')}}/login">Back to Login</a></h5>
							<div class="innerryt_linkbtn text-center">
								<button class="btn btn_more" type="submit">RESET</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@stop
